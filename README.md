# WorldManager-PeopleStreme-Middleware

WorldManager and PeopleStreme middleware for Guzman y Gomez.
Project built using [Serverless framework](https://serverless.com/framework/) to act as a middleware between [World Manager](https://www.worldmanager.com) and [PeopleStreme](https://www.peoplestreme.com/).


# AWS Initialization
Assuming you have already created a new AWS account and would like to deploy the resources into AWS follow the below instructions. You will need a brand new AWS account to start

1. Create an IAM policy called ServerlessDeployPolicy and give the policy permissions the below JSON:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "apigateway:GET",
                "apigateway:POST",
                "apigateway:PUT",
                "apigateway:DELETE",
                "cloudformation:CreateStack",
                "cloudformation:Describe*",
                "cloudformation:ValidateTemplate",
                "cloudformation:UpdateStack",
                "cloudformation:List*",
                "iam:GetRole",
                "iam:PassRole",
                "iam:CreateRole",
                "iam:DeleteRole",
                "iam:CreateServiceLinkedRole",
                "iam:DetachRolePolicy",
                "iam:PutRolePolicy",
                "iam:AttachRolePolicy",
                "iam:DeleteRolePolicy",
                "lambda:UpdateFunctionCode",
                "lambda:Get*",
                "lambda:CreateFunction",
                "lambda:InvokeFunction",
                "lambda:UpdateFunctionConfiguration",
                "lambda:PublishVersion",
                "lambda:DeleteFunction",
                "lambda:DeleteLayerVersion",
                "lambda:List*",
                "lambda:AddPermission",
                "lambda:RemovePermission",
                "s3:CreateBucket",
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:GetBucketLocation",
                "s3:ListBucket",
                "s3:PutObject",
                "s3:DeleteBucket",
                "s3:GetEncryptionConfiguration",
                "s3:PutEncryptionConfiguration",
                "logs:Describe*",
                "logs:CreateLogGroup",
                "logs:DeleteLogGroup",
                "events:PutRule",
                "events:DescribeRule",
                "events:PutTargets",
                "ssm:PutParameter",
                "ssm:DeleteParameter",
                "ssm:GetParameterHistory",
                "ssm:GetParametersByPath",
                "ssm:GetParameters",
                "ssm:GetParameter",
                "dynamodb:CreateTable",
                "dynamodb:BatchWriteItem",
                "cloudformation:DeleteStack"
            ],
            "Resource": "*"
        }
    ]
}
```

2. Create an IAM user with programatic access ONLY and call it: ServelessDeployUser. Attached the above policy to the user
You will need the Access Key ID and the Secret Access Key for this user to set up in Bitbucket and your local .aws configuration file

3. Verify email address gyg@4mation.com.au in region us-east-1 for SES

# bitbucket Setup
Set up Bitbucket with the Access and Secret Key:
    Got the Bitbucket Repository and click the "Repository variables" section.
    Enter in the AWS_KEY id from the above ServelessDeployUser you created
    Enter in the AWS_SECRET id from the above ServelessDeployUser you created



# Local Installation instructions

Make sure [Lando](https://docs.devwithlando.io/) is installed.

You will need the AWS CLI too already installed on your machine to proceed.
Go to you local .aws/credentials file and update it with the below (use your newly created AWS Access Key and Secret you created to fill in the XXXXXXXX below):
```
[gyg-serverless]
aws_access_key_id = XXXXXXXX
aws_secret_access_key = XXXXXXXX
aws_region = ap-southeast-2
```

## Setup

```bash
lando start
```

## Deploying with Lando
..
DB

App



4. Setting up your local .aws credentials file:
    Get the AWS Access Key and Secret Id and from Keeper for the ServerlessDeployUser. Then create a folder called .aws in the project root and a new file in .aws folder called credentials. Copy the below code and replace XXXXXXXX with your Access ID and Secret Key.
    ```
    [gyg-serverless]
    aws_access_key_id = XXXXXXXX
    aws_secret_access_key = XXXXXXXX
    aws_region = ap-southeast-2
    ```

5. Update the Systems Manager Parameter Store:

    Go to Keeper Security and retrieve the `AWS Systems Manager Password` password commands. See an example below of one of the commands:
    ```
    aws ssm put-parameter --name "dev_world_manager_client_id" --value "XXXXX" --type "SecureString" --region ap-southeast-2 --profile gyg-serverless
    ```

    Run all the password commands in your terminal. This will update Systems Manager Parameter Store with the passwords for Staging, UAT and Production


6. Go to Bitbucket Pipelines and run the pipeline for on the master branch for the environment you want to deploy to. 


# Running the Tests 
Credentials are in keeper

## World Manager to Preceda

1. Edit at least one account in https://guzmanygomez-playground.worldmanager.com

2. Open this URL https://6kv1fd2ei6.execute-api.ap-southeast-2.amazonaws.com/dev/exportAccountUpdatedSince/1234
You should see something like this:
image.png

3. Connect to Peoplestreme SFTP Prod using a client like FileZilla.
You should see 2 files edited recently
image.png

The file should have the accounts you updated in step 1-.
Check business rules here, https://4mation.atlassian.net/wiki/spaces/GYG/pages/608698548/World+Manager+to+PeopleStreme+Business+Rules

## Preceda to World Manager

1. Create a CSV file with the accounts you want to update. Name should be dev_Preceda2WM_yyMMddHHMM.csv, changing YYMMDDHHMM with today's date. There is a sample file in the SFTP server mentioned in step 2-.

2. Connect to Preceda SFTP - TEST using a client like FileZilla.

3. Upload the file created in step 1- to /AUTO_INTEGRATION/Outbound

4. Open this URL https://6kv1fd2ei6.execute-api.ap-southeast-2.amazonaws.com/dev/exportToWM
You should see something like this:
image.png

5. Login to World Manager playground and check the updated account

Check Business rules here, https://4mation.atlassian.net/wiki/spaces/GYG/pages/609648962/Preceda+to+World+Manager+Business+Rules