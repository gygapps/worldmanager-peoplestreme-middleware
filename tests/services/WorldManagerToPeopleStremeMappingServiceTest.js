const test = require('colored-tape');
const moment = require('moment-timezone');

const WorldManagerToPeopleStremeMappingService = require('../../src/services/WorldManagerToPeopleStremeMapping');

const worldManagerToPeopleStremeMappingService = new WorldManagerToPeopleStremeMappingService();

const today = moment().tz('Australia/Sydney').format('DD.MM.YY');

const femaleAccount = {
  uuid: '77d724f9-b5f3-45e1-8f36-a23a80290e4c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: '5cb93dd1-816a-4782-97d6-fd0ae79f9e92',
  storeUuids: '2e6414d9-b1f6-43ef-be13-121e303fe36b',
  username: 'SienaZunic',
  person: {
    title: 'MISS',
    firstName: 'Siena',
    middleName: '',
    lastName: 'Zunic',
    nickname: '',
    displayName: null,
  },
  active: 'true',
  language: 'English',
  positionName: 'Crew',
  positionType: 'Part Time',
  email: 'Siena.Zunic@gyg.com',
  phone: '0455660903',
  mobile: '0455660903',
  receiveEmails: 'true',
  timezone: {
    value: ['Australia/Sydney'],
    name: ['(GMT+10:00) Canberra, Melbourne, Sydney'],
  },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '26-06-2019',
  createdAt: '2019-06-26T00:58:35Z',
  updatedAt: '2019-07-07T00:40:37Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Female' },
    { fieldName: '03. Position Title', value: 'Crew' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55,000' },
    { fieldName: '07. Trainee', value: 'N' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: '42. Preceda_ID', value: ' ' },
  ],
};

const maleAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '165838b2-106f-4f06-bd1d-83807d006d50',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Assistant Restaurant Manager',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Assistant Restaurant Manager' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'N' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const headOfficeAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '165838b2-106f-4f06-bd1d-83807d006d50',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'N' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Preceda_ID', value: ' ' },
  ],
};

const traineeYear10Account = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '79771781-fffc-4cc8-882a-843264e196dd',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Crew',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Crew' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'N' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Year 10' },
    { fieldName: 'Date Left School', value: today },
  ],
};

const traineeYear12YearsMoreThan5Account = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '79771781-fffc-4cc8-882a-843264e196dd',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Crew',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Crew' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'N' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Year 12' },
    { fieldName: 'Date Left School', value: '01.01.80' },
  ],
};

const traineeStillAtSchoolAccount = {
  uuid: '77d724f9-b5f3-45e1-8f36-a23a80290e4c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: '5cb93dd1-816a-4782-97d6-fd0ae79f9e92',
  storeUuids: '79771781-fffc-4cc8-882a-843264e196dd',
  username: 'SienaZunic',
  person: {
    title: 'MISS',
    firstName: 'Siena',
    middleName: '',
    lastName: 'Zunic',
    nickname: '',
    displayName: null,
  },
  active: 'true',
  language: 'English',
  positionName: 'Crew',
  positionType: 'Part Time',
  email: 'Siena.Zunic@gyg.com',
  phone: '0455660903',
  mobile: '0455660903',
  receiveEmails: 'true',
  timezone: {
    value: ['Australia/Sydney'],
    name: ['(GMT+10:00) Canberra, Melbourne, Sydney'],
  },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '26-06-2019',
  createdAt: '2019-06-26T00:58:35Z',
  updatedAt: '2019-07-07T00:40:37Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Female' },
    { fieldName: '03. Position Title', value: 'Crew' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'N' },
    { fieldName: '06. Salary', value: '55,000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Still at School' },
    { fieldName: 'Date Left School', value: '01.01.80' },
  ],
};

const traineeFromRestaurantWithoutTraineeCodes = {
  uuid: '77d724f9-b5f3-45e1-8f36-a23a80290e4c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: '5cb93dd1-816a-4782-97d6-fd0ae79f9e92',
  storeUuids: '165838b2-106f-4f06-bd1d-83807d006d50',
  username: 'SienaZunic',
  person: {
    title: 'MISS',
    firstName: 'Siena',
    middleName: '',
    lastName: 'Zunic',
    nickname: '',
    displayName: null,
  },
  active: 'true',
  language: 'English',
  positionName: 'Crew',
  positionType: 'Part Time',
  email: 'Siena.Zunic@gyg.com',
  phone: '0455660903',
  mobile: '0455660903',
  receiveEmails: 'true',
  timezone: {
    value: ['Australia/Sydney'],
    name: ['(GMT+10:00) Canberra, Melbourne, Sydney'],
  },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '26-06-2019',
  createdAt: '2019-06-26T00:58:35Z',
  updatedAt: '2019-07-07T00:40:37Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Female' },
    { fieldName: '03. Position Title', value: 'Crew' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'N' },
    { fieldName: '06. Salary', value: '55,000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Year 12' },
    { fieldName: 'Date Left School', value: '01.01.80' },
  ],
};

const restaurantManagerAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '165838b2-106f-4f06-bd1d-83807d006d50',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Restaurant Manager',
  positionType: 'Part Time',
  email: 'restaurantManager@mail.com',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Restaurant Manager' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const helensvaleAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: 'ef633e6c-cb24-45d4-9170-d9817fdd56a6',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: 'N/A' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: 'N/A' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const haynesAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '82a83681-9ba2-4681-8144-c4d7047dd649',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const caltexTullamarineAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: 'ae577ac4-85d4-4fa8-82b4-28253263b382',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const heatherbraeAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '250bf581-7d0b-4f6b-9d12-1b503368dbc9',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const bakewellAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: 'fb7f1448-6756-4f89-abe7-7ea87461adb1',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const australiaSquareTraineeAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '165838b2-106f-4f06-bd1d-83807d006d50',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Casual',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

const castleTowersShiftLeaderTraineeYesAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: 'b2475da8-fee7-4251-bec1-68221852a703',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Shift Leader',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Shift Leader' },
    { fieldName: '04. Employment Level', value: 'RIA LVL 2 Crew' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Year 12' },
    { fieldName: 'Date Left School', value: '15.09.20' },
  ],
};

const rutherfordAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '8d69466d-183c-4441-a8e6-a4810b0133d2',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Shift Leader',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Shift Leader' },
    { fieldName: '04. Employment Level', value: 'RIA LVL 2 Crew' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Year 12' },
    { fieldName: 'Date Left School', value: '15.09.20' },
  ],
};

const noTraineeQuestionAccount = {
  uuid: '98ca9992-bd31-401c-9fa9-9120f5a1298c',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: '79771781-fffc-4cc8-882a-843264e196dd',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Shift Leader',
  positionType: 'Part Time',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Shift Leader' },
    { fieldName: '04. Employment Level', value: 'RIA LVL 2 Crew' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
    { fieldName: 'Year Left School', value: 'Year 12' },
    { fieldName: 'Date Left School', value: '15.09.19' },
  ],
};

const northIpswichAccount = {
  uuid: 'dd92a0cf-d25c-4387-8401-b0fa1a78a7f0',
  groupId: '14',
  countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
  areaUuids: 'f5d91215-a229-4014-a4cd-1260f303bbe7',
  storeUuids: 'dd92a0cf-d25c-4387-8401-b0fa1a78a7f0',
  username: 'ASubek',
  person: {
    title: 'MR',
    firstName: 'Anthony',
    middleName: '',
    lastName: 'Subek',
    nickname: '',
    displayName: null,
  },
  active: 'false',
  language: 'English',
  positionName: 'Head Office',
  positionType: 'Casual',
  email: '',
  phone: '61413476729',
  mobile: '0413476729',
  receiveEmails: 'true',
  timezone: { value: [Array], name: [Array] },
  userDetailsUpdated: 'true',
  userPasswordUpdated: 'true',
  workStartDate: '21-06-2019',
  createdAt: '2019-06-21T08:44:09Z',
  updatedAt: '2019-07-15T00:00:08Z',
  customDetails: [
    { fieldName: "01. Crew Member's Date Of Birth (RM must complete when creating account)", value: '01-05-1985' },
    { fieldName: '02. Gender', value: 'Male' },
    { fieldName: '03. Position Title', value: 'Head Office' },
    { fieldName: '04. Employment Level', value: 'FFIA LVL 1' },
    { fieldName: '05. Salaried Employee', value: 'Y' },
    { fieldName: '06. Salary', value: '55000' },
    { fieldName: '07. Trainee', value: 'Y' },
    { fieldName: '08. Monday Shift Time', value: '10.00am - 4.30pm' },
    { fieldName: '09. Monday Break Duration', value: '30mins' },
    { fieldName: '10. Monday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '11. Monday Total Hours', value: '5.5' },
    { fieldName: '12. Tuesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '13. Tuesday Break Duration', value: '60mins' },
    { fieldName: '14. Tuesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '15. Tuesday Total Hours', value: '8' },
    { fieldName: '16. Wednesday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '17. Wednesday Break Duration', value: '30mins' },
    { fieldName: '18. Wednesday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '19. Wednesday Total Hours', value: '8' },
    { fieldName: '20. Thursday Shift Time', value: '10.00am-4.30pm' },
    { fieldName: '21. Thursday Break Duration', value: '60mins' },
    { fieldName: '22. Thursday Break Time', value: '2pm - 2.30pm' },
    { fieldName: '23. Thursday Total Hours', value: '8' },
    { fieldName: '24. Friday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '25. Friday Break Duration', value: '30mins' },
    { fieldName: '26. Friday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '27. Friday Total Hours', value: '8' },
    { fieldName: '28. Saturday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '29. Saturday Break Duration', value: '60mins' },
    { fieldName: '30. Saturday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '31. Saturday Total Hours', value: '8' },
    { fieldName: '32. Sunday Shift Time', value: '2pm - 2.30pm' },
    { fieldName: '33. Sunday Break Duration', value: '30mins' },
    { fieldName: '34. Sunday Break Time', value: '10.00am-4.30pm' },
    { fieldName: '35. Sunday Total Hours', value: '8' },
    { fieldName: "36. Emergency Contact: Person's Name", value: 'Melanie Cribb' },
    { fieldName: "37. Emergency Contact: Person's phone number", value: '0415075541' },
    { fieldName: '38. Preceda I.D', value: null },
    { fieldName: '39.Training Academy - Career Interests', value: 'Franchisee or progress and gain further skills in H/O operations.' },
    { fieldName: '40. Training Academy - Qualifications', value: 'Diploma in Hospitality\r\nDiploma in Mangement' },
  ],
};

test('Get manager email by location and position', async (t) => {
  // Function to check if a string is a valid email
  const emailIsValid = email => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

  const email = await worldManagerToPeopleStremeMappingService.getManagerEmail('07267255-3bb5-4656-abeb-985028b7bc95', 'Crew');

  t.plan(2);
  t.equal(typeof email, 'string');
  t.equal(emailIsValid(email), true, `${email} is a valid email`);
});

test('Map gender male', async (t) => {
  const gender = worldManagerToPeopleStremeMappingService.mapGender('Male');

  t.plan(2);
  t.equal(typeof gender, 'number');
  t.equal(gender, 2, 'Male maps to 2.');
});

test('Map gender female', async (t) => {
  const gender = worldManagerToPeopleStremeMappingService.mapGender('Female');

  t.plan(2);
  t.equal(typeof gender, 'number');
  t.equal(gender, 3, 'Female maps to 3.');
});

test('Map age 15 to Under 16', async (t) => {
  const age = worldManagerToPeopleStremeMappingService.mapAge(15);

  t.plan(2);
  t.equal(typeof age, 'string');
  t.equal(age, 'Under 16', '15 maps to Under 16');
});

test('Map age 23 to 21+', async (t) => {
  const age = worldManagerToPeopleStremeMappingService.mapAge(23);

  t.plan(2);
  t.equal(typeof age, 'string');
  t.equal(age, '21+', '23 maps to 21+.');
});

test('Map age 18 to 18', async (t) => {
  const age = worldManagerToPeopleStremeMappingService.mapAge(18);

  t.plan(2);
  t.equal(typeof age, 'string');
  t.equal(age, '18', '18 maps to 18.');
});

test('Map position type casual', async (t) => {
  const positionType = await worldManagerToPeopleStremeMappingService.mapPositionType('Casual', 'N');

  t.plan(2);
  t.equal(typeof positionType, 'string');
  t.equal(positionType, 'Casual', 'Employment type is Casual');
});

test('Map position type Part Time Salaried', async (t) => {
  const positionType = await worldManagerToPeopleStremeMappingService.mapPositionType('Part Time', 'Y');

  t.plan(2);
  t.equal(typeof positionType, 'string');
  t.equal(positionType, 'Part Time Salaried', 'Employment type is Part Time Salaried');
});

test('Map position code Assistant Restaurant Manager', async (t) => {
  const positionCode = await worldManagerToPeopleStremeMappingService.getCostLevel4('Assistant Restaurant Manager');

  t.plan(2);
  t.equal(typeof positionCode, 'string');
  t.equal(positionCode, '0200', 'Position code is 0200');
});

test('Map employment type FFIA LVL 1 to level 1', async (t) => {
  const level = await worldManagerToPeopleStremeMappingService.getLevel('FFIA LVL 1');

  t.plan(2);
  t.equal(typeof level, 'string');
  t.equal(level, 'Level 1', 'PSLevel is Level 1');
});

test('Map age under 16, level FFIA LVL 1 and Full-Time to Salary Code A1', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getSalaryCode('Fulltime', '15', 'FFIA LVL 1', 'N');

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'A1', 'Salary code is A1');
});

test('Map age 20+, level RIA LVL 3 SL and Part-Time to Salary Code H5', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getSalaryCode('Part Time', '22', 'RIA LVL 3 SL', 'N');

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'H5', 'Salary code is H5');
});

test('Map age 18, level FFIA LVL 1 and Casual to Salary Code I4', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getSalaryCode('Casual', '18', 'FFIA LVL 1', 'N');

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'I4', 'Salary code is I4');
});

test('Map age 17, level FFIA LVL 1 and Casual to Salary Code I3', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getSalaryCode('Casual', '17', 'FFIA LVL 1', 'N');

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'I3', 'Salary code is I3');
});

test('Map age 16, level FFIA LVL 1 and Casual to Salary Code I2', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getSalaryCode('Casual', '16', 'FFIA LVL 1', 'N');

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'I2', 'Salary code is I2');
});

test('Map Year 10, current year to Salary Code R1', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 10', 0);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'R1', 'Salary code is R1');
});

test('Map Year 10, current year to Salary Code R2', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 10', 1);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'R2', 'Salary code is R2');
});

test('Map Year 10, current year to Salary Code R3', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 10', 2);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'R3', 'Salary code is R3');
});

test('Map Year 10, current year to Salary Code R4', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 10', 3);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'R4', 'Salary code is R4');
});

test('Map Year 10, current year to Salary Code R5', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 10', 4);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'R5', 'Salary code is R5');
});

test('Map Year 11, current year to Salary Code S1', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 11', 0);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'S1', 'Salary code is S1');
});

test('Map Year 11, current year to Salary Code S2', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 11', 1);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'S2', 'Salary code is S2');
});

test('Map Year 11, current year to Salary Code S3', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 11', 2);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'S3', 'Salary code is S3');
});

test('Map Year 11, current year to Salary Code S4', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 11', 3);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'S4', 'Salary code is S4');
});

test('Map Year 11, current year to Salary Code S5', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 11', 4);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'S5', 'Salary code is S5');
});

test('Map Year 11, current year to Salary Code S5', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 11', 5);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'S5', 'Salary code is S5');
});

test('Map Year 12, current year to Salary Code T1', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 12', 0);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'T1', 'Salary code is T1');
});

test('Map Year 12, 1 year to Salary Code T2', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 12', 1);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'T2', 'Salary code is T2');
});

test('Map Year 12, 2 years to Salary Code T3', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 12', 2);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'T3', 'Salary code is T3');
});

test('Map Year 12, 3 years to Salary Code T4', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 12', 3);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'T4', 'Salary code is T4');
});

test('Map Year 12, 4 years to Salary Code T4', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 12', 4);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'T4', 'Salary code is T4');
});

test('Map Year 12, 10 years to Salary Code T4', async (t) => {
  const salaryCode = await worldManagerToPeopleStremeMappingService.getTraineeSalaryCode('Year 12', 10);

  t.plan(2);
  t.equal(typeof salaryCode, 'string');
  t.equal(salaryCode, 'T4', 'Salary code is T4');
});

test('Get UNIVERSITY QLD managers email', async (t) => {
  const email = await worldManagerToPeopleStremeMappingService.getManagerEmail('00c94b2c-4794-4ee3-af82-2b28b870a0d6', 'Crew');

  t.plan(2);
  t.equal(typeof email, 'string');
  t.equal(email, 'rmuoq@GyG.com.au', 'Email is rmuoq@GyG.com.au');
});

test('Restaurant Manager email is empty', async (t) => {
  const email = await worldManagerToPeopleStremeMappingService.getManagerEmail('00c94b2c-4794-4ee3-af82-2b28b870a0d6', 'Restaurant Manager');

  t.plan(2);
  t.equal(typeof email, 'string');
  t.equal(email, '', 'Email is empty');
});

test('Calculate age', async (t) => {
  const age = await worldManagerToPeopleStremeMappingService.calculateAge('27-01-1986');

  t.plan(2);
  t.equal(typeof age, 'number', 'Age is a number.');
  t.assert(age >= 33, 'Age is 33 or older.');
});

test('Add total hours', async (t) => {
  const hours = {
    mondayTotalHours: 1,
    tuesdayTotalHours: 2,
    wednesdayTotalHours: 3,
    thursdayTotalHours: 0,
    fridayTotalHours: 4,
    saturdayTotalHours: 5,
    sundayTotalHours: 6,
  };

  const totalHours = worldManagerToPeopleStremeMappingService.addTotalHours(hours);

  t.plan(2);
  t.equal(typeof totalHours, 'number');
  t.equal(totalHours, 21, 'Total hours is 21');
});

test('PeopleStreme Standard File Mapping female account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeStandardFileMapping(femaleAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.payrollId, femaleAccount.uuid, 'UUID is mapped correctly.');
  t.equal(PSAccount.managerEmail, 'rmwrb@GyG.com.au', 'Manager email mapped correctly.');
  t.equal(PSAccount.username, femaleAccount.email, 'Username is mapped correctly.');
  t.equal(PSAccount.salutation, femaleAccount.person.title, 'Salutation is mapped correctly.');
  t.equal(PSAccount.firstName, femaleAccount.person.firstName, 'First name is mapped correctly.');
  t.equal(PSAccount.lastName, femaleAccount.person.lastName, 'Last name is mapped correctly.');
  t.equal(PSAccount.gender, 3, 'Female gender mapped correctly.');
  t.equal(PSAccount.positionTitle, 'WERRIBEE - Crew', 'Position title mapped correctly.');

  t.equal(PSAccount.mobile, femaleAccount.mobile, 'Mobile mapped correctly.');

  t.equal(PSAccount.startDate, '2019-06-26', 'Start date is mapped correctly.');
  t.equal(PSAccount.terminationDate, null, 'Termination date is null.');
  t.equal(PSAccount.dateOfBirth, '1985-05-01', 'Date of birth is mapped correctly.');

  t.equal(PSAccount.highFrequencyReview, 'FALSE', 'High frequency review is always FALSE.');
  t.equal(PSAccount.ceo, 'FALSE', 'CEO is always FALSE.');
  t.equal(PSAccount.language, 1, 'Language is 1 (English).');
  t.equal(PSAccount.moduleAccessOnboarding, 'TRUE', 'Onboarding is TRUE.');

  t.equal(PSAccount.emergencyContactName, 'Melanie Cribb', 'Emergency contact name.');
  t.equal(PSAccount.emergencyContactMobile, '0415075541', 'Emergency contact mobile.');

  t.end();
});

test('PeopleStreme Custom File Mapping female account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(femaleAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, femaleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '060606', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Crew', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'N', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'VIC', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0606', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0202', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0036', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'WERRIBEE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'rmwrb@GyG.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Standard File Mapping male account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeStandardFileMapping(maleAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.payrollId, maleAccount.uuid, 'UUID is mapped correctly.');
  t.equal(PSAccount.managerEmail, 'ASQRM@gyg.com.au', 'Manager email mapped correctly.');
  t.equal(PSAccount.username, maleAccount.email, 'Username is mapped correctly.');
  t.equal(PSAccount.salutation, maleAccount.person.title, 'Salutation is mapped correctly.');
  t.equal(PSAccount.firstName, maleAccount.person.firstName, 'First name is mapped correctly.');
  t.equal(PSAccount.lastName, maleAccount.person.lastName, 'Last name is mapped correctly.');
  t.equal(PSAccount.gender, 2, 'Male gender mapped correctly.');
  t.equal(PSAccount.positionTitle, 'AUSTRALIA SQUARE - Assistant Restaurant Manager', 'Position title mapped correctly.');

  t.equal(PSAccount.mobile, maleAccount.mobile, 'Mobile mapped correctly.');

  t.equal(PSAccount.startDate, '2019-06-21', 'Start date is mapped correctly.');
  t.equal(PSAccount.terminationDate, null, 'Termination date is null.');
  t.equal(PSAccount.dateOfBirth, '1985-05-01', 'Date of birth is mapped correctly.');

  t.equal(PSAccount.highFrequencyReview, 'FALSE', 'High frequency review is always FALSE.');
  t.equal(PSAccount.ceo, 'FALSE', 'CEO is always FALSE.');
  t.equal(PSAccount.language, 1, 'Language is 1 (English).');
  t.equal(PSAccount.moduleAccessOnboarding, 'TRUE', 'Onboarding is TRUE.');

  t.equal(PSAccount.emergencyContactName, 'Melanie Cribb', 'Emergency contact name.');
  t.equal(PSAccount.emergencyContactMobile, '0415075541', 'Emergency contact mobile.');

  t.end();
});

test('PeopleStreme Standard File Mapping Head Office account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeStandardFileMapping(headOfficeAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.payrollId, maleAccount.uuid, 'UUID is mapped correctly.');
  t.equal(PSAccount.managerEmail, 'ASQRM@gyg.com.au', 'Manager email mapped correctly.');
  t.equal(PSAccount.username, maleAccount.email, 'Username is mapped correctly.');
  t.equal(PSAccount.salutation, maleAccount.person.title, 'Salutation is mapped correctly.');
  t.equal(PSAccount.firstName, maleAccount.person.firstName, 'First name is mapped correctly.');
  t.equal(PSAccount.lastName, maleAccount.person.lastName, 'Last name is mapped correctly.');
  t.equal(PSAccount.gender, 2, 'Male gender mapped correctly.');
  t.equal(PSAccount.positionTitle, null, 'Position title is empty.');

  t.equal(PSAccount.mobile, maleAccount.mobile, 'Mobile mapped correctly.');

  t.equal(PSAccount.startDate, '2019-06-21', 'Start date is mapped correctly.');
  t.equal(PSAccount.terminationDate, null, 'Termination date is null.');
  t.equal(PSAccount.dateOfBirth, '1985-05-01', 'Date of birth is mapped correctly.');

  t.equal(PSAccount.highFrequencyReview, 'FALSE', 'High frequency review is always FALSE.');
  t.equal(PSAccount.ceo, 'FALSE', 'CEO is always FALSE.');
  t.equal(PSAccount.language, 1, 'Language is 1 (English).');
  t.equal(PSAccount.moduleAccessOnboarding, 'TRUE', 'Onboarding is TRUE.');

  t.equal(PSAccount.emergencyContactName, 'Melanie Cribb', 'Emergency contact name.');
  t.equal(PSAccount.emergencyContactMobile, '0415075541', 'Emergency contact mobile.');

  t.end();
});

test('PeopleStreme Custom File Mapping male account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(maleAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '040402', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Assistant Restaurant Manager', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'N', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'NSW', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0404', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0200', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, ' ', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'AUSTRALIA SQUARE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'ASQRM@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping head office account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(headOfficeAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'N', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'NSW', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0404', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, ' ', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'AUSTRALIA SQUARE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'ASQRM@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping trainee Year 10 account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(traineeYear10Account);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, traineeYear10Account.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '050807', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Trainee - Crew', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 'P', 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'N', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, '', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 4, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'QLD', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0508', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0201', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0037', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'SOUTHBANK', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'rmsbk@GyG.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, 'R1', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping trainee Year 12, More than 5 years account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(traineeYear12YearsMoreThan5Account);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, traineeYear12YearsMoreThan5Account.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '050807', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Trainee - Crew', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 'P', 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'N', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, '', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 4, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'QLD', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0508', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0201', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0037', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'SOUTHBANK', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'rmsbk@GyG.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, 'T4', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Standard File Mapping trainee and still at school account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeStandardFileMapping(traineeStillAtSchoolAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.payrollId, traineeStillAtSchoolAccount.uuid, 'UUID is mapped correctly.');
  t.equal(PSAccount.managerEmail, 'rmsbk@GyG.com.au', 'Manager email mapped correctly.');
  t.equal(PSAccount.username, traineeStillAtSchoolAccount.email, 'Username is mapped correctly.');
  t.equal(PSAccount.salutation, traineeStillAtSchoolAccount.person.title, 'Salutation is mapped correctly.');
  t.equal(PSAccount.firstName, traineeStillAtSchoolAccount.person.firstName, 'First name is mapped correctly.');
  t.equal(PSAccount.lastName, traineeStillAtSchoolAccount.person.lastName, 'Last name is mapped correctly.');
  t.equal(PSAccount.gender, 3, 'Female gender mapped correctly.');
  t.equal(PSAccount.positionTitle, 'SOUTHBANK - Trainee - Crew', 'Position title mapped correctly.');

  t.equal(PSAccount.mobile, traineeStillAtSchoolAccount.mobile, 'Mobile mapped correctly.');

  t.equal(PSAccount.startDate, '2019-06-26', 'Start date is mapped correctly.');
  t.equal(PSAccount.terminationDate, null, 'Termination date is null.');
  t.equal(PSAccount.dateOfBirth, '1985-05-01', 'Date of birth is mapped correctly.');

  t.equal(PSAccount.highFrequencyReview, 'FALSE', 'High frequency review is always FALSE.');
  t.equal(PSAccount.ceo, 'FALSE', 'CEO is always FALSE.');
  t.equal(PSAccount.language, 1, 'Language is 1 (English).');
  t.equal(PSAccount.moduleAccessOnboarding, 'TRUE', 'Onboarding is TRUE.');

  t.equal(PSAccount.emergencyContactName, 'Melanie Cribb', 'Emergency contact name.');
  t.equal(PSAccount.emergencyContactMobile, '0415075541', 'Emergency contact mobile.');

  t.end();
});

test('PeopleStreme Custom File Mapping trainee and still at school account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(traineeStillAtSchoolAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, traineeStillAtSchoolAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '050807', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Trainee - Crew', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 'P', 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'N', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, '', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 4, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'QLD', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0508', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0201', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0037', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'SOUTHBANK', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'rmsbk@GyG.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, 'A7', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping trainee from restaurant without trainee codes', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(traineeFromRestaurantWithoutTraineeCodes);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, traineeFromRestaurantWithoutTraineeCodes.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '040406', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Crew', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 'P', 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'N', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, '', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 4, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'NSW', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0404', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0202', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, ' ', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'AUSTRALIA SQUARE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'ASQRM@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, 'A7', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Standard File Mapping Restaurant Manager account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeStandardFileMapping(restaurantManagerAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.payrollId, restaurantManagerAccount.uuid, 'UUID is mapped correctly.');
  // Email should be empty for Restaurant Manager
  t.equal(PSAccount.managerEmail, '', 'Manager email mapped correctly.');
  t.equal(PSAccount.username, restaurantManagerAccount.email, 'Username is mapped correctly.');
  t.equal(PSAccount.salutation, restaurantManagerAccount.person.title, 'Salutation is mapped correctly.');
  t.equal(PSAccount.firstName, restaurantManagerAccount.person.firstName, 'First name is mapped correctly.');
  t.equal(PSAccount.lastName, restaurantManagerAccount.person.lastName, 'Last name is mapped correctly.');
  t.equal(PSAccount.gender, 2, 'Male gender mapped correctly.');
  t.equal(PSAccount.positionTitle, 'AUSTRALIA SQUARE - Restaurant Manager', 'Position title is mapped correctly.');

  t.equal(PSAccount.mobile, restaurantManagerAccount.mobile, 'Mobile mapped correctly.');

  t.equal(PSAccount.startDate, '2019-06-21', 'Start date is mapped correctly.');
  t.equal(PSAccount.terminationDate, null, 'Termination date is null.');
  t.equal(PSAccount.dateOfBirth, '1985-05-01', 'Date of birth is mapped correctly.');

  t.equal(PSAccount.highFrequencyReview, 'FALSE', 'High frequency review is always FALSE.');
  t.equal(PSAccount.ceo, 'FALSE', 'CEO is always FALSE.');
  t.equal(PSAccount.language, 1, 'Language is 1 (English).');
  t.equal(PSAccount.moduleAccessOnboarding, 'TRUE', 'Onboarding is TRUE.');

  t.equal(PSAccount.emergencyContactName, 'Melanie Cribb', 'Emergency contact name.');
  t.equal(PSAccount.emergencyContactMobile, '0415075541', 'Emergency contact mobile.');

  t.equal(PSAccount.email, restaurantManagerAccount.email, 'Email mapped correctly.');
  t.equal(PSAccount.privateEmail, restaurantManagerAccount.email, 'Private email mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping Restaurant Manager account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(restaurantManagerAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, restaurantManagerAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '040401', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Restaurant Manager', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'NSW', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0404', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0204', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, ' ', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'AUSTRALIA SQUARE', 'Location mapped correctly.');
  // Email should be empty for Restaurant Manager
  t.equal(PSAccount.additionalFormRecipient, '', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping Helenvale account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(helensvaleAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.tuesdayShiftTime, '', 'Tuesday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'QLD', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0548', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0014', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'HELENSVALE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'helensvalerm@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping Haynes account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(haynesAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'WA', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0709', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0040', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'HAYNES', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'haynesrm@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping Calltex Tullamarine account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(caltexTullamarineAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'VIC', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0614', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0036', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'CALTEX TULLAMARINE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'tdcrm@GyG.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('N/A to blank', async (t) => {
  t.equal(worldManagerToPeopleStremeMappingService.NAToBlank('N/A'), '');
  t.equal(worldManagerToPeopleStremeMappingService.NAToBlank('NotN/A'), 'NotN/A');
  t.end();
});

test('PeopleStreme Custom File Mapping Heatherbrae account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(heatherbraeAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'NSW', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0456', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, ' ', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'HEATHERBRAE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'heatherbrae@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping Australia Square trainee account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(australiaSquareTraineeAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, australiaSquareTraineeAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Casual', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 'C', 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, '', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 5, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'NSW', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0404', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, ' ', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'AUSTRALIA SQUARE', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'ASQRM@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, 'I7', 'Salary code mapped correctly.');

  t.end();
});

test('Bakewell account should be exported because it is active', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(bakewellAccount);

  t.ok(PSAccount, 'Bakewell account exported');
  t.end();
});

test('Replace for colon', async (t) => {
  // eslint-disable-next-line max-len
  const text = worldManagerToPeopleStremeMappingService.replaceWithColon('This,is.a;text,');

  t.equal(text, 'This:is:a:text:');
  t.end();
});

test('Account without trainee question', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(noTraineeQuestionAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, noTraineeQuestionAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '050804', 'Position ID mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position Type mapped correctly.');
  t.equal(PSAccount.roleTitle, 'Shift Leader', 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'N', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 2', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Part Time Salaried', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 53.5, 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, 'A', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 2, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'QLD', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0508', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '0206', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0037', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'SOUTHBANK', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'rmsbk@GyG.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, '', 'Salary code mapped correctly.');

  t.end();
});

test('PeopleStreme Custom File Mapping North Ipswich account', async (t) => {
  // eslint-disable-next-line max-len
  const PSAccount = await worldManagerToPeopleStremeMappingService.peopleStremeCustomFileMapping(northIpswichAccount);

  t.equal(typeof PSAccount, 'object');
  t.equal(PSAccount.email, maleAccount.email, 'Email is mapped correctly.');
  t.equal(PSAccount.positionId, '', 'Position ID mapped correctly.');
  t.equal(PSAccount.roleTitle, null, 'Role title is mapped correctly.');
  t.equal(PSAccount.trainee, 'Y', 'Trainee is mapped correctly.');
  t.equal(PSAccount.companyName, 'Guzman y Gomez Restaurant Group PTY LTD', 'Company name mapped correctly.');
  t.equal(PSAccount.award, 'Fast Food Industry Award 2010', 'Award mapped correctly.');
  t.equal(PSAccount.level, 'Level 1', 'Level mapped correctly.');
  t.equal(PSAccount.positionType, 'Casual', 'Position type mapped correctly.');
  t.equal(PSAccount.mondayShiftTime, '10:00am - 4:30pm', 'Monday Shift Time mapped correctly.');
  t.equal(PSAccount.sundayTotalHours, '8', 'Sunday Total Hours mapped correctly.');
  t.equal(PSAccount.baseHours, 'C', 'Base Hours mapped correctly.');
  t.equal(PSAccount.autoPay, 'Y', 'Auto pay mapped correctly.');
  t.equal(PSAccount.autoPayHours, '28', 'Auto pay hours mapped correctly.');
  t.equal(PSAccount.rateFrequency, '', 'Rate frequency mapped correctly.');
  t.equal(PSAccount.basePayRate, '55000', 'Base pay rate mapped correctly.');
  t.equal(PSAccount.employmentType, 5, 'Employment type mapped correctly.');
  t.equal(PSAccount.costLevel1, 'RG', 'Cost level 1 mapped correctly.');
  t.equal(PSAccount.costLevel2, 'QLD', 'Cost level 2 mapped correctly.');
  t.equal(PSAccount.costLevel3, '0555', 'Cost level 3 mapped correctly.');
  t.equal(PSAccount.costLevel4, '', 'Cost level 4 mapped correctly.');
  t.equal(PSAccount.costLevel5, '0015', 'Cost level 5 mapped correctly.');
  t.equal(PSAccount.location, 'NORTH IPSWICH', 'Location mapped correctly.');
  t.equal(PSAccount.additionalFormRecipient, 'rmnid@gyg.com.au', 'Additional form recipients mapped correctly.');
  t.equal(PSAccount.salaryCode, 'I7', 'Salary code mapped correctly.');

  t.end();
});
