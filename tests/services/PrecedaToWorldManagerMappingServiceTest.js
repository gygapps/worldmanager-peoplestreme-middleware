const test = require('colored-tape');
const _ = require('lodash');

const WorldManager = require('../../src/services/WorldManager');
const PrecedaToWorldManagerMapping = require('../../src/services/PrecedaToWorldManagerMapping');

const options = {
  tokenURL: 'https://guzmanygomez-playground.worldmanager.com/oauth/token',
  apiURL: 'https://guzmanygomez-playground.worldmanager.com/soap/v7/',
  clientId: 'fLliY6EClEO5CL9z',
  clientSecret: 'jil0GtJIx2DIX8vSYZprSW2QLkhUSYd3',
};

const worldManager = new WorldManager(options);

const precedaToWorldManagerMapping = new PrecedaToWorldManagerMapping({ worldManager });

test('Get custom field with UUID ad2e0282-12a5-4349-ba14-60f6aa0efd9b', async (t) => {
  const expectedUuid = 'ad2e0282-12a5-4349-ba14-60f6aa0efd9b';
  const customFieldUuid = await precedaToWorldManagerMapping.getCustomFieldUUID('02. Gender');

  t.plan(2);
  t.equal(typeof customFieldUuid, 'string', 'Custom field is a string');
  t.equal(customFieldUuid, expectedUuid, `UUID is ${expectedUuid}.`);
});

test('Get custom field with UUID beaa5249-1b64-43f6-9c64-fd3b1390836d', async (t) => {
  const expectedUuid = 'beaa5249-1b64-43f6-9c64-fd3b1390836d';
  const customFieldUuid = await precedaToWorldManagerMapping.getCustomFieldUUID('32. Sunday Shift Time');

  t.plan(2);
  t.equal(typeof customFieldUuid, 'string', 'Custom field is a string');
  t.equal(customFieldUuid, expectedUuid, `UUID is ${expectedUuid}.`);
});

test('Map custom fields data', async (t) => {
  const data = {
    'Employee ID Number': '049518',
    'Global ID 03': 'A424CDD2-C2F5-488F-9DB7-96718A6F6DFA',
    Title: 'Mr',
    'First Name': 'Roger Alejandro',
    Surname: 'Fernandez Blandon',
    'Level 3 Description': 'WORLD SQUARE',
    'Personel Email Address': 'rogeralejo09@gmail.com',
    'Hire Date': '18/10/2016',
    'Mobile Phone Nu': '0449194495',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '20/11/1984',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '123456',
    'Emergency Contact Surname': 'Chala',
    'Emergency Contact Home Phone': '0414456588',
    'Salary Code Description': 'FFIA LVL 1 PT 17',
    'Apprentice/Trainee': 'No',
    'Position Title': 'Senior Cook',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '6.00pm-10.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-10.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': '',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': '6.00pm-9.00pm',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': 'No Contract Hrs',
    'Sun Information': '',
    'Contracted Hrs Mon': '7',
    'Contracted Hrs Tue': '6',
    'Contracted Hrs Wed': '5',
    'Contracted Hrs Thu': '4',
    'Contracted Hrs Fri': '3',
    'Contracted Hrs Sat': '2',
    'Contracted Hrs Sun': '1',
    State: 'NSW',
    Status: 'A',
    'Preceda I.D': ' ',
  };
  const customFields = await precedaToWorldManagerMapping.mapCustomFields(data);

  t.equal(typeof customFields, 'object', 'Custom field is an object');
  // Test each field
  t.equal(_.find(customFields, { WM: "01. Crew Member's Date Of Birth (RM must complete when creating account)" }).value,
    '1984-11-20', 'Date of birth is OK.');
  t.equal(_.find(customFields, { WM: '02. Gender' }).value, 'M', 'Gender is OK');
  t.equal(_.find(customFields, { WM: '04. Employment Level' }).value, 'FFIA LVL 1', 'Employment level is OK');
  t.equal(_.find(customFields, { WM: '05. Salaried Employee' }).value, 'N', 'Salaried Employee is OK');
  t.equal(_.find(customFields, { WM: '06. Salary' }).value, '123456', 'Salary is OK');
  t.equal(_.find(customFields, { WM: '08. Monday Shift Time' }).value, '6.00pm-10.00pm', 'Monday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '11. Monday Total Hours' }).value, '7', 'Monday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '12. Tuesday Shift Time' }).value, 'No Contract Hrs', 'Tuesday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '15. Tuesday Total Hours' }).value, '6', 'Tuesday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '16. Wednesday Shift Time' }).value, 'No Contract Hrs', 'Wednesday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '19. Wednesday Total Hours' }).value, '5', 'Wednesday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '20. Thursday Shift Time' }).value, '6.00pm-10.00pm', 'Thursday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '23. Thursday Total Hours' }).value, '4', 'Thursday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '24. Friday Shift Time' }).value, '', 'Friday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '27. Friday Total Hours' }).value, '3', 'Friday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '28. Saturday Shift Time' }).value, '6.00pm-9.00pm', 'Saturday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '31. Saturday Total Hours' }).value, '2', 'Saturday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '32. Sunday Shift Time' }).value, 'No Contract Hrs', 'Sunday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '35. Sunday Total Hours' }).value, '1', 'Sunday Total Hours is OK');
  t.equal(_.find(customFields, { WM: "36. Emergency Contact: Person's Name" }).value, 'Chala', 'Emergency Contact Name is OK');
  t.equal(_.find(customFields, { WM: "37. Emergency Contact: Person's phone number" }).value, '0414456588', 'Emergency Contact phone is OK');
  t.equal(_.find(customFields, { WM: '38. Preceda I.D' }).value, '049518', 'Preceda ID is OK');
  t.end();
});

test('Map custom fields WM to PS', async (t) => {
  const data = {
    'Employee ID Number': '049518',
    'Global ID 03': 'A424CDD2-C2F5-488F-9DB7-96718A6F6DFA',
    Title: 'Mr',
    'First Name': 'Roger Alejandro',
    Surname: 'Fernandez Blandon',
    'Level 3 Description': 'WORLD SQUARE',
    'Personel Email Address': 'rogeralejo09@gmail.com',
    'Hire Date': '18/10/2016',
    'Mobile Phone Nu': '0449194495',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '20/11/1984',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '123456',
    'Emergency Contact Surname': 'Chala',
    'Emergency Contact Home Phone': '0414456588',
    'Salary Code Description': 'FFIA LVL 1 PT 17',
    'Apprentice/Trainee': 'No',
    'Position Title': 'Crew',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '6.00pm-10.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-10.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': 'No Contract Hrs',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': '6.00pm-9.00pm',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': 'No Contract Hrs',
    'Sun Information': '',
    'Contracted Hrs Mon': '7',
    'Contracted Hrs Tue': '6',
    'Contracted Hrs Wed': '5',
    'Contracted Hrs Thu': '4',
    'Contracted Hrs Fri': '3',
    'Contracted Hrs Sat': '2',
    'Contracted Hrs Sun': '1',
    State: 'NSW',
    Status: 'A',
    'Preceda I.D': ' ',
  };
  const customFields = await precedaToWorldManagerMapping.mapCustomFields(data);

  t.equal(typeof customFields, 'object', 'Custom field is an object');
  // Test each field
  t.equal(_.find(customFields, { WM: "01. Crew Member's Date Of Birth (RM must complete when creating account)" }).PS,
    'Date of Birth', 'Date of birth is OK.');
  t.equal(_.find(customFields, { WM: '02. Gender' }).PS, 'Gender Desc', 'Gender is OK');
  t.equal(_.find(customFields, { WM: '04. Employment Level' }).PS, 'Salary Code Description', 'Employment level is OK');
  t.equal(_.find(customFields, { WM: '05. Salaried Employee' }).PS, 'Autopay', 'Salaried Employee is OK');
  t.equal(_.find(customFields, { WM: '06. Salary' }).PS, 'Base Rate', 'Salary is OK');
  t.equal(_.find(customFields, { WM: '08. Monday Shift Time' }).PS, 'Mon Notes Description', 'Monday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '11. Monday Total Hours' }).PS, 'Contracted Hrs Mon', 'Monday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '12. Tuesday Shift Time' }).PS, 'Tue Notes Description', 'Tuesday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '15. Tuesday Total Hours' }).PS, 'Contracted Hrs Tue', 'Tuesday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '16. Wednesday Shift Time' }).PS, 'Wed Notes Description', 'Wednesday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '19. Wednesday Total Hours' }).PS, 'Contracted Hrs Wed', 'Wednesday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '20. Thursday Shift Time' }).PS, 'Thu Notes Description', 'Thursday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '23. Thursday Total Hours' }).PS, 'Contracted Hrs Thu', 'Thursday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '24. Friday Shift Time' }).PS, 'Fri Notes Description', 'Friday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '27. Friday Total Hours' }).PS, 'Contracted Hrs Fri', 'Friday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '28. Saturday Shift Time' }).PS, 'Sat Notes Description', 'Saturday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '31. Saturday Total Hours' }).PS, 'Contracted Hrs Sat', 'Saturday Total Hours is OK');
  t.equal(_.find(customFields, { WM: '32. Sunday Shift Time' }).PS, 'Sun Notes Description', 'Sunday Shift Time is OK');
  t.equal(_.find(customFields, { WM: '35. Sunday Total Hours' }).PS, 'Contracted Hrs Sun', 'Sunday Total Hours is OK');
  t.equal(_.find(customFields, { WM: "36. Emergency Contact: Person's Name" }).PS, 'Emergency Contact Surname', 'Emergency Contact Name is OK');
  t.equal(_.find(customFields, { WM: "37. Emergency Contact: Person's phone number" }).PS, 'Emergency Contact Home Phone', 'Emergency Contact phone is OK');
  t.equal(_.find(customFields, { WM: '38. Preceda I.D' }).PS, 'Employee ID Number', 'Preceda ID is OK');
  t.end();
});

test('Get store UUID with name HIGHPOINT', async (t) => {
  const expectedUuid = '07267255-3bb5-4656-abeb-985028b7bc95';
  const storeUUID = await precedaToWorldManagerMapping.getStoreUUIDByName('HIGHPOINT');

  t.plan(2);
  t.equal(typeof storeUUID, 'string', 'Store is an object');
  t.equal(storeUUID, expectedUuid, `UUID is ${expectedUuid}.`);
});

test('Get position type for Full Time Salaried', async (t) => {
  const expected = 'Fulltime';
  const positionType = await precedaToWorldManagerMapping.getWMPositionTypeByEmploymentType('Full Time Salaried');

  t.plan(2);
  t.equal(typeof positionType, 'object', 'Position type is an object');
  t.equal(positionType.WMFieldPositionType, expected, `Position type is ${expected}.`);
});

test('Validate a valid persons title', async (t) => {
  const title = 'MR';
  const response = PrecedaToWorldManagerMapping.validatePersonTitle(title);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, title);
});

test('Validate an invalid persons title', async (t) => {
  try {
    PrecedaToWorldManagerMapping.validatePersonTitle('invalidTitle');
  } catch (error) {
    t.assert(error, 'Error thrown.');
  }

  t.end();
});

test('Non custom fields mapping', async (t) => {
  const worldSquareUUID = '296a7e1f-41b5-4c1c-b6d8-edb490e1038f';
  const account = {
    'Global ID 03': '6f68a12b-e3d2-4cbe-871b-11e6bff55906',
    'Level 3 Description': 'WORLD SQUARE',
    Title: 'MR',
    'First Name': 'Mark',
    Surname: 'Hawthorne',
    'Employment Type Description': 'Part Time',
    'Personel Email Address': 'mailfarhad@yahoo.com',
    'Mobile Phone Number': '+61450654987',
    'Hire Date': '15/12/2010',
    Status: 'A',
  };

  const WMAccount = await precedaToWorldManagerMapping.precedaFileMapping(account);

  t.equal(WMAccount.uuid, account['Global ID 03'], 'UUID is OK.');
  t.equal(WMAccount.storeUuids, worldSquareUUID, 'Location is OK.');
  t.equal(WMAccount.person.title, account.Title, 'Title is OK.');
  t.equal(WMAccount.person.firstName, account['First Name'], 'First name is OK.');
  t.equal(WMAccount.person.lastName, account.Surname, 'Last name is OK.');
  t.equal(WMAccount.positionType, account['Employment Type Description'], 'Position type is OK.');
  t.equal(WMAccount.email, account['Personel Email Address'], 'Email is OK.');
  t.equal(WMAccount.mobile, account['Mobile Phone Number'], 'Mobile phone is OK.');
  t.equal(WMAccount.workStartDate, '2010-12-15', 'Start date is OK.');
  t.equal(WMAccount.active, 'true', 'Active is OK.');

  t.end();
});

test('Map gender M to M', async (t) => {
  const gender = 'M';
  const response = PrecedaToWorldManagerMapping.mapGender(gender);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'M');
});

test('Map gender Male to M', async (t) => {
  const gender = 'Male';
  const response = PrecedaToWorldManagerMapping.mapGender(gender);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'M');
});

test('Map gender F to F', async (t) => {
  const gender = 'F';
  const response = PrecedaToWorldManagerMapping.mapGender(gender);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'F');
});

test('Map gender Female to F', async (t) => {
  const gender = 'Female';
  const response = PrecedaToWorldManagerMapping.mapGender(gender);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'F');
});

test('Map RIA LVL 2 Crew PT 20&Ovr to RIA LVL 2 Crew', async (t) => {
  const salaryDescription = 'RIA LVL 2 Crew PT 20&Ovr';
  const response = await precedaToWorldManagerMapping.mapSalaryCodeDescription(salaryDescription);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'RIA LVL 2 Crew');
});

test('Map RIA LVL 3 SL PT 16 & Und to RIA LVL 3 SL', async (t) => {
  const salaryDescription = 'RIA LVL 3 SL PT 16 & Und';
  const response = await precedaToWorldManagerMapping.mapSalaryCodeDescription(salaryDescription);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'RIA LVL 3 SL');
});

test('Map invalid salary description to null', async (t) => {
  const salaryDescription = 'INVALID_SALARY_DESCRIPTION';
  const response = await precedaToWorldManagerMapping.mapSalaryCodeDescription(salaryDescription);

  t.plan(2);
  t.equal(typeof response, 'object');
  t.equal(response, null);
});

test('Map FFIA LVL 1 Cas 21 to FFIA LVL 1', async (t) => {
  const salaryDescription = 'FFIA LVL 1 Cas 21';
  const response = await precedaToWorldManagerMapping.mapSalaryCodeDescription(salaryDescription);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, 'FFIA LVL 1');
});

test('Map CALTEX WERRINGTON custom fields WM to PS', async (t) => {
  const caltexWerringtonUUID = '9df5a517-f855-4ad5-ac67-44f585cf5858';
  const account = {
    'Employee ID Number': '049518',
    'Global ID 03': 'A424CDD2-C2F5-488F-9DB7-96718A6F6DFA',
    Title: 'MR',
    'First Name': 'Roger Alejandro',
    Surname: 'Fernandez Blandon',
    'Level 3 Description': 'CALTEX WERRINGTON',
    'Personel Email Address': 'rogeralejo09@gmail.com',
    'Hire Date': '18/10/2016',
    'Mobile Phone Nu': '0449194495',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '20/11/1984',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '123456',
    'Emergency Contact Surname': 'Chala',
    'Emergency Contact Home Phone': '0414456588',
    'Salary Code Description': 'FFIA LVL 1 PT 17',
    'Apprentice/Trainee': 'No',
    'Position Title': 'Crew',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '6.00pm-10.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-10.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': 'No Contract Hrs',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': '6.00pm-9.00pm',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': 'No Contract Hrs',
    'Sun Information': '',
    'Contracted Hrs Mon': '7',
    'Contracted Hrs Tue': '6',
    'Contracted Hrs Wed': '5',
    'Contracted Hrs Thu': '4',
    'Contracted Hrs Fri': '3',
    'Contracted Hrs Sat': '2',
    'Contracted Hrs Sun': '1',
    State: 'NSW',
    Status: 'A',
  };

  const WMAccount = await precedaToWorldManagerMapping.precedaFileMapping(account);

  t.equal(WMAccount.uuid, account['Global ID 03'], 'UUID is OK.');
  t.equal(WMAccount.storeUuids, caltexWerringtonUUID, 'Location is OK.');
  t.equal(WMAccount.person.title, account.Title, 'Title is OK.');
  t.equal(WMAccount.person.firstName, account['First Name'], 'First name is OK.');
  t.equal(WMAccount.person.lastName, account.Surname, 'Last name is OK.');
  t.equal(WMAccount.positionType, account['Employment Type Description'], 'Position type is OK.');
  t.equal(WMAccount.email, account['Personel Email Address'], 'Email is OK.');
  t.equal(WMAccount.mobile, account['Mobile Phone Number'], 'Mobile phone is OK.');
  t.equal(WMAccount.workStartDate, '2016-10-18', 'Start date is OK.');
  t.equal(WMAccount.active, 'true', 'Active is OK.');

  t.end();
});

test('Map UQ custom fields WM to PS', async (t) => {
  const universityQLDUUID = '00c94b2c-4794-4ee3-af82-2b28b870a0d6';
  const account = {
    'Employee ID Number': '049518',
    'Global ID 03': 'A424CDD2-C2F5-488F-9DB7-96718A6F6DFA',
    Title: 'MR',
    'First Name': 'Roger Alejandro',
    Surname: 'Fernandez Blandon',
    'Level 3 Description': 'UQ',
    'Personel Email Address': 'rogeralejo09@gmail.com',
    'Hire Date': '18/10/2016',
    'Mobile Phone Nu': '0449194495',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '20/11/1984',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '123456',
    'Emergency Contact Surname': 'Chala',
    'Emergency Contact Home Phone': '0414456588',
    'Salary Code Description': 'FFIA LVL 1 PT 17',
    'Apprentice/Trainee': 'No',
    'Position Title': 'Crew',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '6.00pm-10.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-10.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': 'No Contract Hrs',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': '6.00pm-9.00pm',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': 'No Contract Hrs',
    'Sun Information': '',
    'Contracted Hrs Mon': '7',
    'Contracted Hrs Tue': '6',
    'Contracted Hrs Wed': '5',
    'Contracted Hrs Thu': '4',
    'Contracted Hrs Fri': '3',
    'Contracted Hrs Sat': '2',
    'Contracted Hrs Sun': '1',
    State: 'NSW',
    Status: 'A',
  };

  const WMAccount = await precedaToWorldManagerMapping.precedaFileMapping(account);

  t.equal(WMAccount.uuid, account['Global ID 03'], 'UUID is OK.');
  t.equal(WMAccount.storeUuids, universityQLDUUID, 'Location is OK.');
  t.equal(WMAccount.person.title, account.Title, 'Title is OK.');
  t.equal(WMAccount.person.firstName, account['First Name'], 'First name is OK.');
  t.equal(WMAccount.person.lastName, account.Surname, 'Last name is OK.');
  t.equal(WMAccount.positionType, account['Employment Type Description'], 'Position type is OK.');
  t.equal(WMAccount.email, account['Personel Email Address'], 'Email is OK.');
  t.equal(WMAccount.mobile, account['Mobile Phone Number'], 'Mobile phone is OK.');
  t.equal(WMAccount.workStartDate, '2016-10-18', 'Start date is OK.');
  t.equal(WMAccount.active, 'true', 'Active is OK.');

  t.end();
});

test('Map UQR custom fields WM to PS', async (t) => {
  const UQRefectoryUUID = 'd1323d44-d2ad-4d54-b7ec-a9a7db2a9f65';
  const account = {
    'Employee ID Number': '049518',
    'Global ID 03': 'A424CDD2-C2F5-488F-9DB7-96718A6F6DFA',
    Title: 'MR',
    'First Name': 'Roger Alejandro',
    Surname: 'Fernandez Blandon',
    'Level 3 Description': 'UQR',
    'Personel Email Address': 'rogeralejo09@gmail.com',
    'Hire Date': '18/10/2016',
    'Mobile Phone Nu': '0449194495',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '20/11/1984',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '123456',
    'Emergency Contact Surname': 'Chala',
    'Emergency Contact Home Phone': '0414456588',
    'Salary Code Description': 'FFIA LVL 1 PT 17',
    'Apprentice/Trainee': 'No',
    'Position Title': 'Crew',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '6.00pm-10.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-10.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': 'No Contract Hrs',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': '6.00pm-9.00pm',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': 'No Contract Hrs',
    'Sun Information': '',
    'Contracted Hrs Mon': '7',
    'Contracted Hrs Tue': '6',
    'Contracted Hrs Wed': '5',
    'Contracted Hrs Thu': '4',
    'Contracted Hrs Fri': '3',
    'Contracted Hrs Sat': '2',
    'Contracted Hrs Sun': '1',
    State: 'NSW',
    Status: 'A',
  };

  const WMAccount = await precedaToWorldManagerMapping.precedaFileMapping(account);

  t.equal(WMAccount.uuid, account['Global ID 03'], 'UUID is OK.');
  t.equal(WMAccount.storeUuids, UQRefectoryUUID, 'Location is OK.');
  t.equal(WMAccount.person.title, account.Title, 'Title is OK.');
  t.equal(WMAccount.person.firstName, account['First Name'], 'First name is OK.');
  t.equal(WMAccount.person.lastName, account.Surname, 'Last name is OK.');
  t.equal(WMAccount.positionType, account['Employment Type Description'], 'Position type is OK.');
  t.equal(WMAccount.email, account['Personel Email Address'], 'Email is OK.');
  t.equal(WMAccount.mobile, account['Mobile Phone Number'], 'Mobile phone is OK.');
  t.equal(WMAccount.workStartDate, '2016-10-18', 'Start date is OK.');
  t.equal(WMAccount.active, 'true', 'Active is OK.');

  t.end();
});
