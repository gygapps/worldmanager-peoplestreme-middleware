const test = require('colored-tape');

const IntegrationTables = require('../../src/services/IntegrationTables');

const integrationTables = new IntegrationTables();

test('Get position type', async (t) => {
  const response = await integrationTables.getPositionType();

  t.plan(2);
  t.equal(typeof response, 'object', 'Response should be an object.');
  t.assert(response.length > 0, 'Response should contain elements');
});
