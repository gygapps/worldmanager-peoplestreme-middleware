
const test = require('colored-tape');
const moment = require('moment-timezone');
const WorldManagerToPeopleStremeExport = require('../../src/services/WorldManagerToPeopleStremeExport');

const worldManagerConfig = {
  tokenURL: 'https://guzmanygomez-playground.worldmanager.com/oauth/token',
  apiURL: 'https://guzmanygomez-playground.worldmanager.com/soap/v7/',
  clientId: 'fLliY6EClEO5CL9z',
  clientSecret: 'jil0GtJIx2DIX8vSYZprSW2QLkhUSYd3',
};

const sftpConfig = {
  host: 'peoplestreme.ftpstream.com',
  port: '22',
  username: 'gyg',
  password: 'cml*4C#^sp8VTy!@074aV1eXQ',
  // debug: console.log,
  algorithms: {
    kex: [
      'ecdh-sha2-nistp256',
      'diffie-hellman-group1-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha2-256',
      'hmac-md5',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};

const options = {
  worldManager: worldManagerConfig,
  sftpConfig,
  standardFileName: 'testing_standard.csv',
  customFileName: 'testing_custom.csv',
};

const worldManagerToPeopleStremeExport = new WorldManagerToPeopleStremeExport(options);

test('Export accounts created since yesterday', async (t) => {
  const response = await worldManagerToPeopleStremeExport.exportAccountsCreatedSinceYesterday();

  t.plan(2);
  t.equal(typeof response, 'object', 'Response should be an object.');
  t.equal(response.statusCode, 200, 'Status code is 200.');
});
