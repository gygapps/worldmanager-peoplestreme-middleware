const test = require('colored-tape');

const AlertsService = require('../../src/services/AlertsService');
const SlackAlertsService = require('../../src/services/SlackAlertsService');

const slackOptions = {
  webHook: '/services/TAA6ZKLMB/BE16R61LM/zHEbPBJmmVv9t1A5wbZmJFhG',
  msgPrefix: 'UNIT TEST',
};

const slackAlertService = new SlackAlertsService(slackOptions);

const alertsService = new AlertsService({ services: [slackAlertService, { anotherService: 'test' }] });

test('Slack error message alert works', async (t) => {
  const responses = await alertsService.sendError('Unit test');

  t.plan(3);
  t.equal(typeof responses, 'object', 'Responses is an object.');
  t.assert(responses.length > 0, 'Responses length is greater than 0.');
  t.equal(responses[0], 'ok', 'Slack response is ok.');
});
