const test = require('colored-tape');

const FileService = require('../../src/services/FileService');

test('Export data to file', async (t) => {
  const data = 'This is a test\n';
  const filepath = '/tmp/gyg-test.txt';

  const response = await FileService.exportDataToFile(data, filepath);

  const fileContent = await FileService.readFile(filepath);

  t.plan(2);
  t.equal(response, true);
  t.equal(data, fileContent, 'File contest is correct.');
});

test('Export data to invalid path throws error', async (t) => {
  const data = 'This is a test\n';
  const invalidFilepath = '\\/!#a';

  let errorNumber;

  try {
    await FileService.exportDataToFile(data, invalidFilepath);
  } catch (error) {
    errorNumber = error.errno;
  }

  t.plan(1);
  t.equal(errorNumber, -2, 'Error number is -2.');
});

test('Read invalid file throws error', async (t) => {
  const invalidFilepath = '\\/!#a';

  let errorNumber;

  try {
    await FileService.readFile(invalidFilepath);
  } catch (error) {
    errorNumber = error.errno;
  }

  t.plan(1);
  t.equal(errorNumber, -2, 'Error number is -2.');
});
