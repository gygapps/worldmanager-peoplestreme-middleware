const test = require('colored-tape');

const SFTPService = require('../../src/services/SFTPService');
const FileService = require('../../src/services/FileService');

// TODO
// This should be a test SFTP server
const SFTP_CONFIG = {
  host: 'peoplestreme.ftpstream.com',
  port: '22',
  username: 'gyg',
  password: 'cml*4C#^sp8VTy!@074aV1eXQ',
  // debug: console.log,
  algorithms: {
    kex: [
      'ecdh-sha2-nistp256',
      'diffie-hellman-group1-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha2-256',
      'hmac-md5',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};
/*
const SFTP_CONFIG1 = {
  host: 'sftp.preceda.com.au',
  port: '22',
  username: 'EFT_GYG_WM_INTEG_TEST',
  password: 'GYGPrWM1Test',
  // debug: console.log,
  algorithms: {
    kex: [
      'diffie-hellman-group-exchange-sha256',
      'diffie-hellman-group14-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha1',
      'hmac-sha2-512',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};
*/
const testFileContent = 'This is a test\n';

test('Upload local file to SFTP', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG);
  const localFilePath = '/tmp/test.txt';
  const remoteFilePath = 'test.txt';

  FileService.exportDataToFile(testFileContent, localFilePath);

  const response = await SFTPServiceInstance.uploadLocalFile(localFilePath, remoteFilePath);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, `${localFilePath} was successfully uploaded to ${remoteFilePath}!`, 'File uploaded to SFTP.');
});
/*
test('Upload local file to Preceda SFTP server', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG1);
  const localFilePath = 'dev_Preceda2WM_210624_uploaded.csv';
  const remoteFilePath = '/AUTO_INTEGRATION/Outbound/dev_Preceda2WM_210624_uploaded.csv';

  FileService.exportDataToFile(testFileContent, localFilePath);

  const response = await SFTPServiceInstance.uploadLocalFile(localFilePath, remoteFilePath);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, `${localFilePath} was successfully uploaded to ${remoteFilePath}!`, 'File uploaded to SFTP.');
});
*/
test('File exists in SFTP', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG);
  const remoteFilePath = 'test.txt';

  const response = await SFTPServiceInstance.fileExists(remoteFilePath);

  t.plan(2);
  t.equal(typeof response, 'boolean');
  t.equal(response, true, `File ${remoteFilePath} exists.`);
});

test('File does not exist in SFTP', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG);
  const remoteFilePath = 'INVALID_FILE.txt';

  const response = await SFTPServiceInstance.fileExists(remoteFilePath);

  t.plan(2);
  t.equal(typeof response, 'boolean');
  t.equal(response, false, `File ${remoteFilePath} does not exist.`);
});
/*
test('File exists in Preceda SFTP server', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG1);
  const remoteFilePath = 'PSToWmTest-V2.csv';

  const response = await SFTPServiceInstance.fileExists(remoteFilePath);

  t.plan(2);
  t.equal(typeof response, 'boolean');
  t.equal(response, false, `File ${remoteFilePath} exists.`);
});
*/
test('Get file as string from SFTP', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG);
  const remoteFilePath = 'test.txt';

  const response = await SFTPServiceInstance.getFileAsString(remoteFilePath);

  t.plan(2);
  t.equal(typeof response, 'string');
  t.equal(response, testFileContent, `File content is ${testFileContent}`);
});

test('Get invalid file from SFTP throws error', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG);
  const remoteFilePath = 'INVALID_FILE.txt';

  const response = await SFTPServiceInstance.getFileAsString(remoteFilePath);

  t.plan(1);
  t.equal(response, null, `File ${remoteFilePath} is invalid.`);
});

test('Download file from SFTP server', async (t) => {
  const SFTPServiceInstance = new SFTPService(SFTP_CONFIG);
  const localFilePath = '/tmp/downloadTest.txt';
  const remoteFilePath = 'test.txt';

  const response = await SFTPServiceInstance.downloadFile(remoteFilePath, localFilePath);

  t.plan(1);
  t.equal(response, true, `File ${remoteFilePath} downloaded to ${localFilePath}.`);
});
