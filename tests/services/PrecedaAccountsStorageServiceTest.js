const test = require('colored-tape');
const moment = require('moment');

const PrecedaAccountsStorage = require('../../src/services/PrecedaAccountsStorage');

const precedaAccountsStorage = new PrecedaAccountsStorage();

const UUID = Date.now().toString();

test('Save account', async (t) => {
  const account = {
    UUID,
    updatedAt: moment().format(),
  };

  const response = await precedaAccountsStorage.saveAccount(account);

  t.plan(2);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.statusCode === 200, 'Status code is 200.');
});

test('Get account by UUID', async (t) => {
  const response = await precedaAccountsStorage.getAccountByUUID(UUID);

  t.plan(3);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.statusCode === 200, 'Status code is 200.');
  t.assert(response.body.UUID === UUID, `UUID is ${UUID}.`);
});

test('Account exists', async (t) => {
  const response = await precedaAccountsStorage.accountExistsInTable(UUID);

  t.plan(2);
  t.equal(typeof response, 'boolean', 'Response is a boolean.');
  t.assert(response === true, 'Response is true.');
});

test('Account does not exist', async (t) => {
  const response = await precedaAccountsStorage.accountExistsInTable('AnUUIDThatDoesNotExist');

  t.plan(2);
  t.equal(typeof response, 'boolean', 'Response is a boolean.');
  t.assert(response === false, 'Response is false.');
});

test('Get multiple accounts', async (t) => {
  // Save test accounts
  await precedaAccountsStorage.saveAccount({ UUID: '1' });
  await precedaAccountsStorage.saveAccount({ UUID: '2' });
  await precedaAccountsStorage.saveAccount({ UUID: '3' });
  await precedaAccountsStorage.saveAccount({ UUID: '4' });
  await precedaAccountsStorage.saveAccount({ UUID: '5' });

  const uuids = ['1', '2', '3', '4', '5'];
  const response = await precedaAccountsStorage.getAccountsByUUIDs(uuids);

  t.plan(2);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.length, 5, 'Response contains 5 elements.');
});
