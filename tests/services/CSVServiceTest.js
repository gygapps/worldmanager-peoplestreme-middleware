const test = require('colored-tape');
const path = require('path');

const CSVService = require('../../src/services/CSVService');

test('Export to CSV', async (t) => {
  const fields = [
    { label: 'Car Name', value: 'car' },
    { label: 'Price USD', value: 'price' },
    { label: 'Color', value: 'color' },
  ];

  const data = [
    { car: 'Audi', price: 40000, color: 'light blue' },
    { car: 'BMW', price: 35000, color: 'black' },
    { car: 'Porsche', price: 60000, color: 'green' },
  ];

  const expected = '"Car Name","Price USD","Color"\n"Audi",40000,"light blue"\n"BMW",35000,"black"\n"Porsche",60000,"green"';

  const csv = await CSVService.exportToCSV(fields, data);

  t.plan(2);
  t.equal(typeof csv, 'string');
  t.equal(csv, expected);
});

test('Export to CSV without double quotes', async (t) => {
  const fields = [
    { label: 'Car Name', value: 'car' },
    { label: 'Price USD', value: 'price' },
    { label: 'Color', value: 'color' },
  ];

  const data = [
    { car: 'Audi', price: 40000, color: 'light blue' },
    { car: 'BMW', price: 35000, color: 'black' },
    { car: 'Porsche', price: 60000, color: 'green' },
  ];

  const expected = 'Car Name,Price USD,Color\nAudi,40000,light blue\nBMW,35000,black\nPorsche,60000,green';

  const csv = await CSVService.exportToCSVWithoutQuotes(fields, data);

  t.plan(2);
  t.equal(typeof csv, 'string');
  t.equal(csv, expected);
});

test('Read from CSV file', async (t) => {
  const filePath = path.join(__dirname, '..', 'services', 'precedaTest.csv');

  const headers = [
    'Employee ID Number',
    'Global ID 03',
    'Title',
    'First Name',
    'Surname',
    'Level 3 Description',
    'Personel Email Address',
    'Hire Date',
    'Mobile Phone Nu',
    'Employment Type Description',
    'Date of Birth',
    'Autopay',
    'Gender Desc',
    'Base Rate',
    'Emergency Contact Surname',
    'Emergency Contact Home Phone',
    'Salary Code Description',
    'Apprentice/Trainee',
    'Position Title',
    'Mon Notes',
    'Mon Notes Description',
    'Mon Information',
    'Tue Notes',
    'Tue Notes Description',
    'Tue Information',
    'Wed Notes',
    'Wed Notes Description',
    'Wed Information',
    'Thu Notes',
    'Thu Notes Description',
    'Thu Information',
    'Fri Notes',
    'Fri Notes Description',
    'Fri Information',
    'Sat Notes',
    'Sat Notes Description',
    'Sat Information',
    'Sun Notes',
    'Sun Notes Description',
    'Sun Information',
    'User Field 1',
    'User Field 2',
    'User Field 3',
    'User Field 4',
    'User Field 5',
    'User Field 6',
    'User Field 7',
    'State',
    'Status',
  ];

  const data = await CSVService.readFromCSVFile(filePath);

  t.equal(typeof data, 'object');

  // Check all headers are there
  for (let i = 0; i < headers.length; i += 1) {
    t.assert(Object.prototype.hasOwnProperty.call(data[0], headers[i]), `${headers[i]} found.`);
  }

  t.end();
});
