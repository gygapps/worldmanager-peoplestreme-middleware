const test = require('colored-tape');
const moment = require('moment');

const WorldManager = require('../../src/services/WorldManager');

const options = {
  tokenURL: 'https://guzmanygomez-playground.worldmanager.com/oauth/token',
  apiURL: 'https://guzmanygomez-playground.worldmanager.com/soap/v7/',
  clientId: 'fLliY6EClEO5CL9z',
  clientSecret: 'jil0GtJIx2DIX8vSYZprSW2QLkhUSYd3',
};

const worldManager = new WorldManager(options);

test('Parse XML error', async (t) => {
  const xmlError = `<?xml version='1.0' encoding='UTF-8'?>
                      <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
                        <soap:Body>
                          <soap:Fault>
                            <soap:Code>
                              <soap:Value>soap:Receiver</soap:Value>
                            </soap:Code>
                            <soap:Reason>
                              <soap:Text xml:lang="en">Code 86 - Position type was not found</soap:Text>
                            </soap:Reason>
                            <soap:Detail>
                              <ns2:WmError xmlns:ns2="http://v7.soap.platform.worldmanager.com/">
                                <code>86</code>
                                <description>Position type was not found</description>
                              </ns2:WmError>
                            </soap:Detail>
                          </soap:Fault>
                        </soap:Body>
                      </soap:Envelope>`;

  const response = await WorldManager.parseErrorResponse(xmlError);

  t.plan(2);
  t.equal(typeof response, 'string', 'Response is a string');
  t.equal(response, 'Code 86 - Position type was not found');
});

test('Find custom fields', async (t) => {
  const uuid = '486d6d37-bd9b-4076-a219-5974c29f1e5e';
  const customFields = await worldManager.findCustomFields(uuid);

  t.plan(42);
  t.equal(typeof customFields, 'object', 'customFields is an object');
  t.assert(customFields.length > 0, 'customFields should contain 1 or more elements');
  // Check field names
  t.assert(customFields[0].fieldName === '01. Crew Member\'s Date Of Birth (RM must complete when creating account)', '01. Crew Member\'s Date Of Birth (RM must complete when creating account)');
  t.assert(customFields[1].fieldName === '02. Gender', '02. Gender');
  t.assert(customFields[2].fieldName === '03. Position Title', '03. Position Title');
  t.assert(customFields[3].fieldName === '04. Employment Level', '04. Employment Level');
  t.assert(customFields[4].fieldName === '05. Salaried Employee', '05. Salaried Employee');
  t.assert(customFields[5].fieldName === '06. Salary', '06. Salary');
  t.assert(customFields[6].fieldName === '07. Trainee', '07. Trainee');
  t.assert(customFields[7].fieldName === '08. Monday Shift Time', '08. Monday Shift Time');
  t.assert(customFields[8].fieldName === '09. Monday Break Duration', '09. Monday Break Duration');
  t.assert(customFields[9].fieldName === '10. Monday Break Time', '10. Monday Break Time');
  t.assert(customFields[10].fieldName === '11. Monday Total Hours', '11. Monday Total Hours');
  t.assert(customFields[11].fieldName === '12. Tuesday Shift Time', '12. Tuesday Shift Time');
  t.assert(customFields[12].fieldName === '13. Tuesday Break Duration', '13. Tuesday Break Duration');
  t.assert(customFields[13].fieldName === '14. Tuesday Break Time', '14. Tuesday Break Time');
  t.assert(customFields[14].fieldName === '15. Tuesday Total Hours', '15. Tuesday Total Hours');
  t.assert(customFields[15].fieldName === '16. Wednesday Shift Time', '16. Wednesday Shift Time');
  t.assert(customFields[16].fieldName === '17. Wednesday Break Duration', '17. Wednesday Break Duration');
  t.assert(customFields[17].fieldName === '18. Wednesday Break Time', '18. Wednesday Break Time');
  t.assert(customFields[18].fieldName === '19. Wednesday Total Hours', '19. Wednesday Total Hours');
  t.assert(customFields[19].fieldName === '20. Thursday Shift Time', '20. Thursday Shift Time');
  t.assert(customFields[20].fieldName === '21. Thursday Break Duration', '21. Thursday Break Duration');
  t.assert(customFields[21].fieldName === '22. Thursday Break Time', '22. Thursday Break Time');
  t.assert(customFields[22].fieldName === '23. Thursday Total Hours', '23. Thursday Total Hours');
  t.assert(customFields[23].fieldName === '24. Friday Shift Time', '24. Friday Shift Time');
  t.assert(customFields[24].fieldName === '25. Friday Break Duration', '25. Friday Break Duration');
  t.assert(customFields[25].fieldName === '26. Friday Break Time', '26. Friday Break Time');
  t.assert(customFields[26].fieldName === '27. Friday Total Hours', '27. Friday Total Hours');
  t.assert(customFields[27].fieldName === '28. Saturday Shift Time', '28. Saturday Shift Time');
  t.assert(customFields[28].fieldName === '29. Saturday Break Duration', '29. Saturday Break Duration');
  t.assert(customFields[29].fieldName === '30. Saturday Break Time', '30. Saturday Break Time');
  t.assert(customFields[30].fieldName === '31. Saturday Total Hours', '31. Saturday Total Hours');
  t.assert(customFields[31].fieldName === '32. Sunday Shift Time', '32. Sunday Shift Time');
  t.assert(customFields[32].fieldName === '33. Sunday Break Duration', '33. Sunday Break Duration');
  t.assert(customFields[33].fieldName === '34. Sunday Break Time', '34. Sunday Break Time');
  t.assert(customFields[34].fieldName === '35. Sunday Total Hours', '35. Sunday Total Hours');
  t.assert(customFields[35].fieldName === '36. Emergency Contact: Person\'s Name', '36. Emergency Contact: Person\'s Name');
  t.assert(customFields[36].fieldName === '37. Emergency Contact: Person\'s phone number', '37. Emergency Contact: Person\'s phone number');
  t.assert(customFields[37].fieldName === '38. Preceda I.D', '38. Preceda I.D');
  t.assert(customFields[38].fieldName === '39.Training Academy - Career Interests', '39.Training Academy - Career Interests');
  t.assert(customFields[39].fieldName === '40. Training Academy - Qualifications', '40. Training Academy - Qualifications');
});

test('Find accounts with custom fields', async (t) => {
  const uuids = ['1a95e776-bfba-4064-b8fc-1626159cf7ff',
    '3e765308-8129-47e7-8473-73f91406ecb6',
    '8417b3e5-0473-4aad-8280-33f2dcc0261a',
    // 'fe8a3ef1-2a79-4544-81e2-a026cdf08de7', // This one throws error
    '4c586f22-fb1b-4f6b-abbc-886e7187374e'];

  const accountsWithCustomFields = await worldManager.findCustomFieldsByAccountIds(uuids);

  t.plan(2);
  t.equal(typeof accountsWithCustomFields, 'object');
  t.assert(accountsWithCustomFields.length > 0, 'Should contain 1 or more elements');
});

test('Find accounts with custom fields updated since yesterday', async (t) => {
  const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
  const accountsWithCustomFields = await worldManager.findAccountsWithCustomFieldsUpdatedSince(`${yesterday}T00:00:00Z`);

  t.plan(2);
  t.equal(typeof accountsWithCustomFields, 'object');
  t.assert(accountsWithCustomFields && accountsWithCustomFields.length > 0, 'Should contain 1 or more elements');
});

test('Update account with UUID 8039c572-a4a1-456d-bd13-38150f3f5db1', async (t) => {
  const accountData = {
    uuid: '8039c572-a4a1-456d-bd13-38150f3f5db1',
    groupId: '18',
    countryUuids: '698033d6-726b-45ee-8162-a8590557f488',
    areaUuids: '5eeaf42f-7a2a-482f-a947-682632c47a84',
    storeUuids: 'acec648e-6cf4-4c21-9054-f61cfcfb24da',
    username: 'Tansy',
    person: {
      title: 'MISS',
      firstName: 'Tansy Phoebe',
      middleName: 'middleName',
      lastName: 'Richards',
      nickname: 'Tansy',
      displayName: 'DisplayName',
    },
    active: 'false',
    language: 'English',
    positionName: null,
    positionType: 'Part Time',
    email: 'email@email.com',
    phone: '987654',
    mobile: '+6583681166',
    receiveEmails: 'true',
    timezone: {
      value: ['Asia/Singapore'],
      name: ['(GMT+08:00) Kuala Lumpur, Singapore'],
    },
    userDetailsUpdated: 'true',
    userPasswordUpdated: 'true',
    workStartDate: '13-01-2018',
    createdAt: '2018-04-25T11:08:52Z',
    updatedAt: '2019-07-26T01:38:47Z',
    customFields: [
      {
        uuid: '103d0acb-4f4f-4571-8495-d6f9fca4cdef',
        value: 'Emergency Contact: Persons Name',
      },
      {
        uuid: '2fc75864-4524-428f-920d-adbeff73d79d', // 01. Crew Member's Date Of Birth
        value: '1985-12-01',
      },
    ],
  };

  const updateAccountResponse = await worldManager.updateAccount(accountData);

  t.plan(1);
  t.assert(updateAccountResponse, 'Update account response is true.');
});

test('Get account with UUID 8039c572-a4a1-456d-bd13-38150f3f5db1', async (t) => {
  const uuid = '8039c572-a4a1-456d-bd13-38150f3f5db1';
  const countryUuid = '698033d6-726b-45ee-8162-a8590557f488';
  const areaUuid = '5eeaf42f-7a2a-482f-a947-682632c47a84';
  const storeUuid = 'acec648e-6cf4-4c21-9054-f61cfcfb24da';

  const account = await worldManager.getAccountByUuid(uuid);

  t.equal(typeof account, 'object');
  t.equal(account.uuid, uuid, `UUID is ${uuid}.`);
  t.equal(account.storeUuids[0], storeUuid, `Store UUID is ${storeUuid}.`);
  t.equal(account.countryUuids[0], countryUuid, `Country UUID is ${countryUuid}.`);
  t.equal(account.areaUuids[0], areaUuid, `Area UUID is ${areaUuid}.`);

  t.end();
});

test('Get custom fields', async (t) => {
  const customFields = await worldManager.getCustomFields();

  t.plan(4);
  t.equal(typeof customFields, 'object');
  t.assert(customFields.length > 0, 'Should contain 1 or more elements');
  t.assert(customFields[0].uuid, 'First element should contain uuid');
  t.assert(customFields[0].name, 'First element should contain name');
});

test('Find accounts with custom fields updated since yesterday', async (t) => {
  const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
  const accountsWithCustomFields = await worldManager.findAccountsWithCustomFieldsUpdatedSince(`${yesterday}T00:00:00Z`);

  t.plan(2);
  t.equal(typeof accountsWithCustomFields, 'object');
  t.assert(accountsWithCustomFields && accountsWithCustomFields.length > 0, 'Should contain 1 or more elements');
});

test('Find accounts created since yesterday', async (t) => {
  const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');

  const newAccounts = await worldManager.findAccountsCreatedSince(`${yesterday}T00:00:00Z`);

  t.plan(1);
  t.equal(typeof newAccounts, 'object', 'newAccounts should be an object.');
});

test('Find accounts updated since yesterday', async (t) => {
  const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
  const accounts = await worldManager.findAccountsUpdatedSince(`${yesterday}T00:00:00Z`);

  t.plan(2);
  t.equal(typeof accounts, 'object');
  t.assert(accounts && accounts.length > 0, 'Should contain 1 or more elements');
});

test('Update account with no Salary Code Description', async (t) => {
  const accountData = {
    uuid: '07a8ba55-392e-436a-9985-55e5bcbc7a98',
    groupId: '8',
    countryUuids: 'c9f4856f-c603-4d93-9b22-ed15266d8cc9',
    areaUuids: '93553f22-0cce-4f5c-a37b-ef3135c95509',
    storeUuids: '165838b2-106f-4f06-bd1d-83807d006d50',
    username: 'andresmurcia',
    person: {
      title: 'MR',
      firstName: 'Andres',
      middleName: 'Felipe',
      lastName: 'Diaz-Murcia',
      nickname: 'Andres',
      displayName: 'DisplayName',
    },
    active: 'true',
    language: 'English',
    positionName: 'Andres',
    positionType: 'Fulltime',
    email: 'andresdimur@gmail.com',
    phone: '0452552994',
    mobile: '0452552994',
    receiveEmails: 'true',
    timezone: {
      value: ['Asia/Singapore'],
      name: ['(GMT+08:00) Kuala Lumpur, Singapore'],
    },
    userDetailsUpdated: 'true',
    userPasswordUpdated: 'true',
    workStartDate: '14-01-2019',
    createdAt: '2018-03-07T05:31:36Z',
    updatedAt: '2019-11-13T04:00:04Z',
    customFields: [
      {
        uuid: '103d0acb-4f4f-4571-8495-d6f9fca4cdef',
        value: 'Emergency Contact: Persons Name',
      },
      {
        uuid: '2fc75864-4524-428f-920d-adbeff73d79d', // 01. Crew Member's Date Of Birth
        value: '1985-12-01',
      },
      {
        uuid: '6e3648df-a1fd-440f-b868-eb4dffc8eca2', // 04. Employment Level
        value: '',
      },
    ],
  };

  const updateAccountResponse = await worldManager.updateAccount(accountData);

  t.plan(1);
  t.assert(updateAccountResponse, 'Update account response is true.');
});

test('Valid value for custom field with no validValues list', async (t) => {
  const isValid = worldManager.isValidCustomFieldValue('103d0acb-4f4f-4571-8495-d6f9fca4cdef', 'some value');

  t.plan(1);
  t.equal(isValid, true);
});

test('Valid value for custom field with validValues list', async (t) => {
  const isValid = worldManager.isValidCustomFieldValue('6e3648df-a1fd-440f-b868-eb4dffc8eca2', 'FFIA LVL 1');

  t.plan(1);
  t.equal(isValid, true);
});

test('Valid values for gender custom field', async (t) => {
  t.plan(2);
  t.equal(worldManager.isValidCustomFieldValue('ad2e0282-12a5-4349-ba14-60f6aa0efd9b', 'M'), true);
  t.equal(worldManager.isValidCustomFieldValue('ad2e0282-12a5-4349-ba14-60f6aa0efd9b', 'F'), true);
});

test('Invalid value for custom field', async (t) => {
  const isValid = worldManager.isValidCustomFieldValue('6e3648df-a1fd-440f-b868-eb4dffc8eca2', '');

  t.plan(1);
  t.equal(isValid, false);
});

test('Invalid custom field', async (t) => {
  const isValid = worldManager.isValidCustomFieldValue('01234567-abcde-9876-qwer-zxcvvb', '');

  t.plan(1);
  t.equal(isValid, false);
});
