const test = require('colored-tape');
const moment = require('moment');

const WorldManagerAccounts = require('../../src/services/WorldManagerAccounts');

const worldManagerAccounts = new WorldManagerAccounts();

const UUID = Date.now().toString();

test('Save account', async (t) => {
  const account = {
    UUID,
    updatedAt: moment().format(),
  };

  const response = await worldManagerAccounts.saveAccount(account);

  t.plan(2);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.statusCode === 200, 'Status code is 200.');
});

test('Get account by UUID', async (t) => {
  const response = await worldManagerAccounts.getAccountByUUID(UUID);

  t.plan(3);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.statusCode === 200, 'Status code is 200.');
  t.assert(response.body.UUID === UUID, `UUID is ${UUID}.`);
});

test('Account exists', async (t) => {
  const response = await worldManagerAccounts.accountExistsInTable(UUID);

  t.plan(2);
  t.equal(typeof response, 'boolean', 'Response is a boolean.');
  t.assert(response === true, 'Response is true.');
});

test('Account does not exist', async (t) => {
  const response = await worldManagerAccounts.accountExistsInTable('AnUUIDThatDoesNotExist');

  t.plan(2);
  t.equal(typeof response, 'boolean', 'Response is a boolean.');
  t.assert(response === false, 'Response is false.');
});

test('Get multiple accounts', async (t) => {
  const uuids = ['1', '2', '3', '4', '5'];
  const response = await worldManagerAccounts.getAccountsByUUIDs(uuids);

  t.plan(2);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.length, 5, 'Response contains 5 elements.');
});
