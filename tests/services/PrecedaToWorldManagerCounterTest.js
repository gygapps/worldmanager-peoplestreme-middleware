const test = require('colored-tape');

const PrecedaToWorldManagerCounter = require('../../src/services/PrecedaToWorldManagerCounter');
const DynamoDB = require('../../src/services/DynamoDB');

const precedaToWorldManagerCounter = new PrecedaToWorldManagerCounter();

const dynamoDB = new DynamoDB();

test('Increment failed counter', async (t) => {
  // Add test row
  await dynamoDB.putItem('dev-PSExportCount', {
    Timestamp: '2019-08-12 00:00:00',
    InFile: 0,
    ToUpdate: 0,
    Failed: 0,
    Updated: 0,
  });

  const response = await precedaToWorldManagerCounter.incrementFailedCounter('2019-08-12 00:00:00');

  t.plan(2);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.Attributes.Failed > 0, 'Failed is greater than 0.');
});

test('Increment failed counter', async (t) => {
  // Add test row
  await dynamoDB.putItem('dev-PSExportCount', {
    Timestamp: '2019-08-12 00:00:00',
    InFile: 0,
    ToUpdate: 0,
    Failed: 0,
    Updated: 0,
  });

  const response = await precedaToWorldManagerCounter.incrementUpdatedCounter('2019-08-12 00:00:00');

  t.plan(2);
  t.equal(typeof response, 'object', 'Response is an object.');
  t.assert(response.Attributes.Updated > 0, 'Failed is greater than 0.');
});

test('Check if email was sent', async (t) => {
  const response = await precedaToWorldManagerCounter.emailWasSent('2019-08-12');

  t.plan(1);
  t.equal(response, true, 'Email sent');
});

test('Check if email was not sent', async (t) => {
  const response = await precedaToWorldManagerCounter.emailWasSent('2000-01-01');

  t.plan(1);
  t.equal(response, false, 'Email not sent');
});
