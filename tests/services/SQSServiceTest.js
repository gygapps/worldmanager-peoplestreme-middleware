const test = require('colored-tape');

const SQSService = require('../../src/services/SQSService');

const sqsService = new SQSService();

// Uncomment the URL you want to test
// const queueUrl = 'https://sqs.ap-southeast-2.amazonaws.com/740211342396/dev-PSAccounts'; // LR
const queueUrl = 'https://sqs.ap-southeast-2.amazonaws.com/032301510746/dev-PSAccounts'; // GyG

test('Send message', async (t) => {
  const options = {
    queueUrl,
    messageAttributes: {
      Title: {
        DataType: 'String',
        StringValue: 'The Whistler',
      },
      Author: {
        DataType: 'String',
        StringValue: 'John Grisham',
      },
      WeeksOn: {
        DataType: 'Number',
        StringValue: '6',
      },
    },
    messageBody: JSON.stringify('Information about current NY Times fiction bestseller for week of 12/11/2016.'),
  };

  let response;

  try {
    response = await sqsService.sendMessage(options);
  } catch (error) {
    console.error(error);
    t.equals(true, false, 'Error');
    t.end();
    return;
  }

  t.plan(2);
  t.equal(typeof response, 'object', 'Response should be an object.');
  t.assert(response.MessageId, 'Response contains MessageId.');
});
