const test = require('colored-tape');

const CSVStandard = require('../../src/services/CSVStandard');

const account = [
  {
    payrollId: '77d724f9-b5f3-45e1-8f36-a23a80290e4c',
    managerEmail: 'email@email.com',
    username: '',
    salutation: 'MISS',
    firstName: 'Siena',
    lastName: 'Zunic',
    gender: 3,
    positionTitle: 'aPositionTitle',
    email: 'email',
    privateEmail: 'email',
    mobile: '0455660903',
    jobCode: 'jobCode',
    startDate: '2019-06-26',
    terminationDate: 'terminationDate',
    dateOfBirth: '2003-03-09',
    highFrequencyReview: 'FALSE',
    ceo: 'FALSE',
    language: 1,
    eRecruitmentAccess: 'eRecruitmentAccess',
    streetAddress: 'streetAddress',
    suburb: 'suburb',
    postCode: 'postCode',
    state: 'state',
    country: 'country',
    homePhone: 'homePhone',
    emergencyContactName: 'Sharon Zunic',
    emergencyContactMobile: '0458888269',
    emergencyContactPhone: 'emergencyContactPhone',
    emergencyInstructions: 'emergencyInstructions',
    organisationLevel1: 'locationCode',
    organisationLevel2: 'ABN',
    organisationLevel3: 'stateCode',
    organisationLevel4: 'departmentCode',
    organisationLevel5: 'positionTypeCode',
    organisationLevel6: 'organisationLevel6',
    organisationLevel7: 'organisationLevel7',
    organisationLevel8: 'organisationLevel8',
    organisationLevel9: 'organisationLevel9',
    organisationLevel10: 'organisationLevel10',
    moduleAccessPerformance: 'moduleAccessPerformance',
    moduleAccessLearning: 'moduleAccessLearning',
    moduleAccessERecruitment: 'moduleAccessERecruitment',
    moduleAccessWorkforcePlanning: 'moduleAccessWorkforcePlanning',
    moduleAccessOnboarding: 'TRUE',
    moduleAccessTalentManagement: 'moduleAccessTalentManagement',
    moduleAccessRemuneration: 'moduleAccessRemuneration',
    moduleAccessSocialStreme: 'moduleAccessSocialStreme',
    moduleAccessSocialRecognition: 'moduleAccessSocialRecognition',
    moduleAccessEForms: 'moduleAccessEForms',
    moduleAccessUndefined: 'moduleAccessUndefined',
    moduleAccessEmployeeRecognition: 'moduleAccessEmployeeRecognition',
    moduleAccessIncidentManagement: 'moduleAccessIncidentManagement',
    moduleAccessCentralVue: 'moduleAccessCentralVue',
    moduleAccessDocuments: 'moduleAccessDocuments',
    moduleAccessEmployeeLifecycle: 'moduleAccessEmployeeLifecycle',
    moduleAccessExtendedCoreHR: 'moduleAccessExtendedCoreHR',
    moduleAccessOrgChart: 'moduleAccessOrgChart',
    moduleAccessJobLevel: 'moduleAccessJobLevel',
    moduleAccessFTE: 'moduleAccessFTE',
  },
];

test('Export to CSV', async (t) => {
  const expected = '"Payroll Id","Manager\'s Email","Username","Salutation","First Name","Last Name","Gender","Position Title","Email","Private Email","Mobile Phone Number","Job Code","Start Date (yyyy-MM-dd)","Termination Date (yyyy-MM-dd)","Date Of Birth (yyyy-MM-dd)","High Frequency Review","CEO","Language","eRecruitment Access","Street Address","Suburb","Postcode","State","Country","Home Phone","Emergency Contact Name","Emergency Contact Mobile","Emergency Contact Phone","Emergency Instructions","Organisation Level 1","Organisation Level 2","Organisation Level 3","Organisation Level 4","Organisation Level 5","Organisation Level 6","Organisation Level 7","Organisation Level 8","Organisation Level 9","Organisation Level 10","Module Access - Performance","Module Access - Learning","Module Access - eRecruitment","Module Access - Workforce Planning","Module Access - Onboarding","Module Access - Talent Management","Module Access - Remuneration","Module Access - SocialStreme","Module Access - Social Recognition","Module Access - eForms","Module Access - undefined","Module Access - Employee Recognition","Module Access - Incident Management","Module Access - CentralVue","Module Access - Documents","Module Access - Employee Lifecycle","Module Access - Extended Core HR","Module Access - Org Chart","Job Level","FTE"\n"77d724f9-b5f3-45e1-8f36-a23a80290e4c","email@email.com","","MISS","Siena","Zunic",3,"aPositionTitle","email","email","0455660903","jobCode","2019-06-26","terminationDate","2003-03-09","FALSE","FALSE",1,"eRecruitmentAccess","streetAddress","suburb","postCode","state","country","homePhone","Sharon Zunic","0458888269","emergencyContactPhone","emergencyInstructions","locationCode","ABN","stateCode","departmentCode","positionTypeCode","organisationLevel6","organisationLevel7","organisationLevel8","organisationLevel9","organisationLevel10","moduleAccessPerformance","moduleAccessLearning","moduleAccessERecruitment","moduleAccessWorkforcePlanning","TRUE","moduleAccessTalentManagement","moduleAccessRemuneration","moduleAccessSocialStreme","moduleAccessSocialRecognition","moduleAccessEForms","moduleAccessUndefined","moduleAccessEmployeeRecognition","moduleAccessIncidentManagement","moduleAccessCentralVue","moduleAccessDocuments","moduleAccessEmployeeLifecycle","moduleAccessExtendedCoreHR","moduleAccessOrgChart","moduleAccessJobLevel","moduleAccessFTE"';

  const csv = await CSVStandard.exportCSV(account);

  t.plan(2);
  t.equal(typeof csv, 'string');
  t.equal(csv, expected);
});

test('Export to CSV file', async (t) => {
  const filePath = '/tmp/CSVOnboardingTemplateServiceTest.csv';

  const fileResponse = await CSVStandard.exportCSVToFile(account, filePath);

  t.plan(2);
  t.equal(typeof fileResponse, 'boolean');
  t.equal(fileResponse, true);
});
