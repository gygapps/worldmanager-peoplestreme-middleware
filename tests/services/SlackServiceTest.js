const test = require('colored-tape');

const SlackAlertsService = require('../../src/services/SlackAlertsService');

const slackOptions = {
  webHook: '/services/TAA6ZKLMB/BE16R61LM/zHEbPBJmmVv9t1A5wbZmJFhG',
  msgPrefix: 'UNIT TEST',
};

test('Slack error message alert works', async (t) => {
  const slackAlertService = new SlackAlertsService(slackOptions);

  const response = await slackAlertService.sendError('Unit test');

  t.plan(2);
  t.equal(typeof response, 'string', 'Slack response is an object.');
  t.equal(response.toString(), 'ok', 'Slack response is ok.');
});
