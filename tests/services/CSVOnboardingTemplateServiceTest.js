const test = require('colored-tape');

const CSVOnboardingTemplate = require('../../src/services/CSVOnboardingTemplate');

const account = [
  {
    email: 'Siena.Zunic@gyg.com',
    positionId: '060606',
    roleTitle: 'Crew',
    trainee: 'Y',
    companyName: 'Guzman y Gomez Restaurant Group PTY LTD',
    award: 'Fast Food Industry Award 2010',
    level: 'FFIA LVL 1',
    positionType: 'Part Time Salaried',
    mondayShiftTime: '10.00am - 4.30pm',
    mondayBreakDuration: '30mins',
    mondayBreakTime: '2pm - 2.30pm',
    mondayTotalHours: '5.5',
    tuesdayShiftTime: '10.00am-4.30pm',
    tuesdayBreakDuration: '60mins',
    tuesdayBreakTime: '2pm - 2.30pm',
    tuesdayTotalHours: '8',
    wednesdayShiftTime: '10.00am-4.30pm',
    wednesdayBreakDuration: '30mins',
    wednesdayBreakTime: '2pm - 2.30pm',
    wednesdayTotalHours: '8',
    thursdayShiftTime: '10.00am-4.30pm',
    thursdayBreakDuration: '60mins',
    thursdayBreakTime: '2pm - 2.30pm',
    thursdayTotalHours: '8',
    fridayShiftTime: '2pm - 2.30pm',
    fridayBreakDuration: '30mins',
    fridayBreakTime: '10.00am-4.30pm',
    fridayTotalHours: '8',
    saturdayShiftTime: '2pm - 2.30pm',
    saturdayBreakDuration: '60mins',
    saturdayBreakTime: '10.00am-1.30pm',
    saturdayTotalHours: '7',
    sundayShiftTime: '1pm - 2.30pm',
    sundayBreakDuration: '34mins',
    sundayBreakTime: '10.00am-4.30pm',
    sundayTotalHours: '8',
    baseHours: 53.5,
    payrunGroup: 'HIRE',
    payFrequency: 'F',
    payMethod: 'B',
    autoPay: 'Y',
    autoPayHours: '28',
    payslipType: 'E',
    deliveryMethod: 'E',
    emailTo: 'P',
    salaryCode: 'salaryCode',
    rateFrequency: 'A',
    basePayRate: '55000',
    costLevel1: 'RG',
    costLevel2: 'VIC',
    costLevel3: '0606',
    costLevel4: '060606',
    costLevel5: '0010',
    employmentType: 2,
    personnelType: 'D',
    location: 'WERRIBEE',
    additionalFormRecipient: 'rmwrb@GyG.com.au',
    companyABN: 'ABN',
    account: 'account',
    department: 'department',
  },
];

test('Export to CSV', async (t) => {
  const expected = 'Email,Position_ID,Role_Title,Trainee,Company_Name,Award/EA,Level,Position_Type,Monday_Shift_Time,Monday_Break_Duration,Monday_Break_Time,Monday_Total_Hours,Tuesday_Shift_Time,Tuesday_Break_Duration,Tuesday_Break_Time,Tuesday_Total_Hours,Wednesday_Shift_Time,Wednesday_Break_Duration,Wednesday_Break_Time,Wednesday_Total_Hours,Thursday_Shift_Time,Thursday_Break_Duration,Thursday_Break_Time,Thursday_Total_Hours,Friday_Shift_Time,Friday_Break_Duration,Friday_Break_Time,Friday_Total_Hours,Saturday_Shift_Time,Saturday_Break_Duration,Saturday_Break_Time,Saturday_Total_Hours,Sunday_Shift_Time,Sunday_Break_Duration,Sunday_Break_Time,Sunday_Total_Hours,Base_Hours,Payrun_Group,Pay_Frequency,Pay_Method,Auto_Pay,Auto_Pay_Hours,Payslip_Type,Delivery_Method,Email_To,Salary_Code,Rate_Frequency,Base_Pay_Rate,Cost_Level_5,Employment_Type,Personnel_Type,Location,Additional_Form_Recipients,Cost_Level_1,Cost_Level_2,Cost_Level_3,Cost_Level_4,Department,Account\nSiena.Zunic@gyg.com,060606,Crew,Y,Guzman y Gomez Restaurant Group PTY LTD,Fast Food Industry Award 2010,FFIA LVL 1,Part Time Salaried,10.00am - 4.30pm,30mins,2pm - 2.30pm,5.5,10.00am-4.30pm,60mins,2pm - 2.30pm,8,10.00am-4.30pm,30mins,2pm - 2.30pm,8,10.00am-4.30pm,60mins,2pm - 2.30pm,8,2pm - 2.30pm,30mins,10.00am-4.30pm,8,2pm - 2.30pm,60mins,10.00am-1.30pm,7,1pm - 2.30pm,34mins,10.00am-4.30pm,8,53.5,HIRE,F,B,Y,28,E,E,P,salaryCode,A,55000,0010,2,D,WERRIBEE,rmwrb@GyG.com.au,RG,VIC,0606,060606,department,account';

  const csv = await CSVOnboardingTemplate.exportCSV(account);

  t.plan(2);
  t.equal(typeof csv, 'string');
  t.equal(csv, expected);
});

test('Export to CSV file', async (t) => {
  const filePath = '/tmp/CSVOnboardingTemplateServiceTest.csv';

  const fileResponse = await CSVOnboardingTemplate.exportCSVToFile(account, filePath);

  t.plan(2);
  t.equal(typeof fileResponse, 'boolean');
  t.equal(fileResponse, true);
});
