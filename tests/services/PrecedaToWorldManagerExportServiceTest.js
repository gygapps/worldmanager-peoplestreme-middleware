const test = require('colored-tape');

const WorldManager = require('../../src/services/WorldManager');
const PrecedaToWorldManagerMapping = require('../../src/services/PrecedaToWorldManagerMapping');
const PrecedaToWorldManagerExport = require('../../src/services/PrecedaToWorldManagerExport');
const CSVService = require('../../src/services/CSVService');
const SFTPService = require('../../src/services/SFTPService');
const AlertsService = require('../../src/services/AlertsService');
const SlackAlertsService = require('../../src/services/SlackAlertsService');
const PrecedaAccountsStorage = require('../../src/services/PrecedaAccountsStorage');
const SQSService = require('../../src/services/SQSService');
const PrecedaToWorldManagerCounter = require('../../src/services/PrecedaToWorldManagerCounter');

const options = {
  tokenURL: 'https://guzmanygomez-playground.worldmanager.com/oauth/token',
  apiURL: 'https://guzmanygomez-playground.worldmanager.com/soap/v7/',
  clientId: 'fLliY6EClEO5CL9z',
  clientSecret: 'jil0GtJIx2DIX8vSYZprSW2QLkhUSYd3',
};

const worldManager = new WorldManager(options);

const precedaToWorldManagerMapping = new PrecedaToWorldManagerMapping({ worldManager });

const sftpConfig = {
  host: 'sftp.preceda.com.au',
  port: '22',
  username: 'EFT_GYG_WM_INTEG_TEST',
  password: 'GYGPrWM1Test',
  // debug: console.log, // Uncomment this for debug output
  algorithms: {
    kex: [
      'diffie-hellman-group-exchange-sha256',
      'diffie-hellman-group14-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha1',
      'hmac-sha2-512',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};

const sftpService = new SFTPService(sftpConfig);

const slackOptions = {
  webHook: '/services/TAA6ZKLMB/BE16R61LM/zHEbPBJmmVv9t1A5wbZmJFhG',
  msgPrefix: 'UNIT TEST',
};

const slackAlertService = new SlackAlertsService(slackOptions);

const alertsService = new AlertsService({ services: [slackAlertService] });

const precedaAccountsStorage = new PrecedaAccountsStorage();

const sqsService = new SQSService();

const precedaToWorldManagerCounter = new PrecedaToWorldManagerCounter();

// Uncomment the URL you want to test
const queueUrl = 'https://sqs.ap-southeast-2.amazonaws.com/032301510746/dev-PSAccounts'; // GyG
// const queueUrl = 'https://sqs.ap-southeast-2.amazonaws.com/740211342396/dev-PSAccounts'; // LR

const precedaToWorldManagerExport = new PrecedaToWorldManagerExport({
  worldManager,
  precedaToWorldManagerMapping,
  SFTPService: sftpService,
  CSVService,
  remoteFileDirectory: '/AUTO_INTEGRATION/Outbound/',
  remoteFileName: 'test_Preceda2WM_YYMMDDhhmm.csv',
  alertsService,
  precedaAccountsStorage,
  sqsService,
  queueUrl,
  precedaToWorldManagerCounter,
});

test('Read CSV file and send to queue', async (t) => {
  // Reset hash for each test account
  await precedaAccountsStorage.saveAccount({ UUID: 'b0def917-332c-42e1-96ac-9e8b76a805a1', PSHash: 'a' });
  await precedaAccountsStorage.saveAccount({ UUID: 'badddce1-1f97-42c2-bd0f-792ca50f2fb8', PSHash: 'a' });
  await precedaAccountsStorage.saveAccount({ UUID: '6f68a12b-e3d2-4cbe-871b-11e6bff55906', PSHash: 'a' });

  const expectedSentToQueue = 3;
  let response;

  try {
    response = await precedaToWorldManagerExport.getAccountsFromCSVAndSendToQueue({ remoteFilePath: '/AUTO_INTEGRATION/INBOUND/PSToWmTest.csv' });
  } catch (error) {
    console.error(error);
    t.equals(true, false, 'Error');
    t.end();
    return;
  }

  t.plan(2);
  t.equal(typeof response, 'object', 'Response should be an object.');
  t.equal(response.toUpdate, expectedSentToQueue, `Sent ${expectedSentToQueue} accounts to queue.`);
});

test('Map PS fields and export to WM', async (t) => {
  const account = {
    'Employee ID Number': '3',
    'Global ID 03': '6f68a12b-e3d2-4cbe-871b-11e6bff55906',
    Title: 'MR',
    'First Name': 'Mark',
    Surname: 'Hawthorne',
    'Level 3 Description': 'WORLD SQUARE',
    'Personel Email Address': 'mailfarhad@yahoo.com',
    'Hire Date': '15/12/2010',
    'Mobile Phone Number': '430934059',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '24/11/1970',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '56815',
    'Emergency Contact Surname': 'Charlie',
    'Emergency Contact Home Phone': '424356789',
    'Salary Code Description': 'FFIA LVL 1',
    'Apprentice/Trainee': 'No',
    'Position Title': '',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '9.00am-9.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-9.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': 'No Contract Hrs',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': 'No Contract Hrs',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': '6.00pm-9.00pm',
    'Sun Information': '',
    'Contracted Hrs Mon': '11',
    'Contracted Hrs Tue': '12',
    'Contracted Hrs Wed': '13',
    'Contracted Hrs Thu': '14',
    'Contracted Hrs Fri': '10',
    'Contracted Hrs Sat': '15',
    'Contracted Hrs Sun': '16',
    State: 'NSW',
    Status: 'A',
  };

  const expectedUUID = account['Global ID 03'];

  const response = await precedaToWorldManagerExport.mapFieldsAndExportToWM(account);

  t.plan(2);
  t.equal(typeof response, 'object', 'Response should be an object.');
  t.equal(response.uuid, expectedUUID, `UUID is ${expectedUUID}.`);
});

test('Account with invalid restaurant returns an error', async (t) => {
  const account = {
    'Employee ID Number': '3',
    'Global ID 03': '6f68a12b-e3d2-4cbe-871b-11e6bff55906',
    Title: 'MR',
    'First Name': 'Mark',
    Surname: 'Hawthorne',
    'Level 3 Description': 'INVALID_RESTAURANT',
    'Personel Email Address': 'mailfarhad@yahoo.com',
    'Hire Date': '15/12/2010',
    'Mobile Phone Number': '430934059',
    'Employment Type Description': 'Part Time',
    'Date of Birth': '24/11/1970',
    Autopay: 'N',
    'Gender Desc': 'Male',
    'Base Rate': '56815',
    'Emergency Contact Surname': 'Charlie',
    'Emergency Contact Home Phone': '424356789',
    'Salary Code Description': 'FFIA LVL 1',
    'Apprentice/Trainee': 'No',
    'Position Title': 'Crew',
    'Mon Notes': '1. MON',
    'Mon Notes Description': '6.00pm-9.00pm',
    'Mon Information': '',
    'Tue Notes': '2. TUE',
    'Tue Notes Description': 'No Contract Hrs',
    'Tue Information': '',
    'Wed Notes': '3. WED',
    'Wed Notes Description': 'No Contract Hrs',
    'Wed Information': '',
    'Thu Notes': '4. THU',
    'Thu Notes Description': '6.00pm-9.00pm',
    'Thu Information': '',
    'Fri Notes': '5. FRI',
    'Fri Notes Description': 'No Contract Hrs',
    'Fri Information': '',
    'Sat Notes': '6. SAT',
    'Sat Notes Description': 'No Contract Hrs',
    'Sat Information': '',
    'Sun Notes': '7. SUN',
    'Sun Notes Description': '6.00pm-9.00pm',
    'Sun Information': '',
    'Contracted Hrs Mon': '11',
    'Contracted Hrs Tue': '12',
    'Contracted Hrs Wed': '13',
    'Contracted Hrs Thu': '14',
    'Contracted Hrs Fri': '10',
    'Contracted Hrs Sat': '15',
    'Contracted Hrs Sun': '16',
    State: 'NSW',
    Status: 'A',
  };

  const expectedError = 'INVALID_RESTAURANT is not a valid restaurant.';

  const response = await precedaToWorldManagerExport.mapFieldsAndExportToWM(account);

  t.plan(2);
  t.equal(typeof response, 'object', 'Response should be an object.');
  t.equal(response.error, expectedError, `Error message is ${expectedError}`);
});


test('Update World Manager accounts from Preceda CSV file', async (t) => {
  // Reset hash for each test account
  await precedaAccountsStorage.saveAccount({ UUID: 'b0def917-332c-42e1-96ac-9e8b76a805a1', PSHash: 'a' });
  await precedaAccountsStorage.saveAccount({ UUID: 'badddce1-1f97-42c2-bd0f-792ca50f2fb8', PSHash: 'a' });
  await precedaAccountsStorage.saveAccount({ UUID: '6f68a12b-e3d2-4cbe-871b-11e6bff55906', PSHash: 'a' });

  const expectedFoundAccounts = 3;
  const response = await precedaToWorldManagerExport.exportAccounts({ remoteFilePath: '/AUTO_INTEGRATION/INBOUND/PSToWmTest.csv' });

  t.plan(3);
  t.equal(typeof response, 'object');
  t.equal(response.foundAccounts, expectedFoundAccounts, `${expectedFoundAccounts} accounts found.`);
  t.equal(response.failedUpdates, 0, 'No failed updated.');
});

test('Update World Manager accounts in chunks from Preceda CSV file', async (t) => {
  // Reset hash for each test account
  await precedaAccountsStorage.saveAccount({ UUID: 'b0def917-332c-42e1-96ac-9e8b76a805a1', PSHash: 'a' });
  await precedaAccountsStorage.saveAccount({ UUID: 'badddce1-1f97-42c2-bd0f-792ca50f2fb8', PSHash: 'a' });
  await precedaAccountsStorage.saveAccount({ UUID: '6f68a12b-e3d2-4cbe-871b-11e6bff55906', PSHash: 'a' });

  const response1 = await precedaToWorldManagerExport.exportAccounts({
    offset: 0,
    limit: 1,
    remoteFilePath: '/AUTO_INTEGRATION/INBOUND/PSToWmTest.csv',
  });

  const response2 = await precedaToWorldManagerExport.exportAccounts({
    offset: 1,
    limit: 3,
    remoteFilePath: '/AUTO_INTEGRATION/INBOUND/PSToWmTest.csv',
  });

  const response3 = await precedaToWorldManagerExport.exportAccounts({
    offset: 3,
    limit: 2,
    remoteFilePath: '/AUTO_INTEGRATION/INBOUND/PSToWmTest.csv',
  });

  t.equal(typeof response1, 'object', 'response1 should be an object.');
  t.equal(response1.foundAccounts, 1, '1 account found.');
  t.equal(response1.failedUpdates, 0, 'No failed updated.');
  t.equal(typeof response2, 'object', 'response2 should be an object.');
  t.equal(response2.foundAccounts, 2, '2 accounts found.');
  t.equal(response2.failedUpdates, 0, 'No failed updated.');
  t.equal(typeof response3, 'object', 'response3 should be an object.');
  t.equal(response3.foundAccounts, 0, '0 accounts found.');
  t.equal(response3.failedUpdates, 0, 'No failed updated.');
  t.end();
});

test('Do not update if accounts have not changed', async (t) => {
  // Reset hash for one test account
  await precedaAccountsStorage.saveAccount({ UUID: 'b0def917-332c-42e1-96ac-9e8b76a805a1', PSHash: 'a' });

  const expectedUpdatedAccounts = 1;
  const expectedUnchangedAccounts = 2;
  const response = await precedaToWorldManagerExport.exportAccounts({
    remoteFilePath: '/AUTO_INTEGRATION/INBOUND/PSToWmTest.csv',
  });

  t.plan(3);
  t.equal(typeof response, 'object');
  t.equal(response.updatedAccounts, expectedUpdatedAccounts, `${expectedUpdatedAccounts} updated accounts.`);
  t.equal(response.unchangedAccounts, expectedUnchangedAccounts, `${expectedUnchangedAccounts} unchanged accounts.`);
});

test('Test csv file can be found on Preceda SFTP server', async (t) => {
  // Using a hard-coded date so test works no matter which day it runs
  const response = await precedaToWorldManagerExport.getRemoteFilePath('1901010000');
  t.equal(typeof response, 'string', 'response should be a string.');
  t.end();
});
