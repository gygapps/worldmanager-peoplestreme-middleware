#!/bin/bash

# Set default opts
STAGE="dev"

# Parse arguments
for i in "$@"
do
case $i in
    -s=*|--stage=*)
    STAGE="${i#*=}"
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done

parent_path=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)

cd "$parent_path"

for file in ../dynamoDB/*.json
do
 # Replace table name by adding stage
 # Create new file
 new_filename="../dynamoDB/$(basename $file).${STAGE}"
 sed "s/#STAGE#/${STAGE}/g" ${file} > ${new_filename}

 echo "dynamodb batch-write-item ${new_filename}"
 aws dynamodb batch-write-item --request-items file://"${new_filename}" --profile jvravideploys --region ap-southeast-2

 # Remove file
 rm ${new_filename}
done
