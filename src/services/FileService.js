const fs = require('fs');

class FileService {
  /**
   * Export data to file
   *
   * @param {*} data
   * @param {*} filepath
   */
  static exportDataToFile(data, filepath) {
    return new Promise((resolve, reject) => {
      fs.writeFile(filepath, data, (err) => {
        if (err) {
          return reject(err);
        }
        return resolve(true);
      });
    });
  }

  /**
   * Read file to a string
   *
   * @param {*} filepath
   */
  static readFile(filepath) {
    return new Promise((resolve, reject) => {
      fs.readFile(filepath, (err, buf) => {
        if (err) {
          return reject(err);
        }
        return resolve(buf.toString());
      });
    });
  }
}

module.exports = FileService;
