const moment = require('moment-timezone');
const _ = require('lodash');
const hash = require('object-hash');

const Restaurant = {
  HeadOffice: 'HEAD OFFICE',
  Franchising: 'Franchising',
};

/**
 * Class to export Preceda accounts to World Manager
 *
 */
class PrecedaToWorldManagerExport {
  constructor(options) {
    this.worldManager = options.worldManager;
    this.precedaToWorldManagerMapping = options.precedaToWorldManagerMapping;
    this.SFTPService = options.SFTPService;
    this.CSVService = options.CSVService;
    this.remoteFileDirectory = options.remoteFileDirectory || '/AUTO_INTEGRATION/Outbound/';
    this.remoteFileName = options.remoteFileName || 'dev_Preceda2WMv1_yyMMddHHMM.csv';
    this.alertsService = options.alertsService || null;
    this.precedaAccountsStorage = options.precedaAccountsStorage;
    this.precedaToWorldManagerCounter = options.precedaToWorldManagerCounter;
    this.sqsService = options.sqsService;
    this.queueUrl = options.queueUrl || process.env.PS_ACCOUNTS_QUEUE_URL; // Get default from env variable
  }

  /**
   * Read Preceda accounts from an SFTP server and
   * send accounts to a queue
   *
   * @param {Object} options
   */
  async getAccountsFromCSVAndSendToQueue(options) {
    const response = await this.getAccountsFromCSV(options);

    // Save count in DB
    if (response.count) {
      await this.precedaToWorldManagerCounter.saveCount(response.count);
    }

    // Send accounts to queue
    if (response && response.sendToQueue) {
      for (let i = 0; i < response.sendToQueue.length; i += 1) {
        const { account } = response.sendToQueue[i];
        const { timestamp } = response.count;
        console.log(`Sending account ${account['Global ID 03']} to queue.`);
        await this.sendAccountToQueue(response.sendToQueue[i].account, timestamp);
      }
    }

    return response.count;
  }

  /**
   * Read Preceda accounts from an SFTP server
   *
   * @param {Object} options
   */
  async getAccountsFromCSV(options) {
    // Counters
    const count = {
      allAccounts: 0, // All accounts found in CSV file
      changedAccounts: 0, // Accounts that changed since last time this function ran
      unchangedAccounts: 0, // Accounts that didn't change since last time this function ran
      toUpdate: 0, // Accounts that should be updated in WM
      timestamp: moment().tz('Australia/Sydney').format('YYYY-MM-DD HH:mm:ss'),
    };

    // Array of accounts to send to queue
    const sendToQueue = [];

    const remoteFilePath = _.has(options, 'remoteFilePath') ? options.remoteFilePath : await this.getRemoteFilePath();

    // Read CSV file
    const allAccounts = await this.getDataFromCSVFile(remoteFilePath);

    // No account found
    if (!allAccounts || allAccounts.length === 0) {
      console.log(`No accounts found in file ${remoteFilePath}.`);
      return {
        foundAccounts: 0,
        updatedAccounts: 0,
        failedUpdates: 0,
        unchangedAccounts: 0,
      };
    }

    console.log(`Found ${allAccounts.length} accounts in file ${remoteFilePath}.`);

    // All accounts found in CSV file
    count.allAccounts = allAccounts.length;

    // If we have limit and offset, get a portion of array
    let accounts;
    if (options && _.has(options, 'limit') && options.limit && _.has(options, 'offset') && options.offset) {
      console.log('options', options);
      console.log(`Looking for ${options.limit} accounts starting from position ${options.offset}.`);
      accounts = allAccounts.slice(options.offset, options.limit);
      console.log(`Found ${accounts.length} accounts to process.`);
    } else {
      accounts = allAccounts;
    }

    // Filter the accounts that haven't changed since the last time this ran
    const { changedAccounts, unchangedAccounts } = await this.filterUnchangedAccounts(accounts);

    // Changed and unchanged accounts count
    count.changedAccounts = changedAccounts.length;
    count.unchangedAccounts = unchangedAccounts.length;

    if (unchangedAccounts && unchangedAccounts.length > 0) {
      console.log(`Found ${unchangedAccounts.length} accounts that didn't change since last run.`);
    }

    // For each CSV row
    // Call API to get account
    // Call API to update account
    if (changedAccounts && changedAccounts.length > 0) {
      for (let i = 0; i < changedAccounts.length; i += 1) {
        const account = changedAccounts[i];
        if (PrecedaToWorldManagerExport.shouldProcessAccount(account)) {
          try {
            // Save account so we can send to a queue later
            sendToQueue.push({ account, timestamp: count.timestamp });
            count.toUpdate += 1;
          } catch (error) {
            // Send alerts
            if (this.alertsService) {
              this.alertsService.sendError(`Failed sending to queue account ${account['Global ID 03']}.`);
              this.alertsService.sendError(error);
            }

            console.error(error);
          }
        } else {
          console.log(`Not processing account ${account['Global ID 03']}.`);
        }
      }
    } else {
      console.log('No updated accounts found.');
    }

    console.log('Done!');

    return {
      count,
      sendToQueue,
    };
  }

  /**
   * Map PS fields to WM and call WP API to update
   *
   * @param {Object} account
   */
  async mapFieldsAndExportToWM(account) {
    if (!account) {
      return null;
    }

    if (PrecedaToWorldManagerExport.shouldProcessAccount(account)) {

      try {
        console.log(`Updating account ${account['Global ID 03']}.`);

        // Map fields
        const mappedData = await this.precedaToWorldManagerMapping.precedaFileMapping(account);

        // Check if mapping was OK
        // If there is no store UUID it means that restaurant is not valid
        if (!mappedData || !mappedData.storeUuids) {
          console.log(`Couldn't map account ${account['Global ID 03']}.`);

          // Couldn't map
          if (!mappedData) {
            return {
              error: `Couldn't map account ${account['Global ID 03']}.`,
            };
          }

          // Invalid restaurant
          if (!mappedData.storeUuids) {
            const errorMsg = `${account['Level 3 Description']} is not a valid restaurant.`;
            console.error(errorMsg);
            return {
              error: errorMsg,
            };
          }
        }

        if(mappedData.active === 'false') {
          console.error(`Restaurant ${account['Level 3 Description']} is not active.`);
          return {
           error: `Restaurant ${account['Level 3 Description']} is not active.`,
          };
        }

        // Get account from WM API
        const WMAccount = await this.worldManager.getAccountByUuid(mappedData.uuid);

        // No WM account found so don't process
        if (!WMAccount) {
          console.log(`Couldn't find account ${account['Global ID 03']} in World Manager.`);

          return {
            error: `Couldn't find account ${account['Global ID 03']} in World Manager.`,
          };
        }

        // Set group ID and username from WM
        mappedData.groupId = WMAccount.groupId;
        mappedData.username = WMAccount.username;

        // Set person details
        mappedData.person.middleName = WMAccount.person.middleName;
        mappedData.person.nickname = WMAccount.person.nickname;
        mappedData.person.displayName = WMAccount.person.displayName;

        // Call API to update account
        const response = await this.worldManager.updateAccount(mappedData);

        if (!response || response.error) {
          // Send alerts
          if (this.alertsService) {
            await this.alertsService.sendError(`Failed updating account ${account['Global ID 03']}.`);
            await this.alertsService.sendError(response.error);
          }

          console.error(`Failed updating account ${account['Global ID 03']}.`);

          return {
            error: `Error from World Manager API: ${response.error}`,
          };
        }

        console.log(`Updated account ${account['Global ID 03']}.`);

        return mappedData;
      } catch (error) {
        // Send alerts
        if (this.alertsService) {
          this.alertsService.sendError(`Failed updating account ${account['Global ID 03']}.`);
          this.alertsService.sendError(error);
        }
        console.error(error);
        return {
          error: error.message,
        };
      }
    } else {
      console.log(`Not processing account ${account['Global ID 03']}.`);
      return false;
    }
  }

  /**
   * Send account to queue
   *
   * @param {Array} account
   * @param {string} timestamp
   */
  async sendAccountToQueue(account, timestamp) {
    const options = {
      queueUrl: this.queueUrl,
      messageAttributes: {
        UUID: {
          DataType: 'String',
          StringValue: account['Global ID 03'],
        },
        SentDate: {
          DataType: 'String',
          StringValue: timestamp,
        },
      },
      messageBody: JSON.stringify(account),
    };

    return this.sqsService.sendMessage(options);
  }

  /**
   * Export accounts from PS to WM
   *
   * @param {Object} options
   */
  async exportAccounts(options) {
    const updatedAccounts = [];
    let failedUpdates = 0;

    // If there is a remote file path in options use that, otherwise call function to get it
    const remoteFilePath = _.has(options, 'remoteFilePath') ? options.remoteFilePath : await this.getRemoteFilePath();

    // Read CSV file
    const allAccounts = await this.getDataFromCSVFile(remoteFilePath);

    // No account found
    if (!allAccounts || allAccounts.length === 0) {
      console.log(`No accounts found in file ${remoteFilePath}.`);
      return {
        foundAccounts: 0,
        updatedAccounts: 0,
        failedUpdates: 0,
        unchangedAccounts: 0,
      };
    }

    console.log(`Found ${allAccounts.length} accounts in file ${remoteFilePath}.`);

    // If we have limit and offset, get a portion of array
    let accounts;
    if (options && _.has(options, 'limit') && _.has(options, 'offset')) {
      console.log(`Looking for ${options.limit} accounts starting from position ${options.offset}.`);
      accounts = allAccounts.slice(options.offset, options.limit);
      console.log(`Found ${accounts.length} accounts to process.`);
    } else {
      accounts = allAccounts;
    }

    // Filter the accounts that haven't changed since the last time this ran
    const { changedAccounts, unchangedAccounts } = await this.filterUnchangedAccounts(accounts);

    if (unchangedAccounts && unchangedAccounts.length > 0) {
      console.log(`Found ${unchangedAccounts.length} accounts that didn't change since last run.`);
    }

    // For each CSV row
    // Call API to get account
    // Call API to update account
    if (changedAccounts && changedAccounts.length > 0) {
      for (let i = 0; i < changedAccounts.length; i += 1) {
        const account = changedAccounts[i];

        // Map fields
        const mappedData = await this.precedaToWorldManagerMapping.precedaFileMapping(account);

        if(mappedData.active === 'false') {
              console.error(`Restaurant ${account['Level 3 Description']} is not active.`);
        } else if (PrecedaToWorldManagerExport.shouldProcessAccount(account)) {
          try {
            console.log(`Updating account ${account['Global ID 03']}.`);

            // Check if mapping was OK
            // If there is no store UUID it means that restaurant is not valid
            if (!mappedData || !mappedData.storeUuids) {
              console.log(`Couldn't map account ${account['Global ID 03']}.`);
            } else {
              // Get account from WM API
              const WMAccount = await this.worldManager.getAccountByUuid(mappedData.uuid);

              // No WM account found so don't process
              if (!WMAccount) {
                console.log(`Couldn't find account ${account['Global ID 03']} in World Manager.`);
              } else {
                // Set group ID and username from WM
                mappedData.groupId = WMAccount.groupId;
                mappedData.username = WMAccount.username;

                // Set person details
                mappedData.person.middleName = WMAccount.person.middleName;
                mappedData.person.nickname = WMAccount.person.nickname;
                mappedData.person.displayName = WMAccount.person.displayName;

                // Call API to update account
                const response = await this.worldManager.updateAccount(mappedData);

                if (!response || response.error) {
                  // Send alerts
                  if (this.alertsService) {
                    await this.alertsService.sendError(`Failed updating account ${account['Global ID 03']}.`);
                    await this.alertsService.sendError(response.error);
                  }

                  console.error(`Failed updating account ${account['Global ID 03']}.`);
                  failedUpdates += 1;
                } else {
                  console.log(`Updated account ${account['Global ID 03']}.`);
                  // updatedAccounts += 1;
                  updatedAccounts.push(account);
                }
              }
            }
          } catch (error) {
            // Send alerts
            if (this.alertsService) {
              this.alertsService.sendError(`Failed updating account ${account['Global ID 03']}.`);
              this.alertsService.sendError(error);
            }
            console.error(error);
          }
        } else {
          console.log(`Not processing account ${account['Global ID 03']}.`);
        }
      }
    } else {
      console.log('No updated accounts found.');
    }

    // Save updated accounts in DB
    await this.saveUpdatedAccounts(updatedAccounts);

    console.log('Done!');

    return {
      foundAccounts: accounts.length, // Number of accounts found in CSV file
      updatedAccounts: updatedAccounts.length, // Number of accounts actually updated in WM
      failedUpdates, // Number of accounts that failed trying to update
      unchangedAccounts: unchangedAccounts.length, // Number of accounts that hadn't changed since last run
    };
  }

  /**
   * Check if we should process the account
   *
   * @param {Object} account
   */
  static shouldProcessAccount(account) {
    // Do not process empty account or from Head Office or from Franchising
    if (!account || account['Level 3 Description'] === Restaurant.HeadOffice
        || account['Level 3 Description'] === Restaurant.Franchising) {
      return false;
    }

    // Do not process account with empty UUID
    if (account['Global ID 03'] === '') {
      return false;
    }

    return true;
  }

  /**
   * Get data from remote CSV file
   *
   * @param {string} remoteFilePath
   */
  async getDataFromCSVFile(remoteFilePath) {
    const localFilePath = '/tmp/downloadedFile.csv';
    const SFTPResponse = await this.SFTPService.downloadFile(remoteFilePath, localFilePath);

    if (!SFTPResponse) {
      console.error(`Error downloading file ${remoteFilePath}.`);
      return null;
    }

    return this.CSVService.readFromCSVFile(localFilePath);
  }

  /**
   * Get remote file path
   *
   * @param {string} fileDate
   */
  async getRemoteFilePath(fileDate = null) {
    let today = fileDate;
    if (!today) {
      today = moment().tz('Australia/Sydney').format('YYMMDD');
    }

    const searchString = this.remoteFileName.split('YYMMDDhhmm').shift() + today;
    const re = new RegExp(`(?:${searchString})`);

    const SFTPResponse = await this.SFTPService.getAllFiles(this.remoteFileDirectory);
    const filterFiles = _.filter(SFTPResponse, obj => re.test(obj.name));

    if (filterFiles.length > 1) {
      console.error(`Multiple .csv files found for processing. Cannot determine which file in: ${filterFiles} is required. Ensure there is only 1 file with the same date time stamp`);
      return false;
    }

    if (!filterFiles || filterFiles.length === 0) {
      console.error(`No files found for processing. ${searchString} used for file search did not yield results`);
      return false;
    }

    return this.remoteFileDirectory + filterFiles[0].name;
  }

  /**
   * Filter accounts that haven't changed
   *
   * @param {Array} accounts
   */
  async filterUnchangedAccounts(accounts) {
    const BATCH_GET_ITEM_LIMIT = 100;
    const changedAccounts = [];
    const unchangedAccounts = [];
    // Get accounts from DynamoDB table and compare to see which ones changed
    const chunks = _.chunk(accounts, BATCH_GET_ITEM_LIMIT);

    // eslint-disable-next-line no-restricted-syntax
    for (const chunk of chunks) {
      // Get array of UUIDs
      const uuids = _.map(chunk, 'Global ID 03');

      // Get from database
      const DBAccounts = await this.precedaAccountsStorage.getAccountsByUUIDs(uuids);

      // If we don't find any accounts, then we need to process all
      if (!DBAccounts || DBAccounts.length === 0) {
        chunk.forEach((PSAccount) => {
          changedAccounts.push(PSAccount);
        });
      } else {
        // For each account, compare hash
        // eslint-disable-next-line no-loop-func
        chunk.forEach((PSAccount) => {
          const accountInDB = _.find(DBAccounts, { UUID: PSAccount['Global ID 03'] });

          // Account not found or hash is different, we need to process it
          if (!accountInDB || accountInDB.PSHash !== hash(PSAccount)) {
            changedAccounts.push(PSAccount);
          } else {
            console.log(`Not updating account ${PSAccount['Global ID 03']} because it did not change since last run.`);
            unchangedAccounts.push(PSAccount);
          }
        });
      }
    }
    return { changedAccounts, unchangedAccounts };
  }

  /**
   * Save the accounts that have been updated
   *
   * @param {Array} accounts
   */
  async saveUpdatedAccounts(accounts) {
    const promises = [];

    // Save accounts and calculate hash so we can later check if the account has changed
    if (accounts && accounts.length !== 0) {
      accounts.forEach((account) => {
        const accountDBItem = {
          UUID: account['Global ID 03'],
          PSHash: hash(account), // Calculate hash
          UpdatedAt: moment().tz('Australia/Sydney').format('YYYY-MM-DD HH:mm:ss'), // Updated in WorldManager
        };

        promises.push(this.precedaAccountsStorage.saveAccount(accountDBItem));
      });
    }

    // Wait for all promises to finish
    await Promise.all(promises);

    return true;
  }
}

module.exports = PrecedaToWorldManagerExport;
