const DynamoDB = require('./DynamoDB');

/**
 * Class to count accounts processed in Preceda to WM integration
 *
 */
class PrecedaToWorldManagerCounter {
  constructor() {
    this.dynamoDB = new DynamoDB();
    // Default table name for testing
    this.tableName = process.env.PRECEDA_ACCOUNTS_EXPORT_COUNT_TABLE_NAME || 'dev-PSExportCount';
  }

  /**
   * Save count
   *
   * @param {Object} count
   */
  async saveCount(count) {
    if (!count) {
      return null;
    }

    const item = {
      Timestamp: count.timestamp,
      InFile: count.allAccounts,
      ToUpdate: count.toUpdate,
      Failed: 0,
      Updated: 0,
    };

    return this.dynamoDB.putItem(this.tableName, item);
  }

  /**
   * Increment Failed counter
   *
   * @param {string} timestamp
   */
  async incrementFailedCounter(timestamp) {
    const key = {
      Timestamp: timestamp,
    };

    return this.dynamoDB.incrementAtomicCounter(this.tableName, key, 'Failed');
  }

  /**
   * Increment Updated counter
   *
   * @param {string} timestamp
   */
  async incrementUpdatedCounter(timestamp) {
    const key = {
      Timestamp: timestamp,
    };

    return this.dynamoDB.incrementAtomicCounter(this.tableName, key, 'Updated');
  }

  /**
   * Set email sent so we don't send it multiple times
   *
   * @param {string} timestamp
   */
  async setEmailSent(timestamp) {
    // Update item
    const params = {
      TableName: this.tableName,
      Key: { Timestamp: timestamp },
      UpdateExpression: 'SET EmailSent = :emailSent',
      ExpressionAttributeValues: { ':emailSent': true },
    };

    return this.dynamoDB.updateItem(params);
  }

  /**
   * Check if email for some timestamp was sent
   *
   * @param {string} timestamp
   */
  async emailWasSent(timestamp) {
    const item = await this.dynamoDB.getFromTable(this.tableName, { Timestamp: timestamp });

    if (item.body) {
      return item.body.EmailSent && item.body.EmailSent === true;
    }

    // Not found
    return false;
  }
}

module.exports = PrecedaToWorldManagerCounter;
