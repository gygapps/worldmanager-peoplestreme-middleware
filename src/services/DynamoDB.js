const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

/**
 * This class is a wrapper for AWS DynamoDB SDK
 *
 */
class DynamoDB {
  constructor() {
    AWS.config.update({
      region: 'ap-southeast-2',
    });

    this.dynamodb = new AWS.DynamoDB.DocumentClient({});
  }

  /**
   * Scan DynamoDB table
   *
   * @param {*} tableName
   */
  async scanTable(tableName) {
    const params = {
      TableName: tableName,
    };

    const scanResults = [];
    let items;
    do {
      // eslint-disable-next-line no-await-in-loop
      items = await this.dynamodb.scan(params).promise();
      items.Items.forEach(item => scanResults.push(item));
      params.ExclusiveStartKey = items.LastEvaluatedKey;
    } while (typeof items.LastEvaluatedKey !== 'undefined');

    return scanResults;
  }

  /**
   * Get item from DynamoDB table by key
   *
   * @param {*} tableName
   * @param {*} key
   */
  getFromTable(tableName, key) {
    const params = {
      TableName: tableName,
      Key: key,
    };

    // Fetch from database
    return new Promise((resolve, reject) => this.dynamodb.get(params, (error, result) => {
      // handle potential errors
      if (error) {
        return reject(error);
      }

      // create a response
      const response = {
        statusCode: 200,
        body: result.Item,
      };

      return resolve(response);
    }));
  }

  /**
   * Query DynamoDB table by key and value
   *
   * @param {*} tableName
   * @param {*} key
   */
  queryTable(tableName, key, value) {
    const params = {
      TableName: tableName,
      KeyConditionExpression: '#key = :value',
      ExpressionAttributeValues: {
        ':value': value,
      },
      ExpressionAttributeNames: {
        '#key': key,
      },
    };

    // Fetch from database
    return new Promise((resolve, reject) => this.dynamodb.query(params, (error, result) => {
      // handle potential errors
      if (error) {
        console.error(error);
        return reject(error);
      }

      // Create a response
      const response = {
        statusCode: 200,
        body: result.Items,
      };

      return resolve(response);
    }));
  }

  /**
   * Put item in DynamoDB table
   *
   * @param {*} tableName
   * @param {*} item
   */
  putItem(tableName, item) {
    const params = {
      TableName: tableName,
      Item: item,
    };

    // Write item to database
    return new Promise((resolve, reject) => {
      this.dynamodb.put(params, (error) => {
        // handle potential errors
        if (error) {
          reject(error);
        }

        // create a response
        const response = {
          statusCode: 200,
          body: JSON.stringify(params.Item),
        };

        return resolve(response);
      });
    });
  }

  /**
   * Update item in DynamoDB table
   *
   * @param {*} options
   */
  updateItem(options) {
    const params = {
      TableName: options.TableName,
      Key: options.Key,
      UpdateExpression: options.UpdateExpression,
      ExpressionAttributeValues: options.ExpressionAttributeValues,
      ReturnValues: 'UPDATED_NEW',
    };

    // Update item
    return new Promise((resolve, reject) => {
      this.dynamodb.update(params, (error, data) => {
        // handle potential errors
        if (error) {
          reject(error);
        }

        // create a response
        const response = {
          statusCode: 200,
          body: JSON.stringify(data),
        };

        return resolve(response);
      });
    });
  }

  /**
   * Get multiple items
   *
   * @param {string} tableName
   * @param {Array} keys
   */
  batchGet(tableName, keys) {
    const params = {
      RequestItems: {
        [tableName]: {
          Keys: keys,
          // ProjectionExpression: 'KEY_NAME, ATTRIBUTE'
        },
      },
    };

    return new Promise((resolve, reject) => {
      this.dynamodb.batchGet(params, (err, data) => {
        if (err) {
          console.log('Error', err);
          return reject(err);
        }

        if (data.Responses && data.Responses[tableName]) {
          return resolve(data.Responses[tableName]);
        }

        return resolve(data);
      });
    });
  }

  /**
   * Increment an atomic counter
   *
   * @param {string} tableName
   * @param {Object} key
   * @param {string} fieldToIncrement
   */
  incrementAtomicCounter(tableName, key, fieldToIncrement) {
    const params = {
      TableName: tableName,
      Key: key,
      UpdateExpression: `set ${fieldToIncrement} = ${fieldToIncrement} + :val`,
      ExpressionAttributeValues: {
        ':val': 1,
      },
      ReturnValues: 'UPDATED_NEW',
    };

    return new Promise((resolve, reject) => {
      this.dynamodb.update(params, (err, data) => {
        if (err) {
          console.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
          return reject(err);
        }

        return resolve(data);
      });
    });
  }
}

module.exports = DynamoDB;
