const DynamoDB = require('./DynamoDB');

/**
 * Class to save and read Preceda accounts to and from a DynamoDB table
 *
 */
class PrecedaAccountsStorage {
  constructor() {
    this.dynamoDB = new DynamoDB();
    // Default table name for testing
    this.tableName = process.env.PRECEDA_ACCOUNTS_TABLE_NAME || 'dev-PSAccount';
  }

  /**
   * Check if account exists
   *
   * @param {string} uuid
   */
  async accountExistsInTable(uuid) {
    const account = await this.getAccountByUUID(uuid);
    return account.body !== undefined;
  }

  /**
   * Get an account by UUID
   *
   * @param {string} uuid
   */
  async getAccountByUUID(uuid) {
    const key = {
      UUID: uuid,
    };

    return this.dynamoDB.getFromTable(this.tableName, key);
  }

  /**
   * Get accounts by UUIDs
   *
   * @param {Array} uuids
   */
  async getAccountsByUUIDs(uuids) {
    if (!uuids || uuids.length === 0) {
      return null;
    }

    // Remove duplicates
    const uuidNoDuplicates = [...new Set(uuids)];

    const keys = [];

    // Create array of keys for DynamoDB
    uuidNoDuplicates.forEach((uuid) => {
      // Remove if empty UUID
      if (uuid && uuid !== '') {
        keys.push({ UUID: uuid });
      }
    });

    return this.dynamoDB.batchGet(this.tableName, keys);
  }

  /**
   * Save account
   *
   * @param {Object} account
   */
  async saveAccount(account) {
    return this.dynamoDB.putItem(this.tableName, account);
  }
}

module.exports = PrecedaAccountsStorage;
