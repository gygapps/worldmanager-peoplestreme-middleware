const Client = require('ssh2-sftp-client');
const fs = require('fs');

const LOCAL_TMP_FILEPATH = '/tmp/test.txt';
const BACKUP_DRIRECTORY= 'backup';

class SFTPService {
  constructor(sftpConfig) {
    this.sftpConfig = sftpConfig;
    this.filePath = '/';
    this.backupDirectory = BACKUP_DRIRECTORY;
  }

  /**
   * Download file from SFTP server
   *
   * @param {*} remoteFilePath
   * @param {*} localFilePath
   */
  async downloadFile(remoteFilePath, localFilePath) {
    const sftp = new Client();
    await sftp.connect(this.sftpConfig);

    try {
      await sftp.fastGet(remoteFilePath, localFilePath);
      return true;
    } catch (error) {
      return null;
    } finally {
      await sftp.end();
    }
  }

  /**
   * Get file from SFTP server as string
   *
   * @param {*} remoteFilePath
   */
  async getFileAsString(remoteFilePath) {
    const sftp = new Client();
    await sftp.connect(this.sftpConfig);

    let fileContent = null;

    try {
      await sftp.fastGet(remoteFilePath, LOCAL_TMP_FILEPATH);
      fileContent = fs.readFileSync(LOCAL_TMP_FILEPATH, { encoding: 'utf8' });
    } catch (error) {
      return null;
    } finally {
      await sftp.end();
    }

    return fileContent;
  }

  /**
   * Upload local file to SFTP server
   *
   * @param {*} localFilepath
   * @param {*} remoteFilePath
   */
  async uploadLocalFile(localFilepath, remoteFilePath) {
    const sftp = new Client();
    await sftp.connect(this.sftpConfig);

    const response = await sftp.fastPut(localFilepath, remoteFilePath);

    await sftp.end();

    return response;
  }

  /**
   * Check if file exists in SFTP server
   *
   * @param {*} filename
   */
  async fileExists(filename) {
    const sftp = new Client();
    await sftp.connect(this.sftpConfig);

    const response = await sftp.list(this.filePath);

    await sftp.end();

    if (!response) {
      return false;
    }

    const filteredArray = response.filter(obj => obj.name === filename);

    return filteredArray && filteredArray.length > 0;
  }

  /**
   * List all files in some path
   *
   * @param {*} remoteDirectory
   */
  async getAllFiles(remoteDirectory) {
    const sftp = new Client();
    await sftp.connect(this.sftpConfig);

    const response = await sftp.list(remoteDirectory);

    await sftp.end();

    if (!response) {
      return false;
    }
    return response;
  }
}

module.exports = SFTPService;
