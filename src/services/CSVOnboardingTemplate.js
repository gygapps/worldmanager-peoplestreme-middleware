const CSVService = require('./CSVService');
const FileService = require('./FileService');

class CSVOnboardingTemplate {

  /**
   * Export to a CSV file
   *
   * @param {*} data
   * @param {*} filepath
   */
  static exportCSVToFile(data, filepath) {
    return FileService.exportDataToFile(CSVOnboardingTemplate.exportCSV(data), filepath);
  }

  /**
   * Export to CSV
   *
   * @param {*} data
   */
  static exportCSV(data) {
    const fields = [
      {
        label: 'Email',
        value: 'email',
      },
      {
        label: 'Position_ID',
        value: 'positionId',
      },
      {
        label: 'Role_Title',
        value: 'roleTitle',
      },
      {
        label: 'Trainee',
        value: 'trainee',
      },
      {
        label: 'Company_Name',
        value: 'companyName',
      },
      {
        label: 'Award/EA',
        value: 'award',
      },
      {
        label: 'Level',
        value: 'level',
      },
      {
        label: 'Position_Type',
        value: 'positionType',
      },
      {
        label: 'Monday_Shift_Time',
        value: 'mondayShiftTime',
      },
      {
        label: 'Monday_Break_Duration',
        value: 'mondayBreakDuration',
      },
      {
        label: 'Monday_Break_Time',
        value: 'mondayBreakTime',
      },
      {
        label: 'Monday_Total_Hours',
        value: 'mondayTotalHours',
      },
      {
        label: 'Tuesday_Shift_Time',
        value: 'tuesdayShiftTime',
      },
      {
        label: 'Tuesday_Break_Duration',
        value: 'tuesdayBreakDuration',
      },
      {
        label: 'Tuesday_Break_Time',
        value: 'tuesdayBreakTime',
      },
      {
        label: 'Tuesday_Total_Hours',
        value: 'tuesdayTotalHours',
      },
      {
        label: 'Wednesday_Shift_Time',
        value: 'wednesdayShiftTime',
      },
      {
        label: 'Wednesday_Break_Duration',
        value: 'wednesdayBreakDuration',
      },
      {
        label: 'Wednesday_Break_Time',
        value: 'wednesdayBreakTime',
      },
      {
        label: 'Wednesday_Total_Hours',
        value: 'wednesdayTotalHours',
      },
      {
        label: 'Thursday_Shift_Time',
        value: 'thursdayShiftTime',
      },
      {
        label: 'Thursday_Break_Duration',
        value: 'thursdayBreakDuration',
      },
      {
        label: 'Thursday_Break_Time',
        value: 'thursdayBreakTime',
      },
      {
        label: 'Thursday_Total_Hours',
        value: 'thursdayTotalHours',
      },
      {
        label: 'Friday_Shift_Time',
        value: 'fridayShiftTime',
      },
      {
        label: 'Friday_Break_Duration',
        value: 'fridayBreakDuration',
      },
      {
        label: 'Friday_Break_Time',
        value: 'fridayBreakTime',
      },
      {
        label: 'Friday_Total_Hours',
        value: 'fridayTotalHours',
      },
      {
        label: 'Saturday_Shift_Time',
        value: 'saturdayShiftTime',
      },
      {
        label: 'Saturday_Break_Duration',
        value: 'saturdayBreakDuration',
      },
      {
        label: 'Saturday_Break_Time',
        value: 'saturdayBreakTime',
      },
      {
        label: 'Saturday_Total_Hours',
        value: 'saturdayTotalHours',
      },
      {
        label: 'Sunday_Shift_Time',
        value: 'sundayShiftTime',
      },
      {
        label: 'Sunday_Break_Duration',
        value: 'sundayBreakDuration',
      },
      {
        label: 'Sunday_Break_Time',
        value: 'sundayBreakTime',
      },
      {
        label: 'Sunday_Total_Hours',
        value: 'sundayTotalHours',
      },
      {
        label: 'Base_Hours',
        value: 'baseHours',
      },
      {
        label: 'Payrun_Group',
        value: 'payrunGroup',
      },
      {
        label: 'Pay_Frequency',
        value: 'payFrequency',
      },
      {
        label: 'Pay_Method',
        value: 'payMethod',
      },
      {
        label: 'Auto_Pay',
        value: 'autoPay',
      },
      {
        label: 'Auto_Pay_Hours',
        value: 'autoPayHours',
      },
      {
        label: 'Payslip_Type',
        value: 'payslipType',
      },
      {
        label: 'Delivery_Method',
        value: 'deliveryMethod',
      },
      {
        label: 'Email_To',
        value: 'emailTo',
      },
      {
        label: 'Salary_Code',
        value: 'salaryCode',
      },
      {
        label: 'Rate_Frequency',
        value: 'rateFrequency',
      },
      {
        label: 'Base_Pay_Rate',
        value: 'basePayRate',
      },
      {
        label: 'Cost_Level_5',
        value: 'costLevel5',
      },
      {
        label: 'Employment_Type',
        value: 'employmentType',
      },
      {
        label: 'Personnel_Type',
        value: 'personnelType',
      },
      {
        label: 'Location',
        value: 'location',
      },
      {
        label: 'Additional_Form_Recipients',
        value: 'additionalFormRecipient',
      },
      {
        label: 'Cost_Level_1',
        value: 'costLevel1',
      },
      {
        label: 'Cost_Level_2',
        value: 'costLevel2',
      },
      {
        label: 'Cost_Level_3',
        value: 'costLevel3',
      },
      {
        label: 'Cost_Level_4',
        value: 'costLevel4',
      },
      {
        label: 'Department',
        value: 'department',
      },
      {
        label: 'Account',
        value: 'account',
      },
    ];

    return CSVService.exportToCSVWithoutQuotes(fields, data);
  }
}

module.exports = CSVOnboardingTemplate;
