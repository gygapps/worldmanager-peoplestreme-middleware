const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

/**
 * This class is a wrapper for AWS SSM SDK
 *
 */
class AWSSSMService {
  constructor() {
    AWS.config.update({ region: 'ap-southeast-2' });
    this.ssm = new AWS.SSM();
  }

  /**
   * Get parameter
   *
   * @param {*} name
   */
  async getParameter(name) {
    const params = {
      Name: name,
      WithDecryption: true,
    };

    return new Promise((resolve, reject) => {
      this.ssm.getParameter(params, (err, data) => {
        if (err) {
          return reject(err);
        }

        return resolve(data);
      });
    });
  }
}

module.exports = AWSSSMService;
