const CSVService = require('./CSVService');
const FileService = require('./FileService');

class CSVStandard {
  /**
   * Export to a CSV file
   *
   * @param {*} data
   * @param {*} filepath
   */
  static exportCSVToFile(data, filepath) {
    return FileService.exportDataToFile(CSVStandard.exportCSV(data), filepath);
  }

  /**
   * Export to CSV
   *
   * @param {*} data
   */
  static exportCSV(data) {
    const fields = [
      {
        value: 'payrollId',
        label: 'Payroll Id',
      },
      {
        value: 'managerEmail',
        label: 'Manager\'s Email',
      },
      {
        value: 'username',
        label: 'Username',
      },
      {
        value: 'salutation',
        label: 'Salutation',
      },
      {
        value: 'firstName',
        label: 'First Name',
      },
      {
        value: 'lastName',
        label: 'Last Name',
      },
      {
        value: 'gender',
        label: 'Gender',
      },
      {
        value: 'positionTitle',
        label: 'Position Title',
      },
      {
        value: 'email',
        label: 'Email',
      },
      {
        value: 'privateEmail',
        label: 'Private Email',
      },
      {
        value: 'mobile',
        label: 'Mobile Phone Number',
      },
      {
        value: 'jobCode',
        label: 'Job Code',
      },
      {
        value: 'startDate',
        label: 'Start Date (yyyy-MM-dd)',
      },
      {
        value: 'terminationDate',
        label: 'Termination Date (yyyy-MM-dd)',
      },
      {
        value: 'dateOfBirth',
        label: 'Date Of Birth (yyyy-MM-dd)',
      },
      {
        value: 'highFrequencyReview',
        label: 'High Frequency Review',
      },
      {
        value: 'ceo',
        label: 'CEO',
      },
      {
        value: 'language',
        label: 'Language',
      },
      {
        value: 'eRecruitmentAccess',
        label: 'eRecruitment Access',
      },
      {
        value: 'streetAddress',
        label: 'Street Address',
      },
      {
        value: 'suburb',
        label: 'Suburb',
      },
      {
        value: 'postCode',
        label: 'Postcode',
      },
      {
        value: 'state',
        label: 'State',
      },
      {
        value: 'country',
        label: 'Country',
      },
      {
        value: 'homePhone',
        label: 'Home Phone',
      },
      {
        value: 'emergencyContactName',
        label: 'Emergency Contact Name',
      },
      {
        value: 'emergencyContactMobile',
        label: 'Emergency Contact Mobile',
      },
      {
        value: 'emergencyContactPhone',
        label: 'Emergency Contact Phone',
      },
      {
        value: 'emergencyInstructions',
        label: 'Emergency Instructions',
      },
      {
        value: 'organisationLevel1',
        label: 'Organisation Level 1',
      },
      {
        value: 'organisationLevel2',
        label: 'Organisation Level 2',
      },
      {
        value: 'organisationLevel3',
        label: 'Organisation Level 3',
      },
      {
        value: 'organisationLevel4',
        label: 'Organisation Level 4',
      },
      {
        value: 'organisationLevel5',
        label: 'Organisation Level 5',
      },
      {
        value: 'organisationLevel6',
        label: 'Organisation Level 6',
      },
      {
        value: 'organisationLevel7',
        label: 'Organisation Level 7',
      },
      {
        value: 'organisationLevel8',
        label: 'Organisation Level 8',
      },
      {
        value: 'organisationLevel9',
        label: 'Organisation Level 9',
      },
      {
        value: 'organisationLevel10',
        label: 'Organisation Level 10',
      },
      {
        value: 'moduleAccessPerformance',
        label: 'Module Access - Performance',
      },
      {
        value: 'moduleAccessLearning',
        label: 'Module Access - Learning',
      },
      {
        value: 'moduleAccessERecruitment',
        label: 'Module Access - eRecruitment',
      },
      {
        value: 'moduleAccessWorkforcePlanning',
        label: 'Module Access - Workforce Planning',
      },
      {
        value: 'moduleAccessOnboarding',
        label: 'Module Access - Onboarding',
      },
      {
        value: 'moduleAccessTalentManagement',
        label: 'Module Access - Talent Management',
      },
      {
        value: 'moduleAccessRemuneration',
        label: 'Module Access - Remuneration',
      },
      {
        value: 'moduleAccessSocialStreme',
        label: 'Module Access - SocialStreme',
      },
      {
        value: 'moduleAccessSocialRecognition',
        label: 'Module Access - Social Recognition',
      },
      {
        value: 'moduleAccessEForms',
        label: 'Module Access - eForms',
      },
      {
        value: 'moduleAccessUndefined',
        label: 'Module Access - undefined',
      },
      {
        value: 'moduleAccessEmployeeRecognition',
        label: 'Module Access - Employee Recognition',
      },
      {
        value: 'moduleAccessIncidentManagement',
        label: 'Module Access - Incident Management',
      },
      {
        value: 'moduleAccessCentralVue',
        label: 'Module Access - CentralVue',
      },
      {
        value: 'moduleAccessDocuments',
        label: 'Module Access - Documents',
      },
      {
        value: 'moduleAccessEmployeeLifecycle',
        label: 'Module Access - Employee Lifecycle',
      },
      {
        value: 'moduleAccessExtendedCoreHR',
        label: 'Module Access - Extended Core HR',
      },
      {
        value: 'moduleAccessOrgChart',
        label: 'Module Access - Org Chart',
      },
      {
        value: 'moduleAccessJobLevel',
        label: 'Job Level',
      },
      {
        value: 'moduleAccessFTE',
        label: 'FTE',
      },
    ];

    return CSVService.exportToCSV(fields, data);
  }
}

module.exports = CSVStandard;
