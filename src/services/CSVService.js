const { Parser } = require('json2csv');
const CSVToJson = require('csvtojson');

class CSVService {
  /**
   * Export data to CSV
   *
   * @param {*} fields (headers)
   * @param {*} data
   */
  static exportToCSV(fields, data) {
    const json2csvParser = new Parser({ fields });
    const csv = json2csvParser.parse(data);

    return csv;
  }

  /**
   * Export data to CSV without double quotes
   *
   * @param {*} fields (headers)
   * @param {*} data
   */
  static exportToCSVWithoutQuotes(fields, data) {
    const json2csvParser = new Parser({
      fields,
      quote: '',
    });
    const csv = json2csvParser.parse(data);

    return csv;
  }

  /**
   * Read and parse a CSV file
   *
   * @param {*} filePath
   */
  static async readFromCSVFile(filePath) {
    return CSVToJson().fromFile(filePath);
  }
}

module.exports = CSVService;
