const _ = require('lodash');
const moment = require('moment');
const IntegrationTables = require('../../src/services/IntegrationTables');

const Boolean = {
  True: 'TRUE',
  False: 'FALSE',
};

const Gender = {
  Default: 1,
  Male: 2,
  Female: 3,
};

const Language = {
  English: 1,
};

const PositionType = {
  FullTime: 'Fulltime',
  PartTime: 'Part Time',
  Casual: 'Casual',
};

const positionNames = {
  RestaurantManager: 'Restaurant Manager',
  AssistantRestaurantManager: 'Assistant Restaurant Manager',
  HeadCook: 'Head Cook (Salaried)',
  ShiftLeader: 'Shift Leader',
  SeniorCook: 'Senior Cook (Hrly Paid)',
  Crew: 'Crew',
  Franchisee: 'Franchisee',
  HeadOffice: 'Head Office',
  Trainee: 'Trainee',
};

const DateFormats = {
  Input: 'DD-MM-YYYY',
  Output: 'YYYY-MM-DD',
  DateLeftSchoolInput: 'DD.MM.YY',
};

const Age = {
  Under16: 'Under 16',
  Over20: '21+',
};

const YearLeftSchool = {
  StillAtSchool: 'Still at School',
  Year10: 'Year 10',
  Year11: 'Year 11',
  Year12: 'Year 12',
};

const traineeSalaryCodes = {
  Year10: 'R',
  Year11: 'S',
  Year12: 'T',
};

const NA = 'N/A';

/* eslint-disable class-methods-use-this */
class WorldManagerToPeopleStremeMapping {
  constructor() {
    this.integrationTables = new IntegrationTables();
    // Restaurants cache
    this.restaurants = {};
  }

  async peopleStremeStandardFileMapping(data) {
    // Get restaurant data
    const restaurant = await this.getRestaurant(data.storeUuids);

    if (!restaurant || !restaurant.Active) {
      // No restaurant or restaurant not active so we shouldn't export
      if (!restaurant) {
        console.error(`No restaurant found for UUID ${data.storeUuids}`);
      } else if (!restaurant.Active) {
        console.log(`Restaurant ${restaurant.Name} is not active so not exporting.`);
      }

      return null;
    }

    this.customDetails = data.customDetails;

    // Get position title
    let positionTitle = await this.getCustomFieldValue('03. Position Title');
    // Get Preceda Id
    // const newPrecedaId = await this.getCustomFieldValue('38. Preceda I.D');
    // Trainee
    const trainee = this.getTraineeQuestion();

    if (this.restaurantHasTraineeSalaryCodes(restaurant)) {
      // If trainee and Crew, position should be Trainee - Crew
      if (trainee === 'Y' && positionTitle === positionNames.Crew) {
        positionTitle = `${positionNames.Trainee} - ${positionTitle}`;
      }
    }

    const response = {};

    // Payroll ID
    response.payrollId = data.uuid;

    // Manager's email
    // Recognise in integration from a background table based on restaurant location.
    // Will be RM emails for corporate and Franchisee or RM email for
    // franchisee (never restaurant email)
    response.managerEmail = await this.getManagerEmail(data.storeUuids, positionTitle);

    // Username
    response.username = data.email;

    // Salutation
    response.salutation = data.person.title;

    // First name
    response.firstName = data.person.firstName.trim();

    // Last name
    response.lastName = data.person.lastName.trim();

    // Gender
    // Need to turn Male into 2 and Female into 3
    if (data.customDetails && data.customDetails[1]) {
      response.gender = this.mapGender(data.customDetails[1].value);
    } else {
      response.gender = 1;
    }

    // Position title
    // Add together Location and Position Title with a - in between
    // e.g. 'Australia Square - Shift Leader'.
    if (this.restaurants[data.storeUuids]) {
      // If their position title is Franchisee or HO remove them from the integration.
      if ([positionNames.Franchisee, positionNames.HeadOffice].includes(positionTitle)) {
        response.positionTitle = null;
      } else {
        response.positionTitle = `${this.restaurants[data.storeUuids].Name} - ${positionTitle}`;
      }
    } else {
      response.positionTitle = '';
    }

    // Email
    response.email = data.email;

    // Private email
    response.privateEmail = data.email;

    // Mobile phone number
    response.mobile = data.mobile;

    // Job code
    response.jobCode = null;

    // Start date
    // Change date from DD-MM-YYYY to YYYY-MM-DD
    response.startDate = moment(data.workStartDate, DateFormats.Input).format(DateFormats.Output);

    // Termination date
    response.terminationDate = null;

    // Date of birth
    // Change date from DD-MM-YYYY to YYYY-MM-DD
    const dateOfBirth = this.getCustomFieldValue("01. Crew Member's Date Of Birth (RM must complete when creating account)");
    if (dateOfBirth) {
      response.dateOfBirth = moment(dateOfBirth, DateFormats.Input).format(DateFormats.Output);
    } else {
      response.dateOfBirth = '';
    }

    // High Frequency Review
    // Need to populate all with FALSE
    response.highFrequencyReview = Boolean.False;

    // CEO
    // Need to populate all with FALSE
    response.ceo = Boolean.False;

    // Language
    response.language = Language.English;

    // eRecruitment Access
    response.eRecruitmentAccess = null;

    // Street Address
    response.streetAddress = null;

    // Suburb
    response.suburb = null;

    // Postcode
    response.postCode = null;

    // State
    response.state = null;

    // Country
    response.country = null;

    // Home Phone
    response.homePhone = null;

    // Emergency Contact Name
    response.emergencyContactName = this.getCustomFieldValue("36. Emergency Contact: Person's Name");

    // Emergency Contact Mobile
    response.emergencyContactMobile = this.getCustomFieldValue("37. Emergency Contact: Person's phone number");

    // Emergency Contact Phone
    response.emergencyContactPhone = null;

    // Emergency Instructions
    response.emergencyInstructions = null;

    // Organisation Level 1
    response.organisationLevel1 = null;

    // Organisation Level 2
    response.organisationLevel2 = null;

    // Organisation Level 3
    response.organisationLevel3 = null;

    // Organisation Level 4
    response.organisationLevel4 = null;

    // Organisation Level 5
    response.organisationLevel5 = null;

    // Organisation Level 6
    response.organisationLevel6 = null;

    // Organisation Level 7
    response.organisationLevel7 = null;

    // Organisation Level 8
    response.organisationLevel8 = null;

    // Organisation Level 9
    response.organisationLevel9 = null;

    // Organisation Level 10
    response.organisationLevel10 = null;

    // Module Access - Performance
    response.moduleAccessPerformance = Boolean.False;

    // Module Access - Learning
    response.moduleAccessLearning = Boolean.False;

    // Module Access - eRecruitment
    response.moduleAccessERecruitment = Boolean.False;

    // Module Access - Workforce Planning
    response.moduleAccessWorkforcePlanning = Boolean.False;

    // Module Access - Onboarding
    // Need to Populate all with TRUE
    response.moduleAccessOnboarding = Boolean.True;

    // Module Access - Talent Management
    response.moduleAccessTalentManagement = Boolean.False;

    // Module Access - Remuneration
    response.moduleAccessRemuneration = Boolean.False;

    // Module Access - SocialStreme
    response.moduleAccessSocialStreme = Boolean.False;

    // Module Access - Social Recognition
    response.moduleAccessSocialRecognition = Boolean.False;

    // Module Access - eForms
    response.moduleAccessEForms = Boolean.False;

    // Module Access - undefined
    response.moduleAccessUndefined = Boolean.False;

    // Module Access - Employee Recognition
    response.moduleAccessEmployeeRecognition = Boolean.False;

    // Module Access - Incident Management
    response.moduleAccessIncidentManagement = Boolean.False;

    // Module Access - CentralVue
    response.moduleAccessCentralVue = Boolean.False;

    // Module Access - Documents
    response.moduleAccessDocuments = Boolean.False;

    // Module Access - Employee Lifecycle
    response.moduleAccessEmployeeLifecycle = Boolean.False;

    // Module Access - Extended Core HR
    response.moduleAccessExtendedCoreHR = Boolean.False;

    // Module Access - Org Chart
    response.moduleAccessOrgChart = Boolean.False;

    // Job Level
    response.moduleAccessJobLevel = null;

    // FTE - Do we need this?
    response.moduleAccessFTE = null;

    // Checking first time addition of new customm field
    //    response.newPrecedaId = this.getCustomFieldValue('38. Preceda I.D');

    return response;
  }

  async peopleStremeCustomFileMapping(data) {
    // Get restaurant data
    const restaurant = await this.getRestaurant(data.storeUuids);

    if (!restaurant || !restaurant.Active) {
      // No restaurant or restaurant not active so we shouldn't export
      if (!restaurant) {
        console.error(`No restaurant found for UUID ${data.storeUuids}`);
      } else if (!restaurant.Active) {
        console.log(`Restaurant ${restaurant.Name} is not active so not exporting.`);
      }

      return null;
    }

    this.customDetails = data.customDetails;
    // const newPrecedaId = await this.getCustomFieldValue('38. Preceda I.D');

    const response = {};

    // Primary Email Address
    response.email = data.email;

    // Position ID
    // Get position title from WM
    // Get location from WM
    // Get the position id from static db table TBD or spreadsheet
    // attached tab "Position ID Table (Std)"
    let positionTitle = await this.getCustomFieldValue('03. Position Title');

    // Trainee
    response.trainee = this.getTraineeQuestion();

    if (this.restaurantHasTraineeSalaryCodes(restaurant)) {
      // If trainee and Crew, position should be Trainee - Crew
      if (response.trainee === 'Y' && positionTitle === positionNames.Crew) {
        positionTitle = `${positionNames.Trainee} - ${positionTitle}`;
      }
    }

    response.positionId = await this.getPositionId(data.storeUuids, positionTitle);

    // Role Title
    // If they are listed as Franchisee or HO remove them from the integration
    if ([positionNames.Franchisee, positionNames.HeadOffice].includes(positionTitle)) {
      response.roleTitle = null;
    } else {
      response.roleTitle = positionTitle;
    }

    // Company Name
    // Recognises Company Name by Location from WM
    response.companyName = await this.getCompanyName(data.storeUuids);

    // Award / EA
    // Recognises Award by Location from WM
    response.award = await this.getAward(data.storeUuids);

    // Level
    const wmLevel = this.getCustomFieldValue('04. Employment Level');
    response.level = await this.getLevel(wmLevel);

    // Position Type
    // This is the name of Employment Type (Position Type), if the field in WM for Salaried says 'Y'
    // the Full-time or Part-time needs updating to say Full-time Salaried or Part-time Salaried.
    const salariedEmployee = this.getCustomFieldValue('05. Salaried Employee');

    if (salariedEmployee) {
      response.positionType = await this.mapPositionType(data.positionType, salariedEmployee);
    } else {
      response.positionType = '';
    }

    // Monday Shift Time
    response.mondayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('08. Monday Shift Time')));

    // Monday Break Duration
    response.mondayBreakDuration = this.NAToBlank(this.getCustomFieldValue('09. Monday Break Duration'));

    // Monday Break Time
    response.mondayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('10. Monday Break Time')));

    // Monday Total Hours
    response.mondayTotalHours = this.NAToBlank(this.getCustomFieldValue('11. Monday Total Hours'));

    // Tuesday Shift Time
    response.tuesdayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('12. Tuesday Shift Time')));

    // Tuesday Break Duration
    response.tuesdayBreakDuration = this.NAToBlank(this.getCustomFieldValue('13. Tuesday Break Duration'));

    // Tuesday Break Time
    response.tuesdayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('14. Tuesday Break Time')));

    // Tuesday Total Hours
    response.tuesdayTotalHours = this.NAToBlank(this.getCustomFieldValue('15. Tuesday Total Hours'));

    // Wednesday Shift Time
    response.wednesdayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('16. Wednesday Shift Time')));

    // Wednesday Break Duration
    response.wednesdayBreakDuration = this.NAToBlank(this.getCustomFieldValue('17. Wednesday Break Duration'));

    // Wednesday Break Time
    response.wednesdayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('18. Wednesday Break Time')));

    // Wednesday Total Hours
    response.wednesdayTotalHours = this.NAToBlank(this.getCustomFieldValue('19. Wednesday Total Hours'));

    // Thursday Shift Time
    response.thursdayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('20. Thursday Shift Time')));

    // Thursday Break Duration
    response.thursdayBreakDuration = this.NAToBlank(this.getCustomFieldValue('21. Thursday Break Duration'));

    // Thursday Break Time
    response.thursdayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('22. Thursday Break Time')));

    // Thursday Total Hours
    response.thursdayTotalHours = this.NAToBlank(this.getCustomFieldValue('23. Thursday Total Hours'));

    // Friday Shift Time
    response.fridayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('24. Friday Shift Time')));

    // Friday Break Duration
    response.fridayBreakDuration = this.NAToBlank(this.getCustomFieldValue('25. Friday Break Duration'));

    // Friday Break Time
    response.fridayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('26. Friday Break Time')));

    // Friday Total Hours
    response.fridayTotalHours = this.NAToBlank(this.getCustomFieldValue('27. Friday Total Hours'));

    // Saturday Shift Time
    response.saturdayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('28. Saturday Shift Time')));

    // Saturday Break Duration
    response.saturdayBreakDuration = this.NAToBlank(this.getCustomFieldValue('29. Saturday Break Duration'));

    // Saturday Break Time
    response.saturdayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('30. Saturday Break Time')));

    // Saturday Total Hours
    response.saturdayTotalHours = this.NAToBlank(this.getCustomFieldValue('31. Saturday Total Hours'));

    // Sunday Shift Time
    response.sundayShiftTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('32. Sunday Shift Time')));

    // Sunday Break Duration
    response.sundayBreakDuration = this.NAToBlank(this.getCustomFieldValue('33. Sunday Break Duration'));

    // Sunday Break Time
    response.sundayBreakTime = this.replaceWithColon(this.NAToBlank(this.getCustomFieldValue('34. Sunday Break Time')));

    // Sunday Total Hours
    response.sundayTotalHours = this.NAToBlank(this.getCustomFieldValue('35. Sunday Total Hours'));

    // Base Hours
    // If Position type is FT and Salaried says 'Y' = 38,
    // If position Type is PT and Salaried says 'Y' = populate with
    // Total Mon-Sun hrs fields added together e.g.20,
    // If FT and Salaried says 'N' = F,
    // If PT and Salaried says 'N' = P
    // If Casual = C
    response.baseHours = this.getBaseHours(data.positionType, salariedEmployee);

    if (response.baseHours === 'addHours') {
      const hours = {
        mondayTotalHours: response.mondayTotalHours,
        tuesdayTotalHours: response.tuesdayTotalHours,
        wednesdayTotalHours: response.wednesdayTotalHours,
        thursdayTotalHours: response.thursdayTotalHours,
        fridayTotalHours: response.fridayTotalHours,
        saturdayTotalHours: response.saturdayTotalHours,
        sundayTotalHours: response.sundayTotalHours,
      };
      response.baseHours = this.addTotalHours(hours);
    }

    // Payrun Group
    // Populate all with 'HIRE'
    response.payrunGroup = 'HIRE';

    // Pay Frequency
    // Populate all with 'F'
    response.payFrequency = 'F';

    // Pay Method
    // Populate with 'B'
    response.payMethod = 'B';

    // Auto Pay
    // Populate with 'Y' if have 'Y' in WM Salaried Employee field, otherwise populate with 'N'
    response.autoPay = this.getAutoPay(salariedEmployee);

    // Auto Pay Hours
    // Populate with "28" if have a Y in Auto Pay field above
    response.autoPayHours = this.getAutoPayHours(response.autoPay);

    // Payslip Type
    // Populate with 'E'
    response.payslipType = 'E';

    // Delivery Method
    // Populate with 'E'
    response.deliveryMethod = 'E';

    // Email To
    // Populate with 'P'
    response.emailTo = 'P';

    // Salary Code
    // If the employee has a 'Position Type' of FT or PT
    // and 'Salaried Employee' says 'Y' leave blank,
    // if the employee has any other Position type:
    // - Calculate their age from the Date of Birth field
    // - Add together their: 'Level"", ""Employment Classification"" and ""Age""
    // - Use a table in the background to apply the salary code"
    const yearLeftSchool = this.getCustomFieldValue('Year Left School');
    const stillAtSchool = yearLeftSchool === YearLeftSchool.StillAtSchool;

    // Get salary code
    if (!this.shouldUseTraineeSalaryCodes(restaurant, response.trainee, stillAtSchool)) {
      // Normal salary codes
      const dateOfBirth = this.getCustomFieldValue("01. Crew Member's Date Of Birth (RM must complete when creating account)");
      const age = this.calculateAge(dateOfBirth);
      response.salaryCode = await this.getSalaryCode(data.positionType, age, wmLevel, salariedEmployee);
    } else {
      // Trainee salary codes
      const dateLeftSchool = this.getCustomFieldValue('Date Left School');
      const yearsSinceLeftSchool = this.calculateYearsSinceLeftSchool(dateLeftSchool);
      response.salaryCode = await this.getTraineeSalaryCode(yearLeftSchool, yearsSinceLeftSchool);
    }

    // Rate Frequency
    // If the employee has a 'Position Type' of FT or PT
    // and 'Salaried Employee' says 'Y' then populate with 'A',
    // otherwise leave blank
    response.rateFrequency = this.getRateFrequency(data.positionType, salariedEmployee);

    // Base Pay Rate
    response.basePayRate = this.NAToBlank(WorldManagerToPeopleStremeMapping.parseBasePayRate(this.getCustomFieldValue('06. Salary')));

    // Cost Level 1
    // Get corporate or franchise field from WM
    response.costLevel1 = await this.getCostLevel1(data.storeUuids);

    // Cost Level 2
    // Get location from WM
    response.costLevel2 = await this.getStateCode(data.storeUuids);

    // Cost Level 3
    // Restaurant code
    response.costLevel3 = await this.getRestaurantCode(data.storeUuids);

    // Cost Level 4
    // Position type code
    response.costLevel4 = await this.getCostLevel4(positionTitle);

    // Cost Level 5
    // Position type code
    response.costLevel5 = await this.getCostLevel5(data.storeUuids);

    // Employment Type
    // Recognise Employment Type by referencing the 'Position Type' and 'Salaried Employee'
    // Field from WM and apply relevant code from a table
    response.employmentType = this.getEmploymentType(data.positionType, salariedEmployee);

    // Personnel Type
    // Populate all with 'D'
    response.personnelType = 'D';

    // Location
    if (this.restaurants[data.storeUuids]) {
      response.location = this.restaurants[data.storeUuids].Name;
    } else {
      response.location = '';
    }

    // Additional Form Recipient (RM email)
    // Recognise in integration from a background table based on restaurant location.
    // Will be RM emails for corporate and Franchisee
    // or RM email for franchisee (never restaurant email)
    // Delimited by a pipe “|” character, eg. email@email.com|test@email.com
    response.additionalFormRecipient = await this.getAdditionalFormRecipients(data.storeUuids, positionTitle);

    // Company ABN
    // Recognise in integration from background table based on restaurant location
    response.companyABN = this.getABN(data.storeUuids);

    // Account
    response.account = '';

    // Department
    response.department = '';
    response.newPrecedaId = this.getCustomFieldValue('38. Preceda I.D');
    return response;
  }

  /**
   * Check if employee has trainee question because not every restaurant has that
   * If it doesn't, set value to No
   *
   */
  getTraineeQuestion() {
    const trainee = this.getCustomFieldValue('07. Trainee');

    if (trainee === '') {
      return 'N';
    }

    return trainee;
  }

  /**
   * Check if we should use trainee salary codes
   *
   * @param {*} restaurant
   * @param {*} trainee
   * @param {*} stillAtSchool
   */
  shouldUseTraineeSalaryCodes(restaurant, trainee, stillAtSchool) {
    return this.restaurantHasTraineeSalaryCodes(restaurant)
      && trainee === 'Y'
      && !stillAtSchool;
  }

  /**
   * Convert N/A string to blank
   *
   * @param {string} value
   */
  NAToBlank(value) {
    if (value && value === NA) {
      return '';
    }

    return value;
  }

  /**
   * Replace . ; and , for :
   *
   * @param {string} text
   */
  replaceWithColon(text) {
    return text.replace(/[.;,]/g, ':');
  }

  /**
   * Parse base pay rate
   *
   * @param {string} value
   */
  static parseBasePayRate(value) {
    if (value) {
      // Remove comma
      return value.replace(/,/g, '');
    }

    return '';
  }

  getCustomFieldValue(fieldName) {
    if (!this.customDetails || !fieldName) {
      return '';
    }

    const customField = _.find(this.customDetails, { fieldName });
    if (customField && customField.value) {
      return customField.value;
    }

    // Not found
    return '';
  }

  /**
   * Get manager's email
   *
   * @param {*} location
   * @param {*} positionName
   */
  async getManagerEmail(location, positionName) {
    if (!location || positionName === positionNames.RestaurantManager) {
      return '';
    }

    // Get restaurants
    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.Email) {
      return restaurant.Email;
    }

    // Not found
    return '';
  }

  /**
    * Map gender values
    *
    * @param {*} gender
    */
  mapGender(gender) {
    if (!gender) {
      return Gender.Default;
    }

    switch (gender.toLowerCase()) {
      case 'male':
        return Gender.Male;
      case 'female':
        return Gender.Female;
      default:
        return Gender.Default;
    }
  }

  /**
   * Get restaurant code
   *
   * @param {*} location
   */
  async getRestaurantCode(location) {
    if (!location) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.Code) {
      return restaurant.Code;
    }

    // Not found
    return '';
  }

  getABN(location) {
    if (!location) {
      return '';
    }
    // TODO
    return 'ABN';
  }

  /**
   * Get state code
   *
   * @param {*} location
   */
  async getStateCode(location) {
    if (!location) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.State) {
      return restaurant.State;
    }

    // Not found
    return '';
  }

  getDepartmentCode() {
    // TODO
    return 'departmentCode';
  }

  getPositionTypeCode(positionTitle) {
    if (!positionTitle) {
      return '';
    }
    // TODO
    return 'positionTypeCode';
  }

  /**
   * Get company name
   *
   * @param {*} location
   */
  async getCompanyName(location) {
    if (!location) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.CompanyName) {
      return restaurant.CompanyName;
    }

    // Not found
    return '';
  }

  /**
   * Get position ID
   *
   * @param {*} location
   * @param {*} positionName
   */
  async getPositionId(location, positionName) {
    if (!location || !positionName) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    // Restaurant not found
    if (!restaurant || !restaurant.Positions) {
      return '';
    }

    const position = _.find(restaurant.Positions, {
      Title: positionName,
    });

    if (position) {
      return position.ID;
    }

    // Not found
    return '';
  }

  /**
   * Get restaurant data
   * @param {*} location
   */
  async getRestaurant(location) {
    if (!location) {
      return null;
    }

    let restaurant;

    // Restaurant not found
    if (!this.restaurants[location]) {
      restaurant = await this.integrationTables.getRestaurant(location);
      this.restaurants[location] = restaurant.body;
      return restaurant.body;
    }

    return this.restaurants[location];
  }

  /**
   * Get restaurant award
   *
   * @param {*} location
   */
  async getAward(location) {
    if (!location) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.Award) {
      return restaurant.Award;
    }

    // Not found
    return '';
  }

  /**
   * Map position type
   * This is the name of Employment Type (Position Type), if the field in WM for Salaried says 'Y'
   * the Full-time or Part-time needs updating to say Full-time Salaried or Part-time Salaried.
   *
   * @param {*} positionType
   * @param {*} salariedEmployee
   */
  async mapPositionType(positionType, salariedEmployee) {
    if (!positionType || !salariedEmployee) {
      return '';
    }

    // If position is Casual, return Casual
    if (positionType === PositionType.Casual) {
      return PositionType.Casual;
    }

    const positionTypes = await this.integrationTables.getPositionType();

    const findResult = _.find(positionTypes, {
      WMFieldSalariedEmployee: salariedEmployee,
      WMFieldPositionType: positionType,
    });

    if (findResult && findResult.EmploymentType) {
      return findResult.EmploymentType;
    }

    // Not found
    return '';
  }

  /**
   * Base Hours
   * If Position type is FT and Salaried says 'Y' = 38,
   * If position Type is PT and Salaried says 'Y' = populate with
   * Total Mon-Sun hrs fields added together e.g.20,
   * If FT and Salaried says 'N' = F,
   * If PT and Salaried says 'N' = P
   * If Casual = C
   * @param {*} positionType
   * @param {*} salariedEmployee
   */
  getBaseHours(positionType, salariedEmployee) {
    if (!positionType) {
      return '';
    }

    switch (positionType) {
      case PositionType.FullTime:
        switch (salariedEmployee) {
          case 'Y':
            return '38';
          case 'N':
            return 'F';
          default:
            return '';
        }
      case PositionType.PartTime:
        switch (salariedEmployee) {
          case 'Y':
            return 'addHours';
          case 'N':
            return 'P';
          default:
            return '';
        }
      case PositionType.Casual:
        return 'C';
      default:
        return '';
    }
  }

  /**
   * Add total hours from Monday to Sunday
   *
   * @param {*} hours
   */
  addTotalHours(hours) {
    return Number(hours.mondayTotalHours, 10) + Number(hours.tuesdayTotalHours, 10)
      + Number(hours.wednesdayTotalHours, 10)
      + Number(hours.thursdayTotalHours, 10)
      + Number(hours.fridayTotalHours, 10)
      + Number(hours.saturdayTotalHours, 10) + Number(hours.sundayTotalHours, 10);
  }

  /**
   * Get auto pay
   * Populate with 'Y' if have 'Y' in WM Salaried Employee field, otherwise populate with 'N'
   * @param {*} salaried
   */
  getAutoPay(salaried) {
    return salaried;
  }

  /**
   * Populate with "28" if have a Y in Auto Pay field above
   *
   * @param {*} autoPay
   */
  getAutoPayHours(autoPay) {
    if (autoPay === 'Y') {
      return '28';
    }

    return '';
  }

  /**
   * Calculate age from date of birth
   * @param {*} dateOfBirth
   */
  calculateAge(dateOfBirth) {
    const birthDate = moment(dateOfBirth, DateFormats.Input);
    return moment().diff(birthDate, 'years');
  }

  /**
   * Calculate years since left school
   *
   * @param {string} dateLeftSchool
   */
  calculateYearsSinceLeftSchool(dateLeftSchool) {
    const yearLeftSchool = moment(dateLeftSchool, DateFormats.DateLeftSchoolInput).year();
    return moment().year() - yearLeftSchool;
  }

  /**
   * Map position type, age and wmLevel to salary code
   * If the employee has a 'Position Type' of FT or PT
   * and 'Salaried Employee' says 'Y' leave blank,
   * if the employee has any other Position type:
   * - Calculate their age from the Date of Birth field
   * - Add together their: Level, Employment Classification and Age
   * - Use a table in the background to apply the salary code
   *
   *
   * @param {*} positionType
   * @param {*} age
   * @param {*} wmLevel
   * @param salariedEmployee
   */
  async getSalaryCode(positionType, age, wmLevel, salariedEmployee) {
    if (!positionType || !age || !wmLevel || !salariedEmployee) {
      return '';
    }

    if (salariedEmployee === 'Y' && (positionType === PositionType.FullTime || positionType === PositionType.PartTime)) {
      return '';
    }

    // Get salary codes table
    const salaryCodes = await this.integrationTables.getSalaryCodes();

    if (!salaryCodes) {
      return '';
    }

    // Age mapping
    // age < 16 --> Under 16
    // age > 20 --> 21+
    const mappedAge = this.mapAge(age);

    // If position type is Casual, add Casual to the wmLevel
    // For example FFIA LVL 1 Casual
    // Update: No longer needed as the `Casual` word has been removed from the level column
    // const mappedLevel = positionType === PositionType.Casual ? `${wmLevel} ${positionType}` : wmLevel;

    const matchingSalaryCode = _.find(salaryCodes, (salaryCode) => {
      // Check if PositionType is of type array
      if (salaryCode.PositionType.values) {
        return salaryCode.PositionType.values.includes(positionType) && salaryCode.Age === mappedAge
          && salaryCode.Level === wmLevel;
      }

      return salaryCode.PositionType === positionType && salaryCode.Age === mappedAge
        && salaryCode.Level === wmLevel;
    });

    if (matchingSalaryCode) {
      return matchingSalaryCode.SalaryCode;
    }

    // Not found
    return '';
  }

  /**
   * Get trainee salary code
   *
   * @param {string} yearLeftSchool
   * @param {number} yearsSinceLeftSchoolParam
   */
  getTraineeSalaryCode(yearLeftSchool, yearsSinceLeftSchoolParam) {
    let yearsSinceLeftSchool;
    let maxYear;

    switch (yearLeftSchool) {
      case YearLeftSchool.Year10:
        maxYear = 5;
        break;
      case YearLeftSchool.Year11:
        maxYear = 4;
        break;
      case YearLeftSchool.Year12:
        maxYear = 3;
        break;
      default:
        return null;
    }

    // Business rule:
    // If it is less than a year the number will be 1
    // If it is more than 1 year but less than 2 years the number will be 2
    // If it is more than 2 years but less than 3 years the number will be 3
    // If it is more than 3 year but less than 4 years the number will be 4
    // If it is more than 4 year but less than 5 years the number will be 5
    // If it is more than 5 years the number will be 6
    if (yearsSinceLeftSchoolParam > maxYear) {
      yearsSinceLeftSchool = maxYear + 1;
    } else {
      yearsSinceLeftSchool = yearsSinceLeftSchoolParam + 1;
    }

    switch (yearLeftSchool) {
      case YearLeftSchool.Year10:
        return traineeSalaryCodes.Year10 + yearsSinceLeftSchool;
      case YearLeftSchool.Year11:
        return traineeSalaryCodes.Year11 + yearsSinceLeftSchool;
      case YearLeftSchool.Year12:
        return traineeSalaryCodes.Year12 + yearsSinceLeftSchool;
      default:
        return '';
    }
  }

  /**
   * Map age
   *
   * @param {*} age
   */
  mapAge(age) {
    if (age < 16) {
      return Age.Under16;
    }

    if (age > 20) {
      return Age.Over20;
    }

    // Cast to string for consistency
    return age.toString();
  }

  /**
   * If the employee has a 'Position Type' of FT or PT
   * and 'Salaried Employee' says 'Y' then populate with 'A',
   * otherwise leave blank
   *
   * @param {*} positionType
   * @param {*} salaried
   */
  getRateFrequency(positionType, salaried) {
    if (!positionType || !salaried) {
      return '';
    }

    if ((positionType === PositionType.FullTime || positionType === PositionType.PartTime)
          && salaried === 'Y') {
      return 'A';
    }

    return '';
  }

  /**
   * Get cost level 1
   * @param {string} location
   * Will be RG for all Corporates.
   * Use PSName value as CostLevel1 for active Franchised locations.
   */

  async getCostLevel1(location) {
    if (!location) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.Type === 'F') {
      return restaurant.PSName;
    }

    // RG for all Corporates.
    return 'RG';
  }

  /**
   * Get cost level 5 value
   *
   * @param {string} location
   */
  async getCostLevel5(location) {
    if (!location) {
      return '';
    }

    const restaurant = await this.getRestaurant(location);

    if (restaurant && restaurant.CostLevel5) {
      return restaurant.CostLevel5;
    }

    // Not found
    return '';
  }

  /**
   * Get cost level 4
   *
   * @param {*} positionTitle
   */
  async getCostLevel4(positionTitle) {
    if (!positionTitle) {
      return '';
    }

    const costLevel4 = await this.integrationTables.getCostLevel4();

    const level4 = _.find(costLevel4, {
      PositionTitle: positionTitle,
    });

    if (level4 && level4.PositionCode) {
      return level4.PositionCode;
    }

    // Not found
    return '';
  }

  /**
   * Get level
   *
   * @param {*} employmentType
   */
  async getLevel(employmentType) {
    if (!employmentType) {
      return '';
    }

    const levels = await this.integrationTables.getLevels();

    const level = _.find(levels, {
      WMEmploymentType: employmentType,
    });

    if (level && level.PSLevel) {
      return level.PSLevel;
    }

    // Not found
    return '';
  }

  /**
   * Get employment type
   *
   * If Full Time salaried then 1
   * If part time salaried then 2
   * If full time then 3
   * If part time then 4
   * If casual then 5
   * @param {*} positionType
   * @param {*} salaried
   */
  getEmploymentType(positionType, salaried) {
    if (!positionType || !salaried) {
      return '';
    }

    switch (positionType) {
      case PositionType.FullTime:
        return salaried === 'Y' ? 1 : 3;
      case PositionType.PartTime:
        return salaried === 'Y' ? 2 : 4;
      case PositionType.Casual:
        return 5;
      default:
        return '';
    }
  }

  /**
   * Get additional form recipients
   * @param {*} location
   * @param {*} positionName
   */
  async getAdditionalFormRecipients(location, positionName) {
    return this.getManagerEmail(location, positionName);
  }

  /**
   * Check if restaurant has new trainee salary codes
   *
   * @param {*} restaurant
   */
  restaurantHasTraineeSalaryCodes(restaurant) {
    if (restaurant && restaurant.HasTraineeSalaryCodes) {
      return true;
    }

    // No trainee salary codes for this restaurant
    return false;
  }
}

module.exports = WorldManagerToPeopleStremeMapping;
