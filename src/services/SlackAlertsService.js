
const https = require('https');

const SlackURL = 'hooks.slack.com';


class SlackAlertsService {
  constructor(options) {
    this.webHook = options.webHook;
    this.msgPrefix = options.msgPrefix || 'TEST';
  }

  sendError(message) {
    return this.sendMessage(`*GYG WM-PS-Middleware error:* ${message}`);
  }

  sendMessage(message) {
    const prefix = this.msgPrefix;
    const payload = JSON.stringify({
      text: `*[${prefix}]* ${message}`,
    });

    const options = {
      hostname: SlackURL,
      method: 'POST',
      path: this.webHook,
    };

    return new Promise((resolve, reject) => {
      const req = https.request(options, res => res.on('data', (data) => {
        resolve(data.toString());
      }));

      req.on('error', (error) => {
        console.log('Slack Error: ', JSON.stringify(error));
        reject(error);
      });

      req.write(payload);
      req.end();
    });
  }
}

module.exports = SlackAlertsService;
