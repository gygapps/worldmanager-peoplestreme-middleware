const DynamoDB = require('./DynamoDB');

/**
 * Class to save and read Preceda accounts to and from a DynamoDB table
 *
 */
class WorldManagerImportErrorsStorage {
  constructor() {
    this.dynamoDB = new DynamoDB();
    // Default table name for testing
    this.tableName = process.env.WORLD_MANAGER_IMPORT_ERRORS_TABLE_NAME || 'dev-WMImportError';
  }

  /**
   * Save error
   *
   * @param {Object} account
   */
  async saveError(error) {
    return this.dynamoDB.putItem(this.tableName, error);
  }

  /**
   * Get all errors by timestamp
   *
   * @param {string} timestamp
   */
  async getAllErrors(timestamp) {
    return this.dynamoDB.queryTable(this.tableName, 'Timestamp', timestamp);
  }
}

module.exports = WorldManagerImportErrorsStorage;
