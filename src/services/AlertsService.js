class AlertsService {
  constructor(options) {
    this.services = options.services;
  }

  /**
   * Send alerts using all services in this.services
   *
   * @param {string} message
   */
  async sendError(message) {
    const responses = [];

    // eslint-disable-next-line no-restricted-syntax
    for (const service of this.services) {
      if (service.sendError && typeof service.sendError === 'function') {
        // eslint-disable-next-line no-await-in-loop
        const response = await service.sendError(message);
        responses.push(response);
      }
    }

    return responses;
  }
}

module.exports = AlertsService;
