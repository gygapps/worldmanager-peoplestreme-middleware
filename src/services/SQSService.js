const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

/**
 * This class is a wrapper for AWS SQS SDK
 *
 */
class SQSService {
  constructor() {
    AWS.config.update({
      region: 'ap-southeast-2',
    });
    this.sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
  }

  /**
   * Send a message to a queue
   *
   * @param {Object} options
   */
  async sendMessage(options) {
    const params = {
      DelaySeconds: options.delaySeconds || 10,
      MessageAttributes: options.messageAttributes,
      MessageBody: options.messageBody,
      QueueUrl: options.queueUrl,
    };

    return new Promise((resolve, reject) => {
      this.sqs.sendMessage(params, (err, data) => {
        if (err) {
          console.log('Error', err);
          return reject(err);
        }

        return resolve(data);
      });
    });
  }
}

module.exports = SQSService;
