const moment = require('moment-timezone');

const WorldManagerToPeopleStremeMapping = require('../../src/services/WorldManagerToPeopleStremeMapping');
const WorldManager = require('../../src/services/WorldManager');
const CSVStandard = require('../../src/services/CSVStandard');
const CSVCustom = require('../../src/services/CSVOnboardingTemplate');
const SFTPService = require('../../src/services/SFTPService');
const WorldManagerAccounts = require('../../src/services/WorldManagerAccounts');

/**
 * Class to create a CSV file from World Manager accounts
 * and upload it to an SFTP server
 *
 */
class WorldManagerToPeopleStremeExport {
  constructor(options) {
    this.worldManager = new WorldManager(options.worldManager);
    this.worldManagerToPeopleStremeMapping = new WorldManagerToPeopleStremeMapping();
    this.sftpService = new SFTPService(options.sftpConfig);
    this.worldManagerAccounts = new WorldManagerAccounts();
    this.standardFileName = options.standardFileName || 'testing_standard.csv';
    this.customFileName = options.customFileName || 'testing_custom.csv';
    this.alertsService = options.alertsService || null;
  }

  async exportAccountsCreatedSinceYesterday() {
    const yesterday = moment().subtract(1, 'days');

    return this.exportAccountsCreatedSince(yesterday.format('YYYY-MM-DD'));
  }

  /**
   * Export accounts created since some date
   * Date format is yyyy-mm-ddThh:mm:ssZ (for example 2019-07-15T00:00:00Z)
   * @param {*} timestamp
   */
  async exportAccountsCreatedSince(timestamp) {
    console.log(`Exporting accounts created since ${timestamp}`);
    // Call WM API to get account created since yesterday
    const recentWorldManagerAccounts = await this.getAccountsCreatedSince(timestamp);

    if (!recentWorldManagerAccounts || recentWorldManagerAccounts.length === 0) {
      console.log('No accounts found');
      return {
        statusCode: 200,
        processedAccounts: 0,
      };
    }

    console.log(`Found ${recentWorldManagerAccounts.length} accounts updated in WM since ${timestamp}`);

    let accountsToExport;

    console.log('Checking which accounts we should export...');

    try {
      // Get the accounts that were not processed and saved to DynamoDB
      accountsToExport = await this.getAccountsThatWereUpdated(recentWorldManagerAccounts);
    } catch (error) {
      console.error(error);

      // Send alerts
      if (this.alertsService) {
        await this.alertsService.sendError('Failed getting WM accounts to export.');
        await this.alertsService.sendError(error);
      }
      return {
        statusCode: 500,
        processedAccounts: 0,
        error,
      };
    }

    if (!accountsToExport || accountsToExport.length === 0) {
      console.log('No new accounts to export found. Done!');
      return {
        statusCode: 200,
        processedAccounts: 0,
        body: 'No new accounts to export found.',
      };
    }

    console.log(`Found ${accountsToExport.length} accounts to export.`);

    let PSAccounts;

    try {
      // Map WM fields to PS
      console.log('Mapping accounts...');
      PSAccounts = await this.mapAccounts(accountsToExport);

      // If there are no accounts, don't create the CSV file
      if (!PSAccounts || PSAccounts.numberOfAccounts === 0) {
        console.log('No new PS accounts to export found. Done!');
        return {
          statusCode: 200,
          processedAccounts: 0,
          body: 'No new PS accounts to export found.',
        };
      }

      // Save CSV and upload to SFTP
      await this.saveFileToCSVAndUploadToSFTP(CSVStandard, PSAccounts.standardFile, this.standardFileName);
      await this.saveFileToCSVAndUploadToSFTP(CSVCustom, PSAccounts.customFile, this.customFileName);
    } catch (error) {
      console.error(error);

      // Send alerts
      if (this.alertsService) {
        await this.alertsService.sendError('Failed exporting accounts.');
        await this.alertsService.sendError(error);
      }

      return {
        statusCode: 500,
        processedAccounts: 0,
        error,
      };
    }

    // Save last updated date to DynamoDB so we don't save the same account multiple times
    console.log('Saving processed accounts...');
    try {
      await this.saveProcessedAccounts(accountsToExport);
    } catch (error) {
      console.error(error);

      // Send alerts
      if (this.alertsService) {
        await this.alertsService.sendError('Failed saving processed accounts to DB.');
        await this.alertsService.sendError(error);
      }

      return {
        statusCode: 500,
        processedAccounts: 0,
        error,
      };
    }

    console.log('Done!');

    return {
      statusCode: 200,
      processedAccounts: PSAccounts.numberOfAccounts,
    };
  }

  /**
   * Call WM API to get all accounts created since some date
   *
   * @param {*} timestamp
   */
  async getAccountsCreatedSince(timestamp) {
    return this.worldManager.findAccountsWithCustomFieldsCreatedSince(`${timestamp}T00:00:00Z`);
  }

  async getAccountsThatWereUpdated(accounts) {
    const accountsToExport = [];

    // Check DynamoDB tables for last updated date of each account
    for (let i = 0; i < accounts.length; i += 1) {
      const savedAccount = await this.worldManagerAccounts.getAccountByUUID(accounts[i].uuid);

      if (!savedAccount || savedAccount.body === undefined
        || (savedAccount.body && savedAccount.body.CreatedAt !== accounts[i].createdAt)) {
        accountsToExport.push(accounts[i]);
      } else if (savedAccount.body.CreatedAt) {
        console.log(`Account ${accounts[i].uuid} already exported on ${accounts[i].updatedAt}.`);
      } else {
        console.log(`Account ${accounts[i].uuid} already exported.`);
      }
    }

    return accountsToExport;
  }

  async mapAccounts(accounts) {
    const PSAccounts = {
      standardFile: [],
      customFile: [],
      numberOfAccounts: 0,
    };

    try {
      for (let i = 0; i < accounts.length; i += 1) {
        const standardFileMapping = await this.worldManagerToPeopleStremeMapping.peopleStremeStandardFileMapping(accounts[i]);
        const customFileMapping = await this.worldManagerToPeopleStremeMapping.peopleStremeCustomFileMapping(accounts[i]);

        if (standardFileMapping) {
          PSAccounts.standardFile.push(standardFileMapping);
        }

        if (customFileMapping) {
          PSAccounts.customFile.push(customFileMapping);
        }

        if (customFileMapping && standardFileMapping) {
          console.log(`Mapped account ${accounts[i].uuid}`);
          PSAccounts.numberOfAccounts += 1;
        } else {
          console.log(`Could not map account ${accounts[i].uuid}`);
        }
      }
    } catch (error) {
      console.log(error);
    }

    return PSAccounts;
  }

  async saveFileToCSVAndUploadToSFTP(CSVService, accounts, filename) {
    const localFilePath = '/tmp/PSAccountsStandard.csv';
    // Create CSV file and export to file
    console.log('Creating CSV Standard file');
    const fileResponse = CSVService.exportCSVToFile(accounts, localFilePath);

    if (!fileResponse) {
      console.log(`Error exporting to CSV file ${localFilePath}.`);
      // TODO. Handle error
    }

    // Upload to SFTP
    console.log('Uploading CSV Standard file to SFTP server...');
    const SFTPResponse = await this.sftpService.uploadLocalFile(localFilePath, filename);

    // backup file to backup directory
    console.log('Backing up to backup folder on server...');
    await this.sftpService.uploadLocalFile(localFilePath, this.createBackupFile(filename));
    return SFTPResponse;
  }

  createBackupFile(filename) {
    const timestamp = moment().tz('Australia/Sydney').format('YYYY-MM-DDTHHmm');
    const bits = filename.split('.');
    return `${this.sftpService.backupDirectory}/${bits[0]}_${timestamp}.${bits[1]}`;
  }

  async saveProcessedAccounts(accounts) {
    for (let i = 0; i < accounts.length; i += 1) {
      const account = {
        UUID: accounts[i].uuid,
        CreatedAt: accounts[i].createdAt, // Created in WorldManager
        ExportedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      await this.worldManagerAccounts.saveAccount(account);
      console.log(`Saved account ${accounts[i].uuid}`);
    }
  }
}

module.exports = WorldManagerToPeopleStremeExport;
