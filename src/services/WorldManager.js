const _ = require('lodash');
const { parseString } = require('xml2js');
const unirest = require('unirest');
const moment = require('moment');

const WorldManagerApi = require('../api/WorldManager');

/**
 * Class to call World Manager API endpoints
 * and parse XML responses
 *
 */
class WorldManager {
  constructor(options) {
    if (!options.tokenURL) {
      throw Error('No token URL.');
    }

    if (!options.apiURL) {
      throw Error('No API URL.');
    }

    if (!options.clientId) {
      throw Error('No client ID.');
    }

    if (!options.clientSecret) {
      throw Error('No client secret.');
    }

    this.token = null;

    // List of custom fields UUID and name
    this.customFields = null;

    // Encode authorization header to request token
    const authorizationToken = Buffer.from(`${options.clientId}:${options.clientSecret}`).toString('base64');
    this.authorization = `Basic ${authorizationToken}`;

    this.unirest = unirest('POST', options.tokenURL);
    this.worldManagerApi = new WorldManagerApi(options.apiURL);
  }

  /**
   * Get WorldManager API token
   *
   */
  getToken() {
    if (this.token) {
      return this.token;
    }

    const req = this.unirest;

    req.headers({
      'cache-control': 'no-cache',
      Authorization: this.authorization,
    });

    req.form({
      grant_type: 'client_credentials',
      scope: 'soapApi',
    });

    return new Promise((resolve, reject) => {
      req.end((res) => {
        if (res.error) {
          return reject(res.error);
        }

        // Save token
        this.token = res.body.access_token;

        return resolve(res.body.access_token);
      });
    });
  }

  /**
   * Get account by UUID
   *
   * @param {*} uuid
   */
  async getAccountByUuid(uuid) {
    let token;

    try {
      token = await this.getToken();
    } catch (error) {
      // TODO. Handle error
      throw Error(error);
    }

    // Timestamp example 2018-01-01T10:15:30Z
    const soapEnvelope = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:v7="http://v7.soap.platform.worldmanager.com/">
                              <soap:Header/>
                              <soap:Body>
                                <v7:getAccountByUuid>
                                    <uuid>
                                      <uuid>${uuid}</uuid>
                                    </uuid>
                                </v7:getAccountByUuid>
                              </soap:Body>
                          </soap:Envelope>`;

    try {
      const response = await this.worldManagerApi.post('/',
        soapEnvelope,
        {
          headers:
          {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'text/xml',
          },
        });

      const xml = response.data;

      const accounts = await WorldManager.parseGetAccountByUuid(xml);

      return accounts;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  /**
   * Parse XML response from getAccountByUuid method
   *
   * @param {*} xmlResponse
   */
  static parseGetAccountByUuid(xmlResponse) {
    return new Promise((resolve, reject) => {
      parseString(xmlResponse, (err, result) => {
        if (err) {
          return reject(err);
        }

        let account = result['soap:Envelope']['soap:Body'][0]['ns2:getAccountByUuidResponse'][0].return;

        if (!account[0] || !account[0].account || !account[0].account[0]) {
          return null;
        }

        // eslint-disable-next-line prefer-destructuring
        account = account[0].account[0];

        const accountResponse = {
          uuid: account.uuid[0].uuid[0],
          groupId: WorldManager.parseSingleAttribute(account.groupID),
          // Assuming one country per account
          countryUuids: WorldManager.parseLocationUUID(account.countryUuids[0]),
          // Assuming one area per account
          areaUuids: WorldManager.parseLocationUUID(account.areaUuids[0]),
          // Assuming one store per account
          storeUuids: WorldManager.parseLocationUUID(account.storeUuids[0]),
          username: WorldManager.parseSingleAttribute(account.username),
          person: {
            title: WorldManager.parseSingleAttribute(account.person[0].title),
            firstName: WorldManager.parseSingleAttribute(account.person[0].firstName),
            middleName: WorldManager.parseSingleAttribute(account.person[0].middleName),
            lastName: WorldManager.parseSingleAttribute(account.person[0].lastName),
            nickname: WorldManager.parseSingleAttribute(account.person[0].nickname),
            displayName: WorldManager.parseSingleAttribute(account.person[0].displayName),
          },
          active: WorldManager.parseSingleAttribute(account.active),
          language: WorldManager.parseSingleAttribute(account.language),
          positionName: WorldManager.parseSingleAttribute(account.positionName),
          positionType: WorldManager.parseSingleAttribute(account.positionType),
          email: WorldManager.parseSingleAttribute(account.email),
          phone: WorldManager.parseSingleAttribute(account.phone),
          mobile: WorldManager.parseSingleAttribute(account.mobile),
          receiveEmails: WorldManager.parseSingleAttribute(account.receiveEmails),
          timezone: WorldManager.parseSingleAttribute(account.timezone),
          userDetailsUpdated: WorldManager.parseSingleAttribute(account.userDetailsUpdated),
          userPasswordUpdated: WorldManager.parseSingleAttribute(account.userPasswordUpdated),
          workStartDate: WorldManager.parseSingleAttribute(account.workStartDate),
          createdAt: WorldManager.parseSingleAttribute(account.createdAt),
          updatedAt: WorldManager.parseSingleAttribute(account.updatedAt),
        };

        return resolve(accountResponse);
      });
    });
  }

  /**
   * Find accounts with custom fields created since some date
   *
   * @param {*} timestamp
   */
  async findAccountsWithCustomFieldsCreatedSince(timestamp) {
    const accounts = await this.findAccountsCreatedSince(timestamp);

    if (!accounts) {
      // No accounts found
      return null;
    }

    // For each account, get custom fields
    const accountsWithCustomDetails = [];

    for (let i = 0; i < accounts.length; i += 1) {
      const accountWithCustomDetails = accounts[i];

      const customDetails = await this.findCustomFields(accounts[i].uuid);

      if (customDetails) {
        accountWithCustomDetails.customDetails = customDetails;
      }

      accountsWithCustomDetails.push(accountWithCustomDetails);
    }

    return accountsWithCustomDetails;
  }

  /**
   * Find accounts with custom fields updated since some date
   *
   * @param {*} timestamp
   */
  async findAccountsWithCustomFieldsUpdatedSince(timestamp) {
    const accounts = await this.findAccountsUpdatedSince(timestamp);

    if (!accounts) {
      // No accounts found
      return null;
    }

    // For each account, get custom fields
    const accountsWithCustomDetails = [];

    for (let i = 0; i < accounts.length; i += 1) {
      const accountWithCustomDetails = accounts[i];

      const customDetails = await this.findCustomFields(accounts[i].uuid);

      if (customDetails) {
        accountWithCustomDetails.customDetails = customDetails;
      }

      accountsWithCustomDetails.push(accountWithCustomDetails);
    }

    return accountsWithCustomDetails;
  }

  /**
   * Find all accounts updated since some date
   *
   * @param {*} timestamp
   */
  async findAccountsCreatedSince(timestamp) {
    // Get all accounts updated since some date.
    // API doesn't have a findAccountsCreatedSince method
    // so we need to use findAccountsUpdatedSince and filter by createdAt date
    const accountsUpdatedSince = await this.findAccountsUpdatedSince(timestamp);

    // No accounts found
    if (!accountsUpdatedSince || accountsUpdatedSince.length === 0) {
      return null;
    }

    // Filter by creation date
    const accountsCreatedSince = _.filter(accountsUpdatedSince, (account) => {
      return moment(account.createdAt) >= moment(timestamp);
    });

    return accountsCreatedSince;
  }

  /**
   * Find all accounts updated since some date
   *
   * @param {*} timestamp
   */
  async findAccountsUpdatedSince(timestamp) {
    let token;

    try {
      token = await this.getToken();
    } catch (error) {
      // TODO. Handle error
      throw Error(error);
    }

    // Timestamp example 2018-01-01T10:15:30Z
    const soapEnvelope = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
                              xmlns:v7="http://v7.soap.platform.worldmanager.com/">
                            <soap:Header/>
                            <soap:Body>
                              <v7:findAccountsUpdatedSince>
                                  <timestamp>${timestamp}</timestamp>
                              </v7:findAccountsUpdatedSince>
                            </soap:Body>
                          </soap:Envelope>`;

    try {
      const response = await this.worldManagerApi.post('/',
        soapEnvelope,
        {
          headers:
          {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'text/xml',
          },
        });

      const xml = response.data;

      const accounts = await WorldManager.parseFindAccountsUpdatedSince(xml);

      return accounts;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  /**
   * Parse XML response from findAccountsUpdatedSince method
   *
   * @param {*} xmlResponse
   */
  static parseFindAccountsUpdatedSince(xmlResponse) {
    return new Promise((resolve, reject) => {
      parseString(xmlResponse, (err, result) => {
        if (err) {
          return reject(err);
        }

        let accounts = result['soap:Envelope']['soap:Body'][0]['ns2:findAccountsUpdatedSinceResponse'][0].return;
        accounts = accounts[0].accounts[0].item;

        const accountsResponse = [];

        accounts.forEach((element) => {
          const storeUUID = WorldManager.parseLocationUUID(element.storeUuids[0]);

          accountsResponse.push({
            uuid: element.uuid[0].uuid[0],
            groupId: WorldManager.parseSingleAttribute(element.groupID),
            // Assuming one country per account
            countryUuids: WorldManager.parseLocationUUID(element.countryUuids[0]),
            // Assuming one area per account
            areaUuids: WorldManager.parseLocationUUID(element.areaUuids[0]),
            // Assuming one store per account
            storeUuids: storeUUID ? storeUUID[0] : null,
            username: WorldManager.parseSingleAttribute(element.username),
            person: {
              title: WorldManager.parseSingleAttribute(element.person[0].title),
              firstName: WorldManager.parseSingleAttribute(element.person[0].firstName),
              middleName: WorldManager.parseSingleAttribute(element.person[0].middleName),
              lastName: WorldManager.parseSingleAttribute(element.person[0].lastName),
              nickname: WorldManager.parseSingleAttribute(element.person[0].nickname),
              displayName: WorldManager.parseSingleAttribute(element.person[0].displayName),
            },
            active: WorldManager.parseSingleAttribute(element.active),
            language: WorldManager.parseSingleAttribute(element.language),
            positionName: WorldManager.parseSingleAttribute(element.positionName),
            positionType: WorldManager.parseSingleAttribute(element.positionType),
            email: WorldManager.parseSingleAttribute(element.email),
            phone: WorldManager.parseSingleAttribute(element.phone),
            mobile: WorldManager.parseSingleAttribute(element.mobile),
            receiveEmails: WorldManager.parseSingleAttribute(element.receiveEmails),
            timezone: WorldManager.parseSingleAttribute(element.timezone),
            userDetailsUpdated: WorldManager.parseSingleAttribute(element.userDetailsUpdated),
            userPasswordUpdated: WorldManager.parseSingleAttribute(element.userPasswordUpdated),
            workStartDate: WorldManager.parseSingleAttribute(element.workStartDate),
            createdAt: WorldManager.parseSingleAttribute(element.createdAt),
            updatedAt: WorldManager.parseSingleAttribute(element.updatedAt),
          });
        });

        return resolve(accountsResponse);
      });
    });
  }

  /**
   * Parse single attribute from XML response
   *
   * @param {*} attribute
   */
  static parseSingleAttribute(attribute) {
    if (attribute[0].$) {
      return null;
    }

    return attribute[0];
  }

  /**
   * Parse location UUID response
   *
   * @param {*} location
   */
  static parseLocationUUID(location) {
    if (!location) {
      return null;
    }

    const UUIDs = [];

    location.item.forEach((item) => {
      UUIDs.push(item.uuid[0]);
    });

    return UUIDs;
  }

  /**
   * Get custom field data by account UUID
   *
   * @param {*} uuids
   */
  async findCustomFieldsByAccountIds(uuids) {
    let token;

    try {
      token = await this.getToken();
    } catch (error) {
      // TODO. Handle error
      throw Error(error);
    }

    let uuidsTag = '';

    uuids.forEach((uuid) => {
      uuidsTag += `<uuids>
                    <uuid>${uuid}</uuid>
                  </uuids>`;
    });

    // uuid example 371151c2-df0d-4975-94ef-d1f0f1a9d4f2
    const soapEnvelope = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
                              xmlns:v7="http://v7.soap.platform.worldmanager.com/">
                            <soap:Header/>
                            <soap:Body>
                              <v7:getCustomFieldDataByAccountUuids>
                                  ${uuidsTag}
                              </v7:getCustomFieldDataByAccountUuids>
                            </soap:Body>
                        </soap:Envelope>`;

    try {
      const response = await this.worldManagerApi.post('/',
        soapEnvelope,
        {
          headers:
          {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'text/xml',
          },
        });

      const xml = response.data;

      const customFields = await WorldManager.parseGetCustomFieldDataByAccountUuids(xml);

      return customFields;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  /**
   * Parse XML response from getCustomFieldDataByAccountUuidsResponse
   *
   * @param {*} xmlResponse
   */
  static parseGetCustomFieldDataByAccountUuids(xmlResponse) {
    return new Promise((resolve, reject) => {
      parseString(xmlResponse, (err, result) => {
        if (err) {
          return reject(err);
        }

        // eslint-disable-next-line prefer-destructuring
        const customFields = result['soap:Envelope']['soap:Body'][0]['ns2:getCustomFieldDataByAccountUuidsResponse'][0].return;

        const customFieldsResponse = [];

        customFields.forEach((element) => {
          const newRow = {};

          // eslint-disable-next-line prefer-destructuring
          newRow.uuid = element.uuid[0].uuid[0];
          newRow.customFields = [];

          // Custom fields
          element.fields[0].item.forEach((field) => {
            const fieldName = field.fieldName[0];

            let fieldValue = null;

            if (!field.value[0].$) {
              // eslint-disable-next-line prefer-destructuring
              fieldValue = field.value[0];
            }

            newRow.customFields.push({
              name: fieldName,
              value: fieldValue,
            });
          });

          customFieldsResponse.push(newRow);
        });

        return resolve(customFieldsResponse);
      });
    });
  }

  /**
   * Find custom fields by account UUID
   *
   * @param {*} uuid
   */
  async findCustomFields(uuid) {
    let token;

    try {
      token = await this.getToken();
    } catch (error) {
      // TODO. Handle error
      throw Error(error);
    }

    const soapEnvelope = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
                              xmlns:v7="http://v7.soap.platform.worldmanager.com/">
                            <soap:Header/>
                            <soap:Body>
                              <v7:findCustomFieldsByAccountUuid>
                                  <uuid>
                                    <uuid>${uuid}</uuid>
                                  </uuid>
                              </v7:findCustomFieldsByAccountUuid>
                            </soap:Body>
                          </soap:Envelope>`;

    try {
      const response = await this.worldManagerApi.post('/',
        soapEnvelope,
        {
          headers:
          {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'text/xml',
          },
        });

      const xml = response.data;

      const customFields = await WorldManager.parseFindCustomFields(xml);

      // Order alphabetically
      const sortedCustomFields = _.orderBy(customFields, ['fieldName'], ['asc']);

      return sortedCustomFields;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  /**
   * Parse XML response from findCustomFieldsByAccountUuidResponse
   *
   * @param {*} xmlResponse
   */
  static parseFindCustomFields(xmlResponse) {
    return new Promise((resolve, reject) => {
      parseString(xmlResponse, (err, result) => {
        if (err) {
          return reject(err);
        }

        // eslint-disable-next-line prefer-destructuring
        let customFields = result['soap:Envelope']['soap:Body'][0]['ns2:findCustomFieldsByAccountUuidResponse'][0].return[0].customFields;
        customFields = customFields[0].item;

        const customFieldsResponse = [];

        customFields.forEach((element) => {
          let value = null;

          if (!element.value[0].$) {
            // eslint-disable-next-line prefer-destructuring
            value = element.value[0];
          }

          customFieldsResponse.push({
            fieldName: element.fieldName[0],
            value,
          });
        });

        return resolve(customFieldsResponse);
      });
    });
  }

  /**
   * Update World Manager account
   *
   * @param {*} accountData
   */
  async updateAccount(accountData) {
    let token;

    try {
      token = await this.getToken();
    } catch (error) {
      // TODO. Handle error
      throw Error(error);
    }

    // Get custom fields
    if (!this.customFields) {
      this.customFields = await this.getCustomFields();
    }

    // XML for custom fields
    const customFields = this.createCustomFieldsTag(accountData);

    const soapEnvelope = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:v7="http://v7.soap.platform.worldmanager.com/">
                          <soap:Header/>
                          <soap:Body>
                            <v7:updateAccount>
                                <uuid>
                                  <uuid>${accountData.uuid}</uuid>
                                </uuid>
                                <account>
                                  <groupID>${accountData.groupId}</groupID>
                                  <storeUuids>
                                      <!--Zero or more repetitions:-->
                                      <item>
                                        <uuid>${accountData.storeUuids}</uuid>
                                      </item>
                                  </storeUuids>
                                  <username>${accountData.username}</username>
                                  <person>
                                      <title>${accountData.person.title}</title>
                                      <firstName>${accountData.person.firstName}</firstName>
                                      <middleName>${accountData.person.middleName}</middleName>
                                      <lastName>${accountData.person.lastName}</lastName>
                                      <nickname>${accountData.person.nickname}</nickname>
                                      <displayName>${accountData.person.displayName}</displayName>
                                  </person>
                                  <positionType>${accountData.positionType}</positionType>
                                  <email>${accountData.email}</email>
                                  <mobile>${accountData.mobile}</mobile>
                                  <workStartDate>${accountData.workStartDate}</workStartDate>
                                  <active>${accountData.active}</active>
                                  ${customFields}
                                </account>
                            </v7:updateAccount>
                          </soap:Body>
                      </soap:Envelope>`;

    try {
      const response = await this.worldManagerApi.post('/',
        soapEnvelope,
        {
          headers:
          {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'text/xml',
          },
        });

      const xml = response.data;
      // TODO. Parse response to check for errors
      return true;
    } catch (error) {
      if (error.response && error.response.data) {
        const parsedErrorMsg = await WorldManager.parseErrorResponse(error.response.data);
        console.error('Error updating account:', accountData);
        console.error('API response:', parsedErrorMsg);
        return {
          error: parsedErrorMsg,
        };
      }

      return null;
    }
  }

  /**
   * Create custom fields tag
   *
   * @param {*} accountData
   */
  createCustomFieldsTag(accountData) {
    let customFields = '';

    if (accountData.customFields) {
      customFields += '<customFields>';

      accountData.customFields.forEach((customField) => {
        // Only add it if it's a valid value
        if (this.isValidCustomFieldValue(customField.uuid, customField.value)) {
          customFields += `<item>
                              <uuid>
                                <uuid>${customField.uuid}</uuid>
                              </uuid>
                              <value>${customField.value}</value>
                          </item>`;
        }
      });
      customFields += '</customFields>';
    }

    return customFields;
  }

  /**
   * Check if custom field value is valid
   *
   * @param {*} customFieldUUID
   * @param {*} value
   */
  isValidCustomFieldValue(customFieldUUID, value) {
    // Get custom field
    const customField = _.find(this.customFields, { uuid: customFieldUUID });

    // Invalid custom field UUID
    if (!customField) {
      return false;
    }

    // Custom field doesn't have list of valid values so value is valid
    if (!customField.validValues) {
      return true;
    }

    // Check if value is in validValues array
    return _.includes(customField.validValues, value);
  }

  /**
   * Get list of custom fields from World Manager API
   *
   */
  async getCustomFields() {
    let token;

    try {
      token = await this.getToken();
    } catch (error) {
      console.error(error);
      // TODO. Handle error
      throw Error(error);
    }

    // Timestamp example 2018-01-01T10:15:30Z
    const soapEnvelope = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:v7="http://v7.soap.platform.worldmanager.com/">
                              <soap:Header/>
                              <soap:Body>
                                <v7:getCustomFields/>
                              </soap:Body>
                          </soap:Envelope>`;

    try {
      const response = await this.worldManagerApi.post('/',
        soapEnvelope,
        {
          headers:
          {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'text/xml',
          },
        });

      const xml = response.data;

      const customFields = await WorldManager.parseGetCustomFields(xml);

      return customFields;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  /**
   * Parse XML response from getCustomFieldsResponse
   *
   * @param {*} xmlResponse
   */
  static parseGetCustomFields(xmlResponse) {
    return new Promise((resolve, reject) => {
      parseString(xmlResponse, (err, result) => {
        if (err) {
          return reject(err);
        }

        // eslint-disable-next-line prefer-destructuring
        const customFields = result['soap:Envelope']['soap:Body'][0]['ns2:getCustomFieldsResponse'][0].return;

        const customFieldsResponse = [];

        customFields.forEach((element) => {
          customFieldsResponse.push({
            uuid: element.uuid[0].uuid[0],
            name: element.name[0],
            validValues: element.validValues[0].item ? element.validValues[0].item : null,
          });
        });

        return resolve(customFieldsResponse);
      });
    });
  }

  /**
   * Parse XML error message
   *
   * @param {*} xmlError
   */
  static parseErrorResponse(xmlError) {
    return new Promise((resolve, reject) => {
      parseString(xmlError, (err, result) => {
        if (err) {
          return reject(err);
        }

        // eslint-disable-next-line prefer-destructuring
        const faultReasonText = result['soap:Envelope']['soap:Body'][0]['soap:Fault'][0]['soap:Reason'][0]['soap:Text'][0]._;

        console.log(faultReasonText);

        return resolve(faultReasonText);
      });
    });
  }
}

module.exports = WorldManager;
