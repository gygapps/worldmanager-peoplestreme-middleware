
const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const EmailService = require('../../src/services/EmailService');
const WorldManagerImportErrorsStorage = require('../../src/services/WorldManagerImportErrorsStorage');

/**
 * Class to send an email with a report after all
 * accounts were exported from Preceda CSV file to World Manager
 *
 */
class EmailReportService {
  constructor(options) {
    let emailServiceConfig;

    if (options && options.sendRealEmails) {
      // SES config
      emailServiceConfig = {
        SES: new AWS.SES({
          apiVersion: '2010-12-01',
          region: 'us-east-1', // SES not available in Sydney yet
        }),
      };
    } else {
      // Mailtrap config
      emailServiceConfig = {
        host: process.env.MAILTRAP_HOST || 'smtp.mailtrap.io',
        port: process.env.MAILTRAP_PORT || '2525',
        auth: {
          user: process.env.MAILTRAP_USER || '4eab6d5c21020f',
          pass: process.env.MAILTRAP_PASSWORD || '5360e05c160c3e',
        },
        pool: true, // Use pooled connection
        rateLimit: true, // Enable to make sure we are limiting
        maxConnections: 1, // Set limit to 1 connection only
        maxMessages: 1, // Send 3 emails per second
      };
    }

    this.emailService = new EmailService(emailServiceConfig);
    this.worldManagerImportErrorsStorage = new WorldManagerImportErrorsStorage();
  }

  /**
   * Set recipients
   *
   * @param {Array} recipients
   */
  setRecipients(recipients) {
    this.recipients = recipients;
  }

  /**
   * Send email
   *
   * @param {*} count
   * @param {*} timestamp
   */
  async sendEmail(count, timestamp) {
    console.log('Send email');
    const htmlContent = await this.getHtmlContent(count, timestamp);
    console.log('htmlContent', htmlContent);
    await this.emailService.sendEmails(this.recipients, htmlContent);
  }

  /**
   * Get HTML content
   *
   * @param {*} count
   * @param {*} timestamp
   */
  async getHtmlContent(count, timestamp) {
    // Get list of errors from DB
    const errors = await this.worldManagerImportErrorsStorage.getAllErrors(timestamp);

    return `
      <p>No of records found in file that were changed: ${count.total}</p>
      <p>No of records updated: ${count.updated}</p>
      <p>No of records failed: ${count.failed}</p>
      <p>List of failed records:</p>
      ${EmailReportService.formatErrorTable(errors)}
    `;
  }

  /**
   * Create HTML table from error list
   *
   * @param {Array} errors
   */
  static formatErrorTable(errors) {
    let errorTable = '';

    if (!errors || errors.body.length === 0) {
      return '<p>No errors found.</p>';
    }

    errorTable += `<table style="border-collapse: collapse">
                    <thead>
                      <tr>
                        <th style="border: 1px solid black;">WM UUID</th>
                        <th style="border: 1px solid black;">Error message</th>
                      </tr>
                    </thead>
                    <tbody>`;

    errors.body.forEach((error) => {
      errorTable += `
                      <tr>
                        <td style="border: 1px solid black;">${error.UUID}</td>
                        <td style="border: 1px solid black;">${error.ErrorMsg}</td>
                      </tr>
                    `;
    });

    errorTable += '</tbody></table>';

    return errorTable;
  }
}

module.exports = EmailReportService;
