const DynamoDB = require('./DynamoDB');

class IntegrationTables {
  constructor() {
    this.dynamoDB = new DynamoDB();
  }

  /**
   * Get PositionType table
   */
  async getPositionType() {
    // Use dev table by default
    const tableName = process.env.POSITION_TYPE_TABLE_NAME || 'dev-PositionType';
    return this.dynamoDB.scanTable(tableName);
  }

  /**
   * Get CostLevel4 table
   *
   */
  async getCostLevel4() {
    // Use dev table by default
    const tableName = process.env.COST_LEVEL_4_TABLE_NAME || 'dev-CostLevel4';
    return this.dynamoDB.scanTable(tableName);
  }

  /**
   * Get Level table
   *
   */
  async getLevels() {
    // Use dev table by default
    const tableName = process.env.LEVEL_TABLE_NAME || 'dev-Level';
    return this.dynamoDB.scanTable(tableName);
  }

  /**
   * Get SalaryCode table
   *
   */
  async getSalaryCodes() {
    // Use dev table by default
    const tableName = process.env.SALARY_CODE_TABLE_NAME || 'dev-SalaryCode3';
    return this.dynamoDB.scanTable(tableName);
  }

  /**
   * Get Restaurant by UUID
   *
   * @param {*} uuid
   */
  async getRestaurant(uuid) {
    // Use dev table by default
    const tableName = process.env.RESTAURANTS_TABLE_NAME || 'dev-Restaurant';

    const key = {
      UUID: uuid,
    };

    return this.dynamoDB.getFromTable(tableName, key);
  }

  /**
   * Get Restaurant table
   *
   */
  async getRestaurants() {
    // Use dev table by default
    const tableName = process.env.RESTAURANTS_TABLE_NAME || 'dev-Restaurant';
    return this.dynamoDB.scanTable(tableName);
  }
}

module.exports = IntegrationTables;
