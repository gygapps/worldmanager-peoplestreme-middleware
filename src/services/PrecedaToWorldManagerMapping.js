const _ = require('lodash');
const moment = require('moment');

const IntegrationTables = require('./IntegrationTables');

const DateFormats = {
  Input: 'DD/MM/YYYY',
  Output: 'YYYY-MM-DD',
};

const Status = {
  Active: { PS: 'A', WM: 'true' },
  Inactive: { PS: 'T', WM: 'false' },
};

const YesNo = {
  Yes: { PS: 'Yes', WM: 'Y' },
  No: { PS: 'No', WM: 'N' },
};

const PersonTitle = {
  Mr: 'MR',
  Mrs: 'MRS',
  Miss: 'MISS',
  Ms: 'MS',
  Dr: 'DR',
  Prof: 'PROF',
  Rev: 'REV',
};

const PositionTitle = [
  { PS: 'Crew', WM: 'Crew' },
  { PS: 'Senior Cook', WM: 'Senior Cook (Hrly Paid)' },
  { PS: 'Shift Leader', WM: 'Shift Leader' },
  { PS: 'Head Cook', WM: 'Head Cook (Salaried)' },
  { PS: 'Assistant Restaurant Manager', WM: 'Assistant Restaurant Manager' },
  { PS: 'Restaurant Manager', WM: 'Restaurant Manager' },
  { PS: 'Franchisee', WM: 'Franchisee' },
  { PS: 'Head Office', WM: 'Head Office' },
];

const Gender = {
  Male: { PS: ['Male', 'M'], WM: 'M' },
  Female: { PS: ['Female', 'F'], WM: 'F' },
};

const PositionType = {
  FullTime: 'Fulltime',
  PartTime: 'Part Time',
  Casual: 'Casual',
};

class PrecedaToWorldManagerMapping {
  constructor(options) {
    this.worldManager = options.worldManager;
    this.integrationTables = new IntegrationTables();
    this.customFields = null;
  }

  /**
   * Preceda file to WM
   *
   * @param {*} data
   */
  async precedaFileMapping(data) {
    // Person title should be upper case and can't be empty
    if (!data.Title) {
      console.error(`No title for account ${data['Global ID 03']}.`);
      return null;
    }

    const personTitle = data.Title.toUpperCase();
    const storeUuid = await this.getStoreUUIDByName(data['Level 3 Description']); // Map from WM
    const storeStatus = await this.getStoreStatusByUUID(storeUuid) ? 'true' : 'false';

    return {
      uuid: data['Global ID 03'],
      // groupId: 'groupId', // Get from WM
      storeUuids: storeUuid,
      // username: username, // Get from WM
      person: {
        title: PrecedaToWorldManagerMapping.validatePersonTitle(personTitle),
        firstName: data['First Name'],
        // middleName: '', // Get from WM
        lastName: data.Surname,
        // nickname: '', // Get from WM
        // displayName: '', // Get from WM
      },
      positionType: PrecedaToWorldManagerMapping.mapPositionType(data['Employment Type Description']), // Map from WM
      email: data['Personel Email Address'],
      mobile: data['Mobile Phone Number'],
      workStartDate: PrecedaToWorldManagerMapping.mapDate(data['Hire Date']), // Format date to YYYY-MM-DD
      active: storeStatus,
      customFields: await this.mapCustomFields(data),
    };
  }

  /**
   * Get all custom fields from WM API
   *
   */
  async getCustomFields() {
    return this.worldManager.getCustomFields();
  }

  /**
   * Get custom field by name
   *
   * @param {string} name
   */
  async getCustomField(name) {
    if (!this.customFields) {
      this.customFields = await this.getCustomFields();
    }

    // Find by name
    const customField = _.find(this.customFields, { name });

    if (customField) {
      return customField;
    }

    // Not found
    return null;
  }

  /**
   * Get custom fields UUID by name
   *
   * @param {string} name
   */
  async getCustomFieldUUID(name) {
    if (!this.customFields) {
      this.customFields = await this.getCustomFields();
    }

    // Find by name
    const customField = _.find(this.customFields, { name });

    if (customField && customField.uuid) {
      return customField.uuid;
    }

    // Not found
    return null;
  }

  /**
   * Map custom fields
   * @param {Array} data
   */
  async mapCustomFields(data) {
    // List of custom fields
    // WM is the name for World Manager API
    // PS is the header from PS CSV file
    const customFieldsWMPSMapping = [
      { WM: '05. Salaried Employee', PS: 'Autopay' },
      { WM: "01. Crew Member's Date Of Birth (RM must complete when creating account)", PS: 'Date of Birth' },
      { WM: '02. Gender', PS: 'Gender Desc' },
      { WM: "36. Emergency Contact: Person's Name", PS: 'Emergency Contact Surname' },
      { WM: "37. Emergency Contact: Person's phone number", PS: 'Emergency Contact Home Phone' },
      { WM: '04. Employment Level', PS: 'Salary Code Description' },
      { WM: '06. Salary', PS: 'Base Rate' },
      { WM: '38. Preceda I.D', PS: 'Employee ID Number' },
      { WM: '08. Monday Shift Time', PS: 'Mon Notes Description' },
      { WM: '11. Monday Total Hours', PS: 'Contracted Hrs Mon' },
      { WM: '12. Tuesday Shift Time', PS: 'Tue Notes Description' },
      { WM: '15. Tuesday Total Hours', PS: 'Contracted Hrs Tue' },
      { WM: '16. Wednesday Shift Time', PS: 'Wed Notes Description' },
      { WM: '19. Wednesday Total Hours', PS: 'Contracted Hrs Wed' },
      { WM: '20. Thursday Shift Time', PS: 'Thu Notes Description' },
      { WM: '23. Thursday Total Hours', PS: 'Contracted Hrs Thu' },
      { WM: '24. Friday Shift Time', PS: 'Fri Notes Description' },
      { WM: '27. Friday Total Hours', PS: 'Contracted Hrs Fri' },
      { WM: '28. Saturday Shift Time', PS: 'Sat Notes Description' },
      { WM: '31. Saturday Total Hours', PS: 'Contracted Hrs Sat' },
      { WM: '32. Sunday Shift Time', PS: 'Sun Notes Description' },
      { WM: '35. Sunday Total Hours', PS: 'Contracted Hrs Sun' },
      { WM: 'Preceda_ID', PS: ' ' },
    ];

    // Create array with custom field UUID and new value
    const accountCustomFields = [];

    // Get all custom fields from WM API
    this.customFields = await this.getCustomFields();

    for (let i = 0; i < customFieldsWMPSMapping.length; i += 1) {
      const element = customFieldsWMPSMapping[i];
      let value;
      if (element.PS === 'Date of Birth') {
        value = PrecedaToWorldManagerMapping.mapDate(data[element.PS]);
      } else if (element.PS === 'Apprentice/Trainee') {
        value = PrecedaToWorldManagerMapping.mapYesNo(data[element.PS]);
      } else if (element.PS === 'Gender Desc') {
        value = PrecedaToWorldManagerMapping.mapGender(data[element.PS]);
      } else if (element.PS === 'Salary Code Description') {
        value = await this.mapSalaryCodeDescription(data[element.PS]);
      } else if (element.PS.includes('Contracted Hrs')) {
        // Check if the value is numeric and set to blank if not
        if (!Number.isNaN(parseFloat(data[element.PS]))) {
          value = data[element.PS];
        } else {
          value = '';
        }
      } else {
        value = data[element.PS];
      }

      // No value found
      if (!value && value !== '') {
        // eslint-disable-next-line no-continue
        continue;
      }

      const customField = await this.getCustomField(element.WM);

      // Custom field not found
      if (!customField) {
        console.error(`Custom field ${element.WM} not found.`);
        // eslint-disable-next-line no-continue
        continue;
      }

      // Not a valid value
      if (value && value !== '' && customField.validValues && !customField.validValues.includes(value)) {
        console.error(`${value} is not one of ${customField.validValues}.`);
        // eslint-disable-next-line no-continue
        continue;
      }

      accountCustomFields.push({
        uuid: customField.uuid,
        value,
        WM: element.WM,
        PS: element.PS,
      });
    }

    return accountCustomFields;
  }

  /**
   * Map date to WM format
   *
   * @param {*} date
   */
  static mapDate(date) {
    return moment(date, DateFormats.Input).format(DateFormats.Output);
  }

  /**
   * Map position type to WM format
   *
   * @param {string} positionType
   */
  static mapPositionType(positionType) {
    switch (positionType) {
      case 'Part Time Salaried':
      case 'Part Time':
      case 'Part-Time':
        return PositionType.PartTime;
      case 'Full Time Salaried':
      case 'Full Time':
      case 'Full-Time':
        return PositionType.FullTime;
      case 'Casual':
        return PositionType.Casual;
      default:
        return '';
    }
  }

  /**
   * Map Apprentice/Trainee value
   *
   * @param {string} value
   */
  static mapYesNo(value) {
    switch (value) {
      case YesNo.Yes.PS:
        return YesNo.Yes.WM;
      case YesNo.No.PS:
        return YesNo.No.WM;
      default:
        return YesNo.No.WM;
    }
  }

  /**
   * Map position title from PS to WM
   *
   * @param {string} value
   */
  static mapPositionTitle(value) {
    if (!value) {
      return '';
    }

    const positionTitle = _.find(PositionTitle, { PS: value });

    if (positionTitle && positionTitle.WM) {
      return positionTitle.WM;
    }

    // Not found
    console.error(`Position title ${value} is not valid.`);
    return '';
  }

  /**
   * Map gender from PS to WM
   *
   * @param {string} value
   */
  static mapGender(value) {
    if (!value) {
      return value;
    }

    if (Gender.Male.PS.includes(value)) {
      return Gender.Male.WM;
    }

    if (Gender.Female.PS.includes(value)) {
      return Gender.Female.WM;
    }

    // Not found
    return '';
  }

  /**
   * Map salary code description from PS to WM
   *
   * @param {string} value
   */
  async mapSalaryCodeDescription(value) {
    if (!value) {
      return value;
    }

    // Get all salary codes
    const salaryCodes = await this.integrationTables.getSalaryCodes();

    if (salaryCodes) {
      const salaryCode = _.find(salaryCodes, { SalaryDescription: value });

      if (salaryCode && salaryCode.Level) {
        // Remove "Casual" from Level
        return salaryCode.Level.replace(PositionType.Casual, '').trim();
      }
    }

    // No salary code
    return null;
  }

  /**
   * Map active value
   * A means Active
   * T means Inactive
   *
   * @param {string} value
   */
  static mapActive(value) {
    switch (value) {
      case Status.Active.PS:
        return Status.Active.WM;
      case Status.Inactive.PS:
        return Status.Inactive.WM;
      default:
        return Status.Inactive.WM;
    }
  }

  /**
   * Validate person title
   *
   * @param {string} title
   */
  static validatePersonTitle(title) {
    const validValues = Object.keys(PersonTitle).map(key => PersonTitle[key]);

    if (!validValues.includes(title)) {
      throw Error(`${title} is not one of ${validValues}`);
    }

    return title;
  }

  /**
   * Get store by name
   *
   * @param {string} storeName
   */
  async getStoreUUIDByName(storeName) {
    if (!storeName) {
      return null;
    }

    // Get all restaurants
    const restaurants = await this.integrationTables.getRestaurants();

    if (restaurants) {
      let restaurant = _.find(restaurants, { Name: storeName.toUpperCase() });

      if (restaurant) {
        return restaurant.UUID;
      }

      // Look in the Preceda column
      restaurant = _.find(restaurants, { PSName: storeName.toUpperCase() });

      if (restaurant) {
        return restaurant.UUID;
      }
    }

    // No restaurants
    return null;
  }

  /**
     * Get store Status by UUID
     *
     * @param {string} storeUUID
     */
  async getStoreStatusByUUID(storeUUID) {
    if (!storeUUID) {
      return false;
    }

    // Get all restaurants
    const restaurants = await this.integrationTables.getRestaurants();

    if (restaurants) {
      const restaurant = _.find(restaurants, { UUID: storeUUID });

      if (restaurant) {
        return restaurant.Active;
      }
    }

    // No restaurants
    return false;
  }

  /**
   * Get WM position type
   *
   * @param {*} employmentType
   */
  async getWMPositionTypeByEmploymentType(employmentType) {
    if (!employmentType) {
      return null;
    }

    // Get all position types
    const positionTypes = await this.integrationTables.getPositionType();

    if (positionTypes) {
      return _.find(positionTypes, { EmploymentType: employmentType });
    }

    // No restaurants
    return null;
  }
}

module.exports = PrecedaToWorldManagerMapping;
