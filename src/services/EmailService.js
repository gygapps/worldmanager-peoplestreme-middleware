const nodemailer = require('nodemailer');

/**
 * Class to send emails using Mailtrap or SES
 * It's a wrapper for nodemailer
 *
 */
class EmailService {
  constructor(options) {
    this.transport = nodemailer.createTransport(options);
  }

  /**
   * Send emails to multiple recipients
   *
   * @param {*} recipientList
   * @param {*} htmlContent
   */
  async sendEmails(recipientList, htmlContent) {
    const responses = [];
    const promises = recipientList.map((recipient) => {
      const response = this.sendEmail(recipient, htmlContent);
      responses.push(response);
      return response;
    });

    // Wait for all emails to be sent
    await Promise.all(promises);

    return responses;
  }

  /**
   * Send email
   *
   * @param {*} recipients
   * @param {*} htmlContent
   */
  async sendEmail(recipients, htmlContent) {
    // Send one email
    return new Promise((resolve, reject) => {
      const subject = 'Preceda to WM integration';

      this.transport.sendMail({
        from: 'gyg@4mation.com.au',
        to: recipients,
        subject,
        html: htmlContent,
      }, (err, info) => {
        if (err) {
          console.log('EmailService::sendEmail() Error', err);
          reject(err);
        } else {
          console.log('EmailService::sendEmail() Envelope: ', info.envelope);
          console.log('EmailService::sendEmail() MessageId', info.messageId);
          resolve(info);
        }
      });
    });
  }
}

module.exports = EmailService;
