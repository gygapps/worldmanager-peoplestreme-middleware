const axios = require('axios');

class WorldManagerApi {
  constructor(url) {
    return axios.create({
      baseURL: url,
    });
  }
}

module.exports = WorldManagerApi;
