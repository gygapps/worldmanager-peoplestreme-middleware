const PrecedaToWorldManagerExport = require('../../src/services/PrecedaToWorldManagerExport');
const WorldManager = require('../../src/services/WorldManager');
const PrecedaToWorldManagerMapping = require('../../src/services/PrecedaToWorldManagerMapping');
const CSVService = require('../../src/services/CSVService');
const SFTPService = require('../../src/services/SFTPService');
const AlertsService = require('../../src/services/AlertsService');
const SlackAlertsService = require('../../src/services/SlackAlertsService');
const PrecedaAccountsStorage = require('../../src/services/PrecedaAccountsStorage');
const PrecedaToWorldManagerCounter = require('../../src/services/PrecedaToWorldManagerCounter');
const SQSService = require('../../src/services/SQSService');

const worldManagerConfig = {
  tokenURL: process.env.WORLD_MANAGER_TOKEN_URL,
  apiURL: process.env.WORLD_MANAGER_API_URL,
  clientId: process.env.WORLD_MANAGER_CLIENT_ID,
  clientSecret: process.env.WORLD_MANAGER_CLIENT_SECRET,
};

const sftpConfig = {
  host: process.env.SFTP_PRECEDA_HOST,
  port: '22',
  username: process.env.SFTP_PRECEDA_USER,
  password: process.env.SFTP_PRECEDA_PASSWORD,
  algorithms: {
    kex: [
      'diffie-hellman-group-exchange-sha256',
      'diffie-hellman-group14-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha1',
      'hmac-sha2-512',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};

const worldManager = new WorldManager(worldManagerConfig);

const precedaToWorldManagerMapping = new PrecedaToWorldManagerMapping({ worldManager });

const sftpService = new SFTPService(sftpConfig);

const slackOptions = {
  webHook: process.env.SLACK_WEBHOOK,
  msgPrefix: process.env.SLACK_MSG_PREFIX,
};

const slackAlertService = new SlackAlertsService(slackOptions);

const alertsService = new AlertsService({ services: [slackAlertService] });

const precedaAccountsStorage = new PrecedaAccountsStorage();

const sqsService = new SQSService();

const precedaToWorldManagerCounter = new PrecedaToWorldManagerCounter();

const precedaToWorldManagerExport = new PrecedaToWorldManagerExport({
  worldManager,
  precedaToWorldManagerMapping,
  SFTPService: sftpService,
  CSVService,
  remoteDirectory: process.env.SFTP_PRECEDA_PATH,
  remoteFileName: process.env.SFTP_PRECEDA_FILENAME,
  alertsService,
  precedaAccountsStorage,
  sqsService,
  precedaToWorldManagerCounter,
});

/**
 * Lambda function handler
 *
 */
module.exports.index = async (event) => {
  // Get limit and offset parameters from event
  const limit = event && event.limit ? event.limit : null;
  const offset = event && event.offset ? event.offset : null;

  const options = { limit, offset };

  // Read CSV file and send to queue
  const response = await precedaToWorldManagerExport.getAccountsFromCSVAndSendToQueue(options);

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: response,
    }, null, 2),
  };
};
