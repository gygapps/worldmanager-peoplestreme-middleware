const moment = require('moment-timezone');
const hash = require('object-hash');
const _ = require('lodash');

const PrecedaToWorldManagerExport = require('../../src/services/PrecedaToWorldManagerExport');
const WorldManager = require('../../src/services/WorldManager');
const PrecedaToWorldManagerMapping = require('../../src/services/PrecedaToWorldManagerMapping');
const CSVService = require('../../src/services/CSVService');
const SFTPService = require('../../src/services/SFTPService');
const AlertsService = require('../../src/services/AlertsService');
const SlackAlertsService = require('../../src/services/SlackAlertsService');
const PrecedaAccountsStorage = require('../../src/services/PrecedaAccountsStorage');
const WorldManagerImportErrorsStorage = require('../../src/services/WorldManagerImportErrorsStorage');
const SQSService = require('../../src/services/SQSService');
const PrecedaToWorldManagerCounter = require('../../src/services/PrecedaToWorldManagerCounter');

const worldManagerConfig = {
  tokenURL: process.env.WORLD_MANAGER_TOKEN_URL,
  apiURL: process.env.WORLD_MANAGER_API_URL,
  clientId: process.env.WORLD_MANAGER_CLIENT_ID,
  clientSecret: process.env.WORLD_MANAGER_CLIENT_SECRET,
};

const sftpConfig = {
  host: process.env.SFTP_PRECEDA_HOST,
  port: '22',
  username: process.env.SFTP_PRECEDA_USER,
  password: process.env.SFTP_PRECEDA_PASSWORD,
  algorithms: {
    kex: [
      'diffie-hellman-group-exchange-sha256',
      'diffie-hellman-group14-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha1',
      'hmac-sha2-512',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};

const worldManager = new WorldManager(worldManagerConfig);

const precedaToWorldManagerMapping = new PrecedaToWorldManagerMapping({ worldManager });

const sftpService = new SFTPService(sftpConfig);

const slackOptions = {
  webHook: process.env.SLACK_WEBHOOK,
  msgPrefix: process.env.SLACK_MSG_PREFIX,
};

const slackAlertService = new SlackAlertsService(slackOptions);

const alertsService = new AlertsService({ services: [slackAlertService] });

const precedaAccountsStorage = new PrecedaAccountsStorage();

const sqsService = new SQSService();

const precedaToWorldManagerCounter = new PrecedaToWorldManagerCounter();

const precedaToWorldManagerExport = new PrecedaToWorldManagerExport({
  worldManager,
  precedaToWorldManagerMapping,
  SFTPService: sftpService,
  CSVService,
  remoteFilePath: process.env.SFTP_PRECEDA_PATH + process.env.SFTP_PRECEDA_FILENAME,
  alertsService,
  precedaAccountsStorage,
  sqsService,
  precedaToWorldManagerCounter,
});

const worldManagerImportErrorsStorage = new WorldManagerImportErrorsStorage();

/**
 * Lambda function handler
 * This function is triggered by PSAccounts SQS
 * It reads an account from the queue and call World Manager service to update it
 *
 */
module.exports.index = async (event) => {
  if (!event || !event.Records || event.Records.length === 0) {
    console.log('No records found');
    return false;
  }

  if (event && event.Records) {
    console.log(`Got ${event.Records.length} records`);
  }

  const mapFieldsAndExportToWMPromises = [];
  const errorCount = {};
  const successCount = {};

  // Read messages from queue
  event.Records.forEach((record) => {
    const { body } = record;
    console.log('Event record body: ', body);
    console.log('Event record messageAttributes: ', record.messageAttributes);
    mapFieldsAndExportToWMPromises.push(precedaToWorldManagerExport.mapFieldsAndExportToWM(JSON.parse(body)));
  });

  // Wait for all accounts to be exported
  const responses = await Promise.all(mapFieldsAndExportToWMPromises);

  const saveErrorPromises = [];

  const updateCounterPromises = [];

  const worldManagerAccountsPromise = [];

  // Send errors to Dynamo DB
  if (responses && responses.length > 0) {
    responses.forEach((response, index) => {
      try {
        // Check we have sent date message attribute
        if (_.has(event.Records[index], 'messageAttributes.SentDate.stringValue')) {
          const sendDate = event.Records[index].messageAttributes.SentDate.stringValue;
          // Get account from event Records
          const account = JSON.parse(event.Records[index].body);

          if (!response || response.error) {
            console.error(`Error updating account ${account}`);
            console.log('Error Message:', response.error);

            // Item to save to DB
            const errorItem = {
              UUID: account['Global ID 03'],
              Timestamp: sendDate,
              ErrorMsg: response.error || 'Unknown error. Check logs.',
            };

            // Save error to DB
            saveErrorPromises.push(worldManagerImportErrorsStorage.saveError(errorItem));

            // Update error count
            errorCount[sendDate] = errorCount[sendDate] ? errorCount[sendDate] + 1 : 1;
            updateCounterPromises.push(precedaToWorldManagerCounter.incrementFailedCounter(sendDate));
          } else {
            // Update success count
            successCount[sendDate] = successCount[sendDate] ? successCount[sendDate] + 1 : 1;
            updateCounterPromises.push(precedaToWorldManagerCounter.incrementUpdatedCounter(sendDate));

            // Saved processed account
            const accountDBItem = {
              UUID: account['Global ID 03'],
              PSHash: hash(account), // Calculate hash
              UpdatedAt: moment().tz('Australia/Sydney').format('YYYY-MM-DD HH:mm:ss'), // Updated in WorldManager
            };

            worldManagerAccountsPromise.push(precedaAccountsStorage.saveAccount(accountDBItem));
          }
        } else {
          console.error('No SentDate message attribute found.');
        }
      } catch (error) {
        console.error(error);
      }
    });
  }

  // Wait for all the error messages to be saved
  await Promise.all(saveErrorPromises);

  // Wait for the counter to be updated
  await Promise.all(updateCounterPromises);

  // Wait for the processed accounts to be saved
  await Promise.all(worldManagerAccountsPromise);

  return {
    statusCode: 200,
    body: {
      errorCount,
      successCount,
    },
  };
};
