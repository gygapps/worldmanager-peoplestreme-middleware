const WorldManager = require('../services/WorldManager');

module.exports.index = async (event) => {
  // Request parameters
  console.log(event);
  const parameters = event.pathParameters;
  const options = {
    tokenURL: process.env.WORLD_MANAGER_TOKEN_URL,
    apiURL: process.env.WORLD_MANAGER_API_URL,
    clientId: process.env.WORLD_MANAGER_CLIENT_ID,
    clientSecret: process.env.WORLD_MANAGER_CLIENT_SECRET,
  };

  const worldManager = new WorldManager(options);
  let response;

  if (parameters.timestamp) {
    response = await worldManager.findAccountsWithCustomFieldsUpdatedSince(`${parameters.timestamp}T00:00:00Z`);
  } else if (parameters.uuid) {
    response = await worldManager.findCustomFields(parameters.uuid);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: response,
    }, null, 2),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
