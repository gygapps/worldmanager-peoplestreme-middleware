const EmailReportService = require('../../src/services/EmailReportService');
const PrecedaToWorldManagerCounter = require('../../src/services/PrecedaToWorldManagerCounter');

const emailReportServiceOptions = {
  sendRealEmails: process.env.SEND_REAL_EMAILS === 'true',
};

const emailReportService = new EmailReportService(emailReportServiceOptions);

const precedaToWorldManagerCounter = new PrecedaToWorldManagerCounter();

/**
 * Lambda function handler
 * Triggered by changes in PSExportCount Dynamo DB table
 * It sends an email when all the accounts in the queue have been processed
 *
 */
module.exports.index = async (event) => {
  let finishedProcessing = false;
  let toUpdate;
  let failed;
  let processed;
  let updated;
  let timestamp;

  // Check Dynamo DB item
  // If ToUpdate === Failed + Updated send an email with report
  if (event.Records) {
    event.Records.forEach((record) => {
      if (record.dynamodb.NewImage && record.dynamodb.NewImage.ToUpdate
            && record.dynamodb.NewImage.Failed && record.dynamodb.NewImage.Updated) {
        // Number of accounts we should update
        toUpdate = Number(record.dynamodb.NewImage.ToUpdate.N);
        // Number of accounts that failed during update
        failed = Number(record.dynamodb.NewImage.Failed.N);
        // Number of accounts successfuly updated
        updated = Number(record.dynamodb.NewImage.Updated.N);
        // Number of accounts processed (failed and updated)
        processed = Number(record.dynamodb.NewImage.Failed.N) + Number(record.dynamodb.NewImage.Updated.N);
        console.log('To update:', toUpdate);
        console.log('Processed', processed);

        // If all accounts are processed, send an email
        if (toUpdate === processed && processed !== 0) {
          finishedProcessing = true;
          timestamp = record.dynamodb.NewImage.Timestamp.S;
        }
      }
    });
  }

  if (finishedProcessing && !await precedaToWorldManagerCounter.emailWasSent(timestamp)) {
    const count = {
      total: toUpdate,
      updated,
      failed,
    };

    // Add recipients to email
    const recipients = process.env.PS_TO_WM_REPORT_RECIPIENTS.split(',');

    // Check if there are recipients
    if (!recipients || recipients.length === 0) {
      return {
        statusCode: 200,
        body: 'No recipients found.',
      };
    }

    emailReportService.setRecipients(recipients);

    // Send email
    await emailReportService.sendEmail(count, timestamp);

    // Mark as sent
    await precedaToWorldManagerCounter.setEmailSent(timestamp);
  }

  return {
    statusCode: 200,
    body: { shouldSendEmail: finishedProcessing },
  };
};
