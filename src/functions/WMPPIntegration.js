/**
 * This is the handler for the lambda function WMPPIntegration
 * It exports World Manager accounts to a CSV file in an SFTP server
 *
 */

const moment = require('moment');
const WorldManagerToPeopleStremeExport = require('../../src/services/WorldManagerToPeopleStremeExport');
const AlertsService = require('../../src/services/AlertsService');
const SlackAlertsService = require('../../src/services/SlackAlertsService');

/**
 * World Manager configuration
 *
 * tokenURL: URL to get token
 * apiURL: API URL
 * clientId: Client ID for API authentication
 * clientSecret: Client Secret for API authentication
 */
const worldManagerConfig = {
  tokenURL: process.env.WORLD_MANAGER_TOKEN_URL,
  apiURL: process.env.WORLD_MANAGER_API_URL,
  clientId: process.env.WORLD_MANAGER_CLIENT_ID,
  clientSecret: process.env.WORLD_MANAGER_CLIENT_SECRET,
};

/**
 * SFTP configuration
 *
 */
const sftpConfig = {
  host: process.env.SFTP_HOST,
  port: '22',
  username: process.env.SFTP_USER,
  password: process.env.SFTP_PASSWORD,
  algorithms: {
    kex: [
      'ecdh-sha2-nistp256',
      'diffie-hellman-group1-sha1',
    ],
    cipher: [
      'aes128-ctr',
      '3des-cbc',
      'blowfish-cbc',
    ],
    hmac: [
      'hmac-sha2-256',
      'hmac-md5',
    ],
    serverHostKey: ['ssh-rsa'],
    compress: ['none'],
  },
};

const slackOptions = {
  webHook: process.env.SLACK_WEBHOOK,
  msgPrefix: process.env.SLACK_MSG_PREFIX,
};

const slackAlertService = new SlackAlertsService(slackOptions);

const alertsService = new AlertsService({ services: [slackAlertService] });

/**
 * Options for WorldManagerToPeopleStremeExport
 *
 * worldManager: World Manager configuration
 * sftpConfig: SFTP configuration
 * standardFileName: CSV standard file name
 * customFileName: CSV custom file name
 * alertsService: alerts service
 *
 */
const options = {
  worldManager: worldManagerConfig,
  sftpConfig,
  standardFileName: process.env.PS_STANDARD_FILEPATH,
  customFileName: process.env.PS_CUSTOM_FILEPATH,
  alertsService,
};

const worldManagerToPeopleStremeExport = new WorldManagerToPeopleStremeExport(options);

const getTimestampFromEvent = (event) => {
  let timestamp = null;

  // If called from API gateway get pathParameters
  if (event && event.pathParameters && event.pathParameters.timestamp) {
    ({ timestamp } = event.pathParameters);
  } else if (event.timestamp) {
    ({ timestamp } = event);
  }

  // Set default timestamp to yesterday
  if (!timestamp) {
    const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
    timestamp = yesterday;
  }

  return timestamp;
};

module.exports.index = async (event) => {
  // If there is a date in event parameters use that
  // Otherwise default is yesterday
  const timestamp = getTimestampFromEvent(event);

  const response = await worldManagerToPeopleStremeExport.exportAccountsCreatedSince(timestamp);

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: response,
    }, null, 2),
  };
};
